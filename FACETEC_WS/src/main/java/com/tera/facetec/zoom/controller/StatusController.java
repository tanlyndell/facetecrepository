package com.tera.facetec.zoom.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.tera.facetec.zoom.implem.UserImplem;
import com.tera.facetec.zoom.mapper.bean.UserBean;
import com.tera.facetec.zoom.util.Helper;

@RestController
public class StatusController implements EnvironmentAware{

	@Autowired
	public static Environment env;
	
	@Autowired
	UserImplem userImplem;
	
	@CrossOrigin
    @GetMapping(value = "/status", produces = "application/json")
    public HashMap<String,Object> getStatus(
    		@RequestHeader("X-Device-License-Key") String license,
    		@RequestHeader("systemID") String systemID) {
    	
        //TESTING ONLY FOR DB CONNECTION
/*        HashMap<String,Object> values = new HashMap<String,Object>();
        try{
            List<UserBean> users = userImplem.getUsers(values);
            if(users!=null && users.size()>0){
            	System.out.println("TEST DB CONNECTION");
            	
            	for(UserBean user : users)
            		System.out.println(user.getUsr_id());
            }
        }catch(Exception e){
        	e.printStackTrace();
        }*/
        //TESTING ONLY FOR DB CONNECTION
        
    	//Get URI
      	String uri = env.getProperty("zoom.server") + env.getProperty("uri.status");

    	//Initiate Rest template
    	SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(Integer.parseInt(env.getProperty("connect.timeout")));
        factory.setReadTimeout(Integer.parseInt(env.getProperty("read.timeout")));
        RestTemplate restTemplate = new RestTemplate(factory);
         
        //Set HTTP headers
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("X-Device-License-Key", license);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
         
        //Turn off SSL Checking
        String offSslCheck = env.getProperty("offSslCheck");
        if("Y".equals(offSslCheck))
        	Helper.sslOff();
        
        //Get result
        ResponseEntity<HashMap> result = restTemplate.exchange(uri, HttpMethod.GET, entity, HashMap.class);

        //if(result.getStatusCode() == HttpStatus.OK)
        //return result.getBody();
        
        HashMap<String,Object> json = (HashMap<String,Object>)result.getBody();
        return json;
    	
    }
    
	@Override
	public void setEnvironment(Environment arg0) {
		// TODO Auto-generated method stub
		env = arg0;
	}

	//@RequestMapping(path = "/employees")
    //@GetMapping(path="/", produces = "application/json")
    //@PostMapping(path= "/", consumes = "application/json", produces = "application/json")

	/*,@RequestHeader(value="DUMMY", required=false) String dummy*/
	
/*        HashMap<String, Object> map = new HashMap<>();
    map.put("systemID", "aa");
    map.put("systemUser", "aa");
*/
/*		JSONObject body = new JSONObject();
	
	try{
		body.put("systemID", "a");
		body.put("systemUser", "b");
	}catch(Exception e){
		e.printStackTrace();
	}*/

    //return map;
	
	
}