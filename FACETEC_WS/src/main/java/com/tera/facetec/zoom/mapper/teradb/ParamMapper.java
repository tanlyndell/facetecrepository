package com.tera.facetec.zoom.mapper.teradb;

import java.util.HashMap;
import java.util.List;

import com.tera.facetec.zoom.mapper.bean.ApplParamBean;
import com.tera.facetec.zoom.mapper.bean.SystemCredentialsBean;
import com.tera.facetec.zoom.mapper.bean.UserBean;


public interface ParamMapper {

	public ApplParamBean getParam(HashMap<String,Object> values);


}