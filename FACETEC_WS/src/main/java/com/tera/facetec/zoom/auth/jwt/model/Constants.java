package com.tera.facetec.zoom.auth.jwt.model;

public class Constants {

    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}
