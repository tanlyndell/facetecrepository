package com.tera.facetec.zoom.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.tera.facetec.zoom.util.Helper;

@RestController
public class AdminController implements EnvironmentAware{

	@Autowired
	public static Environment env;
	
	@CrossOrigin
    @GetMapping(value = "/admin/getTransactionData", produces = "application/json")
    public HashMap<String,Object> getTransactionData() {
    	
    	//Get URI
      	String uri = env.getProperty("zoom.server") + env.getProperty("uri.admin.tranData");

    	//Initiate Rest template
    	SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(Integer.parseInt(env.getProperty("connect.timeout")));
        factory.setReadTimeout(Integer.parseInt(env.getProperty("read.timeout")));
        RestTemplate restTemplate = new RestTemplate(factory);
         
        //Set HTTP headers
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        //headers.add("X-Device-License-Key", license);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
       
        //Turn off SSL Checking
        String offSslCheck = env.getProperty("offSslCheck");
        if("Y".equals(offSslCheck))
        	Helper.sslOff();
        
        //Get result
        ResponseEntity<HashMap> result = restTemplate.exchange(uri, HttpMethod.GET, entity, HashMap.class);
         
        //if(result.getStatusCode() == HttpStatus.OK)
        //return result.getBody();
        
        HashMap<String,Object> json = (HashMap<String,Object>)result.getBody();
        return json;
    }

	@CrossOrigin
    @GetMapping(value = "/admin/getFaceMapData", produces = "application/json")
    public HashMap<String,Object> getFaceMapData() {
    	
    	//Get URI
      	String uri = env.getProperty("zoom.server") + env.getProperty("uri.admin.faceMap");

    	//Initiate Rest template
    	SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(Integer.parseInt(env.getProperty("connect.timeout")));
        factory.setReadTimeout(Integer.parseInt(env.getProperty("read.timeout")));
        RestTemplate restTemplate = new RestTemplate(factory);
         
        //Set HTTP headers
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        //headers.add("X-Device-License-Key", license);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
         
        //Turn off SSL Checking
        String offSslCheck = env.getProperty("offSslCheck");
        if("Y".equals(offSslCheck))
        	Helper.sslOff();
        
        //Get result
        ResponseEntity<HashMap> result = restTemplate.exchange(uri, HttpMethod.GET, entity, HashMap.class);
         
        //if(result.getStatusCode() == HttpStatus.OK)
        //return result.getBody();
        
        HashMap<String,Object> json = (HashMap<String,Object>)result.getBody();
        return json;
	
    }
	
	@Override
	public void setEnvironment(Environment arg0) {
		// TODO Auto-generated method stub
		env = arg0;
	}
	
}