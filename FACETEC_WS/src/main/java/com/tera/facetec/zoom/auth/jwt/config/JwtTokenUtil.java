/*package com.tera.facetec.zoom.auth.jwt.config;


import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import com.tera.facetec.zoom.auth.jwt.model.User;

@Component
public class JwtTokenUtil implements Serializable {
	
	
	private static final long serialVersionUID = -9100196914168143710L;

	@Value("${jwt.key}")
	private String signKey;
	
	@Value("${jwt.expires_in}")
	private String expiresIn;
	
	@Value("${jwt.refresh.expires_in}")
	private String refreshExpiresIn;
	
	
	@Value("${app.name}")
	private String appName;

    public String getUsernameFromToken(String token) {
    	return getClaimFromToken(token, Claims::getSubject);
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token) {
    	 Claims jws = Jwts.parser()
                .setSigningKey(signKey.getBytes())
                .parseClaimsJws(token)
                .getBody();
    	 return jws;
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public String generateToken(User user,String host) {
        return doGenerateToken(user.getUsrId(),host);
    }

    private String doGenerateToken(String subject,String host) {

        Claims claims = Jwts.claims().setSubject(subject);
        claims.put("scopes", Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN")));
        //claims.put("host", host);
        Date now = new Date(System.currentTimeMillis());
        Date exp = new Date(System.currentTimeMillis()  + Integer.parseInt(expiresIn)*60000); 

        String jwt = Jwts.builder()
                .setClaims(claims)
                .setIssuer(host)
                .setIssuedAt(now)
                .setNotBefore(now)
                .setExpiration(exp)
                .signWith(
                		SignatureAlgorithm.HS512, 
                		signKey.getBytes()
                		)
                .setHeaderParam("userid", subject)
                .compact();
        
        String[] jwtArr = jwt.split("\\.");
        
        System.out.println("JWT Signature: " + jwtArr[2] + " " + now);
        System.out.println("JWT Created: " + now);
        
        return jwt;
    }
    
    private String doCreateRefreshToken(String subject,String host) {

        Claims claims = Jwts.claims().setSubject(subject);
        claims.put("scopes", Arrays.asList(new SimpleGrantedAuthority("REFRESH_TOKEN")));
        //claims.put("host", host);
        Date now = new Date(System.currentTimeMillis());
        Date exp = new Date(System.currentTimeMillis()  + Integer.parseInt(refreshExpiresIn)*60000); 

        String jwt = Jwts.builder()
                .setClaims(claims)
                .setIssuer(host)
                .setIssuedAt(now)
                .setNotBefore(now)
                .setExpiration(exp)
                .signWith(
                		SignatureAlgorithm.HS512, 
                		signKey.getBytes()
                		)
                .setHeaderParam("userid", subject)
                .compact();
        
        String[] jwtArr = jwt.split("\\.");
        
        System.out.println("JWT Signature: " + jwtArr[2] + " " + now);
        System.out.println("JWT Created: " + now);
        
        return jwt;
    }
    
    public String refreshToken(String token) {
        Date now = new Date(System.currentTimeMillis());
        Date exp = new Date(System.currentTimeMillis()  + Integer.parseInt(expiresIn)*60000); 

        final Claims claims = getAllClaimsFromToken(token);
        claims.setIssuedAt(now);
        claims.setExpiration(exp);

        String jwt = Jwts.builder()
                .setClaims(claims)
                .signWith(
                		SignatureAlgorithm.HS512, 
                		signKey.getBytes()
                		)
                .compact();
        
        String[] jwtArr = jwt.split("\\.");
        
        System.out.println("JWT Signature: " + jwtArr[2] + " " + now);
        System.out.println("JWT Refreshed: " + now);
        
        return jwt;
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        
        System.out.println("JWT username: " + username);
        
        Claims claims = getAllClaimsFromToken(token);
   	 	System.out.println("JWT claims: " + claims.toString());
   	 	//claims.getIssuer()
        
        return (
              username.equals(userDetails.getUsername())
                    && !isTokenExpired(token));
    }

}*/