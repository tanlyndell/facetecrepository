package com.tera.facetec.zoom.util;

import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.tera.facetec.zoom.SSLUtil;
import com.tera.facetec.zoom.implem.ParamImplem;
import com.tera.facetec.zoom.implem.UserImplem;
import com.tera.facetec.zoom.mapper.bean.ApplParamBean;
import com.tera.facetec.zoom.util.blowfish.BlowfishEasy;

@Configuration
public class Helper{
	
	private static SimpleDateFormat sdf = new SimpleDateFormat();
	
	public static String encryptOneWay(String password, String usrID) throws NoSuchAlgorithmException{
		String newPassword = "";
		
		MessageDigest md = MessageDigest.getInstance("SHA-512");
		byte[] bytes = md.digest((password + usrID).getBytes());
		for(int i=0; i< bytes.length ;i++)
		{
			newPassword += Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1);
		}
		
		return newPassword;
	}
	
	public static RowBounds setRowBounds(HashMap<String, Object> values){
		
		int page = 1;
		//get From DB	
		int display = 15;
		
		try {
			if(values.containsKey("page")){
				page = Integer.parseInt(values.get("page").toString());
			}
			if(values.containsKey("display")){
				display = Integer.parseInt(values.get("display").toString());
			}
			
			if(page == 1){
				page = 0;
			}
			
			if(page > 0){
				page = display * (page - 1);
			}
			
		} catch (Exception e) {
			System.err.println("ROW BOUNDS ERROR: " + e.getMessage());
			return null;
		}
		
		values.put("page", page);
		values.put("display", display);
		
		return new RowBounds(page, display);
	}
	
	public static String formatTime(java.util.Date time, String dateformat)
	{
		if (dateformat.equalsIgnoreCase("MS SQL Server"))
			dateformat = "yyyy-MM-dd kk:mm:ss";
		else if (dateformat.equalsIgnoreCase("MM/dd/YYYY HH:mm aa")) //ADDED JTM 20140114 : from back office
			dateformat = "MM/dd/yyyy hh:mm aa";
		else if (dateformat.equalsIgnoreCase("HH:mm"))
			dateformat = "hh:mm aa";
		else if (dateformat.equalsIgnoreCase("HH:mm:ss aa"))
			dateformat = "hh:mm:ss aa";
		else if (dateformat.equalsIgnoreCase("display"))
			dateformat = "EEE, MMM. dd, yyyy (hh:mm aa)";
		/** Added NSV && MTA 02-02-2011 for Logging **/
		else if(dateformat.equalsIgnoreCase("logFileFilename"))
			      dateformat = "yyyy-MM-dd";
	    else if(dateformat.equalsIgnoreCase("logFile"))
	    	 dateformat = "yyyy-MM-dd HH:mm:ss.SSS";
	    else if(dateformat.equalsIgnoreCase("timestamp"))
	    	 dateformat = "HH:mm:ss.SSS";
	    else if(dateformat.equalsIgnoreCase("batchRef"))
	    	 dateformat = "yyMMdd";
	    else if(dateformat.equalsIgnoreCase("datetime"))
	    	 dateformat = "yyMMddHHmm";
	    else if (dateformat.equalsIgnoreCase("mailDate"))
	    	dateformat = "MMM. dd, yyyy (hh:mm aa)";
	    else if (dateformat.equalsIgnoreCase("confNo"))
	    	dateformat = "ddMMyy";
	    else if (dateformat.equalsIgnoreCase("MMddyyyy"))
	    	dateformat = "MMddyyyy";
	    	
		/** End of logging **/
		
		// BPI Express Online
	    else if(dateformat.equalsIgnoreCase("confirmationNumber"))
	    	dateformat = "yyyyMMddHHmmssSSS";
	    else if(dateformat.equalsIgnoreCase("transactionDate"))
	    	dateformat = "EEE, MMM. dd, yyyy hh:mm:ss aa";
	    else if(dateformat.equalsIgnoreCase("fullTransactionDate"))
	    	dateformat = "EEEE, MMMM dd, yyyy hh:mm:ss aa";
	    else if(dateformat.equalsIgnoreCase("conNumTime"))
	    	dateformat = "HHmmss";
	    else if(dateformat.equalsIgnoreCase("transactionHistoryPeriod"))
	    	dateformat = "EEE, MMM. dd, yyyy";
	    else if(dateformat.equalsIgnoreCase("preferredNameFormat"))
	    	dateformat = "MMMM dd, yyyy; HH:mm:ss";
		else if(dateformat.equalsIgnoreCase("webServiceDate"))
	    	dateformat = "MM-dd-yyyy";
		else if(dateformat.equalsIgnoreCase("accountDetails")) //ADDED JTM 20130125 - for Account details
	    	dateformat = "MM/dd/yyyy";
		else if(dateformat.equalsIgnoreCase("applicableRateFormat"))
			dateformat = "MMMM dd, yyyy HH:mm:ss aa";
		else if(dateformat.equalsIgnoreCase("iEnrollment"))//ADDED FNP - for Initial Enrollment
			dateformat = "M/d/yyyy hh:mm:ss aa";
		else if(dateformat.equalsIgnoreCase("bpieTime"))
			dateformat = "HHmm";
		else if(dateformat.equalsIgnoreCase("secondsOnly"))
			dateformat = "ssSSS";
	    else if (dateformat.equalsIgnoreCase("dateBlocked"))
	    	dateformat = "MM/dd/yyyy";
	    else if (dateformat.equalsIgnoreCase("timeBlocked"))
	    	dateformat = "MM/dd/yyyy HH:mm:ss";
		
		/** START * MODIFIED: NLA01*/
//		SimpleDateFormat sdf = new SimpleDateFormat(dateformat); *** NLA01 due multiple object instance. Moved this initialization outside the method.
		sdf.applyPattern(dateformat);
		/** END */
		return sdf.format(time); 
	}

	
	public static synchronized String decrypt(String str) {
		BlowfishEasy blowFish = new BlowfishEasy("tera");
		return blowFish.decryptString(str);
	}
	
	public static String getDecryptedPassword(String password) {
		String decPassword = new String();
		
		//for blowfish
		decPassword = decrypt(password);

		return decPassword;
	}

	//gets IP address from request
	public static String getIpAddress(HttpServletRequest request){
		if (request.getRemoteAddr().equals("127.0.0.1") || request.getRemoteAddr().equals("localhost") || request.getRemoteAddr().equals("0:0:0:0:0:0:0:1"))
			try {
				return  java.net.InetAddress.getLocalHost().getHostAddress();
			} catch (UnknownHostException e) {
				e.printStackTrace();
				return request.getRemoteAddr();
			}
		else
			return request.getRemoteAddr();
	}
	
	public static void setEnrollmentID(String systemID, Map<String,Object> request){
		if(systemID!=null && request!=null && request.containsKey("enrollmentIdentifier")){
			String id = (String) request.get("enrollmentIdentifier");
			String enrollID = getEnrollmentID(systemID, id);
			request.put("enrollmentIdentifier", enrollID);
		}	
	}
	
	public static String setEnrollmentID(String systemID, String enrollmentId){
		if(systemID!=null && enrollmentId!=null){
			String enrollID = getEnrollmentID(systemID, enrollmentId);
			return enrollID;
		}	
		return enrollmentId;
	}
	
	public static String getEnrollmentID(String sysCode, String id){
		return sysCode + "_" + id;
	}
	
	public static void setAcceptParams(HashMap<String,Object> json, HttpServletRequest request){	
		//AUDIT_CHECK
		//MATCH_LEVEL_3D
		//MATCH_LEVEL_ID
		//REPLAY_FACEMAP_CHECK
		
		//get values from db
		try {
			LinkedHashMap data = (LinkedHashMap) json.get("data");
			if(data!=null){
				data.put("auditCheck", request.getAttribute("auditCheck"));
				data.put("matchLevel3D", request.getAttribute("matchLevel3D"));
				data.put("matchLevelID", request.getAttribute("matchLevelID"));
				data.put("replayFacemapCheck", request.getAttribute("replayFacemapCheck"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static void sslOff(){
        //Turn off SSL checking
        try{
        	SSLUtil.turnOffSslChecking();
        }catch(Exception e){
        	e.printStackTrace();
        }
	}

}
