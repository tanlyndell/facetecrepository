package com.tera.facetec.zoom.datasource.config;


import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableAspectJAutoProxy
@MapperScan(value="com.tera.facetec.zoom.mapper.teradb", sqlSessionFactoryRef="teraDbSqlSessionFactory")
public class TeraDatabaseConfig {

    @Bean(name = "teraDbDataSource")
    @Primary
    @ConfigurationProperties(prefix = "spring.tera.datasource")
    public DataSource teraDbDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "teraDbSqlSessionFactory")
    @Primary
    public SqlSessionFactory teraDbSqlSessionFactory(@Qualifier("teraDbDataSource") DataSource teraDbDataSource, ApplicationContext applicationContext) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(teraDbDataSource);
        sqlSessionFactoryBean.setMapperLocations(applicationContext.getResources("classpath:mybatis/database/teradb/*.xml"));
        return sqlSessionFactoryBean.getObject();
    }

    @Bean(name = "teraDbTransactionManager")
    @Primary
    public DataSourceTransactionManager testTransactionManager(@Qualifier("teraDbDataSource") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = "teraDbSqlSessionTemplate")
    @Primary
    public SqlSessionTemplate teraDbSqlSessionTemplate(SqlSessionFactory teraDbSqlSessionFactory) throws Exception { 
        return new SqlSessionTemplate(teraDbSqlSessionFactory);
    }

}

