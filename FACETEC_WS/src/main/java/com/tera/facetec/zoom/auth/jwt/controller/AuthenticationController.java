/*package com.tera.facetec.zoom.auth.jwt.controller;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tera.facetec.zoom.auth.jwt.config.JwtTokenUtil;
import com.tera.facetec.zoom.auth.jwt.model.JWTResponse;
import com.tera.facetec.zoom.auth.jwt.model.LoginUser;
import com.tera.facetec.zoom.auth.jwt.model.User;
import com.tera.facetec.zoom.auth.jwt.service.UserService;
import com.tera.facetec.zoom.util.Helper;

@RestController
@RequestMapping("/token")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserService userService;
    


    @RequestMapping(value = "/generate-token", method = RequestMethod.POST)
    public JWTResponse register(@RequestBody LoginUser loginUser,@RequestHeader HttpHeaders headers) throws NoSuchAlgorithmException {
    	String newPassword = Helper.encryptOneWay(loginUser.getPassword(), loginUser.getUsername());
    	List<String> host = headers.get("host");
    	String headerHost = host.get(0);
    	
    	System.out.println(headers.get("host"));
    	
    	//Authentication authentication = authenticate(loginUser.getUsername(), newPassword);

    	
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUsername(),
                        newPassword
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final User user = userService.findOne(loginUser.getUsername());
        final String token = jwtTokenUtil.generateToken(user,headerHost);
        
        return new JWTResponse(token);
        
        //return ResponseEntity.ok(new AuthToken(token));
    }
    

}
*/