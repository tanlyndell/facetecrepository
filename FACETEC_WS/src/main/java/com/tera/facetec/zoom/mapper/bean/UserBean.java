package com.tera.facetec.zoom.mapper.bean;

import lombok.Data;

@Data
public class UserBean {

	private String usr_id;
	private String usr_rc;
	
	public String getUsr_id() {
		return usr_id;
	}
	public void setUsr_id(String usr_id) {
		this.usr_id = usr_id;
	}
	public String getUsr_rc() {
		return usr_rc;
	}
	public void setUsr_rc(String usr_rc) {
		this.usr_rc = usr_rc;
	}
	
	
	
}
