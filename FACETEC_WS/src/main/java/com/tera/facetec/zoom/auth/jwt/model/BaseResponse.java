package com.tera.facetec.zoom.auth.jwt.model;


public class BaseResponse {
    private String message;
    private Status status;
    private String refreshJwt;

    public enum Status {
        SUCCESS, ERROR
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

	public String getRefreshJwt() {
		return refreshJwt;
	}

	public void setRefreshJwt(String refreshJwt) {
		this.refreshJwt = refreshJwt;
	}
}

