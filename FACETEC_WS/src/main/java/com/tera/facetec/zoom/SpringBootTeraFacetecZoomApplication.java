package com.tera.facetec.zoom;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import tera.base.codered.CodeRedServlet;

//@SpringBootApplication/*(exclude = {SecurityFilterAutoConfiguration.class})*/
/*public class SpringBootTeraFacetecZoomApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootTeraFacetecZoomApplication.class, args);
	}

}
*/

//Deployment to WAR (tested on IBM WAS 9, not 8.5)
@ComponentScan("com.tera.facetec.zoom") 
@ComponentScan("tera.base.codered")  // for code red
/*@ImportResource("classpath:applicationContext.xml")*/
@SpringBootApplication
@EnableTransactionManagement
@EnableJpaAuditing
@EnableWebMvc
@EnableAutoConfiguration


/*(exclude = {org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class,
		org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration.class})*/
public class SpringBootTeraFacetecZoomApplication extends SpringBootServletInitializer implements BeanDefinitionRegistryPostProcessor {

   @Override
   protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
      return application.sources(SpringBootTeraFacetecZoomApplication.class);
   }
   
   public static void main(String[] args) {
       SpringApplication.run(SpringBootTeraFacetecZoomApplication.class, args);
   }
   
   
   /**For Code Red**/
   @Override
   public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
       registry.registerBeanDefinition("CodeRedServlet", new RootBeanDefinition(ServletRegistrationBean.class,
               () -> new ServletRegistrationBean<>(new CodeRedServlet(), "/CodeRedServlet/*")));
   }

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory arg0) throws BeansException {
		// TODO Auto-generated method stub
		
	}

   
}