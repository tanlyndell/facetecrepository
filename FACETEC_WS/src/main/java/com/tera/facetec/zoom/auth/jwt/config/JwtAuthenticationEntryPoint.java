/*package com.tera.facetec.zoom.auth.jwt.config;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

    @Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException authException) throws IOException {
    	
    	 final String expiredMsg = (String) request.getAttribute("expired");
    	 final String invalid = (String) request.getAttribute("invalid");
    	 final String illegal = (String) request.getAttribute("illegal");
    	 
    	 if(illegal != null) {
    		 //response.sendError(HttpServletResponse.SC_BAD_REQUEST, illegal);
    		 response.sendError(HttpServletResponse.SC_BAD_REQUEST, "illegal_argument");
    	 }else if(expiredMsg != null) {
        	 //response.sendError(HttpServletResponse.SC_UNAUTHORIZED, expiredMsg);
    		 response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "token_expired");
    	 }else if(invalid != null) {
        	 //response.sendError(HttpServletResponse.SC_FORBIDDEN, invalid);
    		 response.sendError(HttpServletResponse.SC_FORBIDDEN, "invalid_token");
    	 }else {
    		 response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "unauthorized");
    	 }
    	 
    	

        
    }
}*/