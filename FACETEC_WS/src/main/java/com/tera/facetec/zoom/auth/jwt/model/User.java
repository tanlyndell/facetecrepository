package com.tera.facetec.zoom.auth.jwt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
@Table(name = "tbl_sec_users_ws")
public class User {
	
	@Id
	@Column(name = "usr_id")
    private String usrId;
	
    @Column(name = "usr_first_name")
    private String usrFirstName;
    
    @Column(name = "usr_password")
    @JsonIgnore
    private String usrPassword;

	public String getUsrId() {
		return usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public String getUsrFirstName() {
		return usrFirstName;
	}

	public void setUsrFirstName(String usrFirstName) {
		this.usrFirstName = usrFirstName;
	}

	public String getUsrPassword() {
		return usrPassword;
	}

	public void setUsrPassword(String usrPassword) {
		this.usrPassword = usrPassword;
	}
   
    
    
}
