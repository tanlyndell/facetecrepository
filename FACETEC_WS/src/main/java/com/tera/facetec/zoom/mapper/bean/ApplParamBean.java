package com.tera.facetec.zoom.mapper.bean;


public class ApplParamBean {

	private String apCode;
	private String apDesc;
	private String apValue;
	private String checklist;
	private String groupInd;
	private String paramGrpCode;
	private String deleteInd;
	
	public String getApCode() {
		return apCode;
	}
	public String getApDesc() {
		return apDesc;
	}
	public String getApValue() {
		return apValue;
	}
	public String getChecklist() {
		return checklist;
	}
	public String getGroupInd() {
		return groupInd;
	}
	public String getParamGrpCode() {
		return paramGrpCode;
	}
	public void setApCode(String apCode) {
		this.apCode = apCode;
	}
	public void setApDesc(String apDesc) {
		this.apDesc = apDesc;
	}
	public void setApValue(String apValue) {
		this.apValue = apValue;
	}
	public void setChecklist(String checklist) {
		this.checklist = checklist;
	}
	public void setGroupInd(String groupInd) {
		this.groupInd = groupInd;
	}
	public void setParamGrpCode(String paramGrpCode) {
		this.paramGrpCode = paramGrpCode;
	}
	public String getDeleteInd() {
		return deleteInd;
	}
	public void setDeleteInd(String deleteInd) {
		this.deleteInd = deleteInd;
	}
}
