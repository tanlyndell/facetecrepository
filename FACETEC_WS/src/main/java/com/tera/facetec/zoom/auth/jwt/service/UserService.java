package com.tera.facetec.zoom.auth.jwt.service;

import com.tera.facetec.zoom.auth.jwt.model.User;

public interface UserService {
    User findOne(String username);
}
