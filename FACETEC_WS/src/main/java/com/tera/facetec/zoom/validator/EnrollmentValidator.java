package com.tera.facetec.zoom.validator;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class EnrollmentValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		System.out.println("EnrollmentValidator clazz: " + clazz);
		if(LinkedHashMap.class.equals(clazz)){
			System.out.println("EnrollmentValidator clazz true");
			return true;
		}
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		System.out.println("EnrollmentValidator target: " + target);
		 if(target instanceof LinkedHashMap) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "enrollmentIdentifier", "mandatory");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "faceMap", "mandatory");
		 }
		
	 } 

}

