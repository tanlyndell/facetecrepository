/*package com.tera.facetec.zoom.audit;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import lombok.extern.slf4j.Slf4j;

@Aspect
@Component
@Slf4j
public class AuditAspect {

    private final AuditService auditService;

    @Autowired
    public AuditAspect(AuditService auditService) {
        this.auditService = auditService;
    }

    
    @Before("execution(* com.tera.facetec.zoom.auth.jwt.service.impl.*.*(..))")
	public void logBefore(JoinPoint joinPoint) {
    	log.info("Before AUDIT: action occurred: {}");
	}
    
    @After("execution(* com.tera.facetec.zoom.auth.jwt.service.impl.*.*(..))")
	public void logAfter(JoinPoint joinPoint) {
    	log.info("After AUDIT: action occurred: {}");
	}
   
    
    @Around("execution(* com.tera.facetec.zoom.auth.jwt.service.impl.*.*(..))")
    public Object audit(ProceedingJoinPoint joinPoint) throws Throwable {
        boolean ok = false;
        try {
            Object o = joinPoint.proceed();
            ok = true;
            
            long start = System.currentTimeMillis();
            long executionTime = System.currentTimeMillis() - start;

            System.out.println(joinPoint.getSignature() + "Query executed in " + executionTime + "ms");
            
            return o;
        } finally {
            //TODO add @AfterThrows advice
            if (ok) {
            	 log.info("AUDIT: action occurred: {}", "GOOD");
            	//System.out.println(joinPoint.getSignature() + " OK");
                //auditService.auditEvent(auditableAnnotation.auditActionType());
            } else {
            	//System.out.println(joinPoint.getSignature() + " NOT OK");
            	log.error("AUDIT: error occurred: {}", "Rollback");
            	TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();    		 
                //auditService.auditErrorEvent(auditableAnnotation.auditActionType());
            }
        }
    }
}
*/