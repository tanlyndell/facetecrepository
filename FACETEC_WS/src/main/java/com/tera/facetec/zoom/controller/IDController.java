package com.tera.facetec.zoom.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.tera.facetec.zoom.implem.ParamImplem;
import com.tera.facetec.zoom.mapper.bean.ApplParamBean;
import com.tera.facetec.zoom.util.Helper;

@RestController
public class IDController implements EnvironmentAware{

	@Autowired
	public static Environment env;

	@Autowired
	ParamImplem paramImplem;
	
	@CrossOrigin
    @PostMapping(value = "/id-check", produces = "application/json")
    public HashMap<String,Object> checkID(
    		HttpServletRequest httpRequest,
    		@RequestHeader("X-Device-License-Key") String license,
    		@RequestHeader("systemID") String systemID,
    		@RequestBody Map<String,Object> request) {
    	
    	//Get URI
    	String uri = env.getProperty("zoom.server") + env.getProperty("uri.idCheck");

    	//Initiate Rest template
    	SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(Integer.parseInt(env.getProperty("connect.timeout")));
        factory.setReadTimeout(Integer.parseInt(env.getProperty("read.timeout")));
        RestTemplate restTemplate = new RestTemplate(factory);
         
        //Set HTTP headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("X-Device-License-Key", license);

        //Create map for post parameters
        Map<String, Object> map = new HashMap<>();
        Helper.setEnrollmentID(systemID, request);
        map.putAll(request);

        //Build the request
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
         
        //Turn off SSL Checking
        String offSslCheck = env.getProperty("offSslCheck");
        if("Y".equals(offSslCheck))
        	Helper.sslOff();
        
        //Get result
        ResponseEntity<HashMap> result = restTemplate.postForEntity(uri, entity, HashMap.class);
        HashMap<String,Object> json = (HashMap<String,Object>)result.getBody();
        Helper.setAcceptParams(json, httpRequest);
        return json;
    }
    
	@Override
	public void setEnvironment(Environment arg0) {
		// TODO Auto-generated method stub
		env = arg0;
	}

	
/**REQUEST**/
/*	var dataToUpload = {
  "sessionId": zoomSessionResult.sessionId,
  "enrollmentIdentifier": INSERT_ENROLLMENT_IDENTIFIER_HERE,
  "idScan": INSERT_ID_SCAN_HERE,
  "idScanFrontImage": INSERT_ID_SCAN_FRONT_IMAGE_HERE,
  "idScanBackImage": INSERT_ID_SCAN_BACK_IMAGE_HERE
}*/
	

/**RESPONSE**/
/*	<!-- Liveness passed -->
{
    "meta": {
        "ok": true,
        "code": 200,
        "mode": "dev",
        "message": "The ID Check was processed successfully."
    },
    "data": {
        "faceMapAgeEstimateGroup": 4
        "glasses": true
        "idScanAgeEstimateGroup": 4
        "isLowQuality": false
        "isReplayFaceMap": false
        "livenessStatus": 0
        "matchLevel": 0
    }
}*/
	
	
	
}