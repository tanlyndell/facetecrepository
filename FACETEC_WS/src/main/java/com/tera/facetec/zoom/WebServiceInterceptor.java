package com.tera.facetec.zoom;

import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tera.facetec.zoom.implem.ParamImplem;
import com.tera.facetec.zoom.implem.UserImplem;
import com.tera.facetec.zoom.mapper.bean.ApplParamBean;
import com.tera.facetec.zoom.mapper.bean.SystemCredentialsBean;
import com.tera.facetec.zoom.util.Helper;

public class WebServiceInterceptor implements HandlerInterceptor {

	public static final String BEARER_IDENTIFIER = "Bearer ";
	
	@Autowired
	UserImplem userImplem;
	
	@Autowired
	ParamImplem paramImplem;
	
	public WebServiceInterceptor(){
		
	}
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		System.out.println("PRE HANDLE.");
		
		String controller;
    	if (handler instanceof HandlerMethod){
			HandlerMethod handlerMethod = (HandlerMethod)handler;
			controller = handlerMethod.getBeanType().getName();
		}
    	else{
        	controller = handler.getClass().getName();
    	}  	

    	System.out.println("CONTROLLER: " + controller);
    	
		//Set default invalid response
		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, Object> returnMap = new HashMap<>();
		returnMap.put("status", 400);
		returnMap.put("error", "Bad Request");
		returnMap.put("message", "Missing system request headers");
		
		//check if system credentials exist in header
		String sysId = request.getHeader("systemID");
		String sysUser = request.getHeader("systemUser");
		String sysPass = request.getHeader("systemPass");
		String ipAddress = request.getHeader("ipAddress");
		
		if(ipAddress == null)
			ipAddress = Helper.getIpAddress(request);
		
		String uri = request.getRequestURI();
		
		System.out.println(uri);
		System.out.println(sysId + " " + sysUser + " " + sysPass + " " + ipAddress);

		if(sysId == null || sysUser == null || sysPass == null || ipAddress == null){
			
			if(uri.indexOf("CodeRed")>-1 || uri.indexOf("/resources/")>-1){
				System.out.println("Proceed with controller");
				return true;
			}
			
			response.setContentType("application/json");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().write(mapper.writeValueAsString(returnMap));
	        return false;
		}
		
		HashMap<String, Object> values = new HashMap<>();
		values.put("systemID", sysId);
		values.put("systemUser", sysUser);
		values.put("systemPass", sysPass);
		values.put("ipAddress", ipAddress);
		
		boolean valid = isValidSystemCredentials(values);

	    if(!valid) {
	    	returnMap.put("message", "Invalid system parameters");
			response.setContentType("application/json");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().write(mapper.writeValueAsString(returnMap));
	        return false;
		} else {
			
			//Check if device license key header is provided
			String deviceLicenseKey = request.getHeader("X-Device-License-Key");
			if(deviceLicenseKey == null){
				returnMap.put("message", "Missing request header 'X-Device-License-Key' for method parameter of type String");
				response.setContentType("application/json");
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				response.getWriter().write(mapper.writeValueAsString(returnMap));
				return false;
			}
			
			//Set default parameters
			setAcceptParams(request);
			
			System.out.println("VALID Credentials. Proceed with controller");
			return true;
		}
 
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("POST HANDLE.");
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}
	
	private boolean isTransactionForLogging(String tranCode)
	{
		/*if(TeraBaseInitServlet.hTransactionInd != null && 
				  (TeraBaseInitServlet.hTransactionInd.get(tranCode) != null && 
				   TeraBaseInitServlet.hTransactionInd.get(tranCode).getJournal_ind().trim().equalsIgnoreCase("Y")))
		  	return true;
		else
			return false;*/
		return true;
	}
	
/*	private void logTrans(HashMap values)
	{
		TransaxnDaoInterface transactionDoer = (TransaxnDaoInterface)ObjectFactory.getObject("TransaxnDoer");
		transactionDoer.exempt_insertTransaxnLog(values);
		
		 Added by NSV 20140220: Add to Pending Transactions Log if Pending and for Transaction Verification 
		if(((String)values.get("status")).equals("Pending"))
		{
			if(values.get("PENDING_BILLS_NO") != null)
				values.put("DEST_ACCT_NO", values.get("PENDING_BILLS_NO"));
			
			transactionDoer.exempt_insertTransaxnLog(values);
		}
	}*/
	
	public boolean isValidSystemCredentials(HashMap<String, Object> values){
		//needs systemID, systemUser, systemPass
		//to put sysCode, usrID, usrPassword
		
		boolean valid = false;
		
		values.put("sysCode", (String)values.get("systemID"));
		values.put("usrID", (String)values.get("systemUser"));
		
		String password = (String)values.get("systemPass");
        SystemCredentialsBean sysCreBean = null;
        try{
        	sysCreBean = userImplem.getSystemCredentialDetails(values);	
        }catch(Exception e){
        	e.printStackTrace();
        }
		
		if(sysCreBean!=null && password!=null){
			if(sysCreBean.getIpAddress().equals((String)values.get("ipAddress"))) {
				String decryptedPass = Helper.getDecryptedPassword(password);
				String decryptedUserPass = Helper.getDecryptedPassword(sysCreBean.getUsrPassword());
				if(decryptedUserPass.equals(decryptedPass))
					valid = true;
			}	
		}
		
		return valid;
		
	}
	
	public String getParamValue(String code){		
		//AUDIT_CHECK
		//MATCH_LEVEL_3D
		//MATCH_LEVEL_ID
		//REPLAY_FACEMAP_CHECK

		HashMap<String, Object> values = new HashMap<String, Object>();
		values.put("AP_CODE", code);

        ApplParamBean paramBean = null;
        try{
        	paramBean = paramImplem.getParam(values);
        }catch(Exception e){
        	e.printStackTrace();
        }	
        
        return paramBean.getApValue();
	}
	
	public void setAcceptParams(HttpServletRequest request){	
		//AUDIT_CHECK
		//MATCH_LEVEL_3D
		//MATCH_LEVEL_ID
		//REPLAY_FACEMAP_CHECK
		
		//get values from db
		try {
			request.setAttribute("auditCheck", /*"N"*/getParamValue("AUDIT_CHECK"));
			request.setAttribute("matchLevel3D", /*"0"*/getParamValue("MATCH_LEVEL_3D"));
			request.setAttribute("matchLevelID", /*"0"*/getParamValue("MATCH_LEVEL_ID"));
			request.setAttribute("replayFacemapCheck", /*"N"*/getParamValue("REPLAY_FACEMAP_CHECK"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	
}
