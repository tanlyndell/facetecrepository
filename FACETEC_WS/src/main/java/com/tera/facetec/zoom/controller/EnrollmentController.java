package com.tera.facetec.zoom.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.tera.facetec.zoom.implem.ParamImplem;
import com.tera.facetec.zoom.implem.UserImplem;
import com.tera.facetec.zoom.mapper.bean.ApplParamBean;
import com.tera.facetec.zoom.mapper.bean.SystemCredentialsBean;
import com.tera.facetec.zoom.util.Helper;
import com.tera.facetec.zoom.validator.EnrollmentValidator;

@RestController
public class EnrollmentController implements EnvironmentAware{

	@Autowired
	public static Environment env;

	@Autowired
	UserImplem userImplem;

	@Autowired
	ParamImplem paramImplem;
	
/*	@InitBinder
	protected void initBinder(WebDataBinder binder){
		binder.setValidator(new EnrollmentValidator());
	}*/
	
	@CrossOrigin
    @PostMapping(value = "/search", produces = "application/json")
    public HashMap<String,Object> search(
    		HttpServletRequest httpRequest,
    		@RequestHeader("X-Device-License-Key") String license,
    		@RequestHeader("systemID") String systemID,
    		@RequestBody Map<String,Object> request) {
    	
    	//Get URI
    	String uri = env.getProperty("zoom.server") + env.getProperty("uri.search");
    	
    	//Initiate Rest template
    	SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(Integer.parseInt(env.getProperty("connect.timeout")));
        factory.setReadTimeout(Integer.parseInt(env.getProperty("read.timeout")));
        RestTemplate restTemplate = new RestTemplate(factory);
         
        //Set HTTP headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("X-Device-License-Key", license);

        //Create map for post parameters
        Map<String, Object> map = new HashMap<>();
        Helper.setEnrollmentID(systemID, request);
        map.putAll(request);
        
        //Build the request
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
         
        //Turn off SSL Checking
        String offSslCheck = env.getProperty("offSslCheck");
        if("Y".equals(offSslCheck))
        	Helper.sslOff();
        
        //Get result
        ResponseEntity<HashMap> result = restTemplate.postForEntity(uri, entity, HashMap.class);
        HashMap<String,Object> json = (HashMap<String,Object>)result.getBody();
        Helper.setAcceptParams(json, httpRequest);

        return json;
    }
    
	@CrossOrigin
    @PostMapping(value = "/enrollment", produces = "application/json")
    public HashMap<String, Object> enroll(
    		HttpServletRequest httpRequest,
    		@RequestHeader("X-Device-License-Key") String license,
    		@RequestHeader("systemID") String systemID,
    		@RequestBody Map<String,Object> request) {
    	
		//TRY FOR SYSTEM CREDENTIALS
        //TESTING TXN_SYSTEM_CREDENTIALS
/*        HashMap<String, Object> values = new HashMap<>();
        values.putAll(request);

        boolean validCredentials = isValidSystemCredentials(values);
		System.out.println("IS VALID CREDENTIALS: " + validCredentials);*/
		//TRY
		
		
    	//Get URI
    	String uri = env.getProperty("zoom.server") + env.getProperty("uri.enroll");

    	//Initiate Rest template
    	SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(Integer.parseInt(env.getProperty("connect.timeout")));
        factory.setReadTimeout(Integer.parseInt(env.getProperty("read.timeout")));
        RestTemplate restTemplate = new RestTemplate(factory);
         
        //Set HTTP headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("X-Device-License-Key", license);

        //Create map for post parameters
        Map<String, Object> map = new HashMap<>();
        Helper.setEnrollmentID(systemID, request);
        map.putAll(request);

        //Build the request
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
         
        //Get result
        //ResponseEntity<String> result = restTemplate.postForEntity(uri, entity, String.class);
        //return result.getBody();
        
        //Turn off SSL Checking
        String offSslCheck = env.getProperty("offSslCheck");
        if("Y".equals(offSslCheck))
        	Helper.sslOff();
        
        ResponseEntity<HashMap> result = restTemplate.postForEntity(uri, entity, HashMap.class);
        HashMap<String,Object> json = (HashMap<String,Object>)result.getBody();
        Helper.setAcceptParams(json, httpRequest);

        return json;
        
        
    }
 
	@CrossOrigin
    @GetMapping(value = "/enrollment/{enrollmentIdentifier}", produces = "application/json")
    public HashMap<String,Object> getEnrolled(
    		HttpServletRequest httpRequest,
    		@RequestHeader("X-Device-License-Key") String license,
    		@RequestHeader("systemID") String systemID,
    		@PathVariable("enrollmentIdentifier") String enrollmentIdentifier) {
    	
    	//Get URI
    	String uri = env.getProperty("zoom.server") + env.getProperty("uri.enroll");
 
    	uri = uri + "/" + Helper.setEnrollmentID(systemID, enrollmentIdentifier) + "?return=faceMap,auditTrailImage";

    	//Initiate Rest template
    	SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(Integer.parseInt(env.getProperty("connect.timeout")));
        factory.setReadTimeout(Integer.parseInt(env.getProperty("read.timeout")));
        RestTemplate restTemplate = new RestTemplate(factory);
         
        //Set HTTP headers
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("X-Device-License-Key", license);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
         
        //Turn off SSL Checking
        String offSslCheck = env.getProperty("offSslCheck");
        if("Y".equals(offSslCheck))
        	Helper.sslOff();
        
        //Get result
        ResponseEntity<HashMap> result = restTemplate.exchange(uri, HttpMethod.GET, entity, HashMap.class);
        HashMap<String,Object> json = (HashMap<String,Object>)result.getBody();
        //Helper.setAcceptParams(json, httpRequest);

        return json;
    }
    
	@CrossOrigin
    @DeleteMapping(value = "/enrollment/{enrollmentIdentifier}", produces = "application/json")
    public HashMap<String,Object> deleteEnrolled(
    		HttpServletRequest httpRequest,
    		@RequestHeader("X-Device-License-Key") String license,
    		@RequestHeader("systemID") String systemID,
    		@PathVariable("enrollmentIdentifier") String enrollmentIdentifier) {
    	
    	//Get URI
    	String uri = env.getProperty("zoom.server") + env.getProperty("uri.enroll");
    	
    	uri = uri + "/" + Helper.setEnrollmentID(systemID, enrollmentIdentifier);

    	//Initiate Rest template
    	SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(Integer.parseInt(env.getProperty("connect.timeout")));
        factory.setReadTimeout(Integer.parseInt(env.getProperty("read.timeout")));
        RestTemplate restTemplate = new RestTemplate(factory);
         
        //Set HTTP headers
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("X-Device-License-Key", license);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
         
        //Turn off SSL Checking
        String offSslCheck = env.getProperty("offSslCheck");
        if("Y".equals(offSslCheck))
        	Helper.sslOff();
        
        //Get result
        ResponseEntity<HashMap> result = restTemplate.exchange(uri, HttpMethod.DELETE, entity, HashMap.class);
        HashMap<String,Object> json = (HashMap<String,Object>)result.getBody();
        //Helper.setAcceptParams(json, httpRequest);
        
        return json;
    }
    
    
	@Override
	public void setEnvironment(Environment arg0) {
		// TODO Auto-generated method stub
		env = arg0;
	}
	
/**REQUEST POST SEARCH**/
/*	var dataToUpload = {
			  "sessionId": zoomSessionResult.sessionId,
			  "enrollmentIdentifier": INSERT_ENROLLMENT_IDENTIFIER_HERE,
			  "auditTrailImage": zoomSessionResult.faceMetrics.getAuditTrailBase64JPG()[0]
			}*/
	
/**RESPONSE POST SEARCH**/
/*	{
	    "meta": {
	        "ok": true,
	        "code": 200,
	        "mode": "dev",
	        "message": "The search request was processed successfully."
	    },
	    "data": {
	        "results": [
	            {
	                "enrollmentIdentifier": "foo",
	                "matchLevel": "10",
	                "auditTrailImage": "..."
	            },
	            {
	                "enrollmentIdentifier": "bar",
	                "matchLevel": "10",
	                "auditTrailImage": ""
	            }
	        ],
	        "sourceFaceMap": {
	            "isReplayFaceMap": false
	        }
	    }
	}*/	
	
	
	
	
/**REQUEST POST ENROLLMENT**/
/*	var dataToUpload = {
			  "sessionId": zoomSessionResult.sessionId,
			  "enrollmentIdentifier": INSERT_ENROLLMENT_IDENTIFIER_HERE
			  "faceMap": zoomSessionResult.faceMetrics.faceMap,
			  "auditTrailImage": zoomSessionResult.faceMetrics.getAuditTrailBase64JPG()[0]
			}*/

/**RESPONSE POST ENEROLLMENT**/
/*	{
	    "meta": {
	        "ok": true,
	        "code": 200,
	        "mode": "dev",
	        "message": "The FaceMap was successfully enrolled."
	    },
	    "data": {
	        "createdDate": "2019-09-16T17:30:40+00:00",
	        "enrollmentIdentifier": "foo",
	        "faceMapType": 0,
	        "glasses": false,
	        "isEnrolled": true,
	        "isLowQuality": false,
	        "isReplayFaceMap": false,
	        "livenessStatus": 0,
	    }
	}	*/
	
	
/**REQUEST GET ENROLLMENT**/	
	
	//xhr.open("GET", "https://api.zoomauth.com/api/v2/biometrics/enrollment/INSERT_ENROLLMENT_IDENTIFIER_HERE?return=faceMap,auditTrailImage");	
	
	
/**RESPONSE GET ENROLLMENT**/	
/*	{
	    "meta": {
	        "ok": true,
	        "code": 200,
	        "mode": "dev",
	        "message": "A FaceMap was found for that enrollmentIdentifier."
	    },
	    "data": {
	        "enrollmentIdentifier": "foo",
	        "createDate": "2017-01-01T00:00:00+00:00",
	        "auditTrailImage": "...",
	        "faceMap": "...",
	        "faceMapType": 0
	    }
	}*/
	
	
/**REQUEST DELETE ENROLLMENT**/
	//xhr.open("DELETE", "https://api.zoomauth.com/api/v2/biometrics/enrollment/INSERT_ENROLLMENT_IDENTIFIER_HERE");
	
/**RESPONSE DELETE ENROLLMENT**/
/*	<!-- enrollmentIdentifier found in database -->
	{
	    "meta": {
	        "ok": true,
	        "code": 200,
	        "mode": "dev",
	        "message": "The entry in the database for this enrollmentIdentifier was successfully deleted."
	    },
	    "data": null
	}

	<!-- enrollmentIdentifier not found in database -->
	{
	    "meta": {
	        "ok": true,
	        "code": 200,
	        "mode": "dev",
	        "message": "No entry found in the database for this enrollmentIdentifier."
	    },
	    "data": null
	}*/
	
/*	public boolean isValidSystemCredentials(HashMap<String, Object> values){
		//needs systemID, systemUser, systemPass
		//to put sysCode, usrID, usrPassword
		
		boolean valid = false;
		
		values.put("sysCode", (String)values.get("systemID"));
		values.put("usrID", (String)values.get("systemUser"));
		
		String password = (String)values.get("systemPass");
        SystemCredentialsBean sysCreBean = null;
        try{
        	sysCreBean = userImplem.getSystemCredentialDetails(values);	
        }catch(Exception e){
        	e.printStackTrace();
        }
        
		
		if(sysCreBean!=null && password!=null){
			if(sysCreBean.getIpAddress().equals((String)values.get("ipAddress"))) {
				String decryptedPass = Helper.getDecryptedPassword(password);
				String decryptedUserPass = Helper.getDecryptedPassword(sysCreBean.getUsrPassword());
				if(decryptedUserPass.equals(decryptedPass))
					valid = true;
			}	
		}
		
		return valid;
		
	}*/
	
	
}