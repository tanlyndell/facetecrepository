package com.tera.facetec.zoom;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.PathResourceResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import tera.base.codered.CodeRedInterceptor;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Bean
    public WebServiceInterceptor webServiceInterceptor() {
        return new WebServiceInterceptor();
    }

    @Bean
    public CodeRedInterceptor codeRedInterceptor() {
        return new CodeRedInterceptor();
    }
    
    public @Override void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(webServiceInterceptor()).excludePathPatterns("/resources/**").excludePathPatterns("/favicon/**").excludePathPatterns("/error/**");
        registry.addInterceptor(codeRedInterceptor()).excludePathPatterns("/resources/**").excludePathPatterns("/favicon/**").excludePathPatterns("/error/**");
    }
    
    /**START: For Code Red**/
    @Bean
    public ViewResolver getViewResolver() {
        InternalResourceViewResolver resolver
          = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        resolver.setViewClass(JstlView.class);
        return resolver;
    }
 
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
 
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**")
          .addResourceLocations("/resources/").setCachePeriod(3600)
          .resourceChain(true).addResolver(new PathResourceResolver());
    }
    /**END: For Code Red**/
    
}