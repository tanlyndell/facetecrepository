package com.tera.facetec.zoom.auth.jwt.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tera.facetec.zoom.auth.jwt.model.User;

@Repository
public interface UserDao extends CrudRepository <User, Long> {
    User findByUsrId(String usrId);
}
