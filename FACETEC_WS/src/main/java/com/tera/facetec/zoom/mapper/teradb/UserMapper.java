package com.tera.facetec.zoom.mapper.teradb;

import java.util.HashMap;
import java.util.List;

import com.tera.facetec.zoom.mapper.bean.SystemCredentialsBean;
import com.tera.facetec.zoom.mapper.bean.UserBean;


public interface UserMapper {

	public List<UserBean> getUsers(HashMap<String,Object> values);
	public SystemCredentialsBean getSystemCredentialDetails(HashMap<String, Object> values);
}

