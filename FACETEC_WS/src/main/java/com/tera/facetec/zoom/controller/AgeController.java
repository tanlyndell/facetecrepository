package com.tera.facetec.zoom.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.tera.facetec.zoom.implem.ParamImplem;
import com.tera.facetec.zoom.mapper.bean.ApplParamBean;
import com.tera.facetec.zoom.util.Helper;

@RestController
public class AgeController implements EnvironmentAware{

	@Autowired
	public static Environment env;
	
	@Autowired
	ParamImplem paramImplem;
	
    @PostMapping(value = "/age-check", produces = "application/json")
    public HashMap<String,Object> checkAge(
    		HttpServletRequest httpRequest,
    		@RequestHeader("X-Device-License-Key") String license,
    		@RequestHeader("systemID") String systemID,
    		@RequestBody Map<String,Object> request) {
    	
    	//Get URI
    	String uri = env.getProperty("zoom.server") + env.getProperty("uri.ageCheck");

    	//Initiate Rest template
    	SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(Integer.parseInt(env.getProperty("connect.timeout")));
        factory.setReadTimeout(Integer.parseInt(env.getProperty("read.timeout")));
        RestTemplate restTemplate = new RestTemplate(factory);
         
        //Set HTTP headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("X-Device-License-Key", license);

        //Create map for post parameters
        Map<String, Object> map = new HashMap<>();
        Helper.setEnrollmentID(systemID, request);
        map.putAll(request);

        //Build the request
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
         
        //Turn off SSL Checking
        String offSslCheck = env.getProperty("offSslCheck");
        if("Y".equals(offSslCheck))
        	Helper.sslOff();
        
        //Get result
        ResponseEntity<HashMap> result = restTemplate.postForEntity(uri, entity, HashMap.class);
        HashMap<String,Object> json = (HashMap<String,Object>)result.getBody();
        Helper.setAcceptParams(json, httpRequest);
        return json;
    }
 
    @PostMapping(value = "/estimate-age", produces = "application/json")
    public HashMap<String,Object> estimateAge(
    		HttpServletRequest httpRequest,
    		@RequestHeader("X-Device-License-Key") String license,
    		@RequestHeader("systemID") String systemID,
    		@RequestBody Map<String,Object> request) {
    	
    	//Get URI
    	String uri = env.getProperty("zoom.server") + env.getProperty("uri.estimateAge");

    	//Initiate Rest template
    	SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(Integer.parseInt(env.getProperty("connect.timeout")));
        factory.setReadTimeout(Integer.parseInt(env.getProperty("read.timeout")));
        RestTemplate restTemplate = new RestTemplate(factory);
         
        //Set HTTP headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("X-Device-License-Key", license);

        //Create map for post parameters
        Map<String, Object> map = new HashMap<>();
        Helper.setEnrollmentID(systemID, request);
        map.putAll(request);

        //Build the request
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
         
        //Turn off SSL Checking
        String offSslCheck = env.getProperty("offSslCheck");
        if("Y".equals(offSslCheck))
        	Helper.sslOff();
        
        //Get result
        ResponseEntity<HashMap> result = restTemplate.postForEntity(uri, entity, HashMap.class);
        HashMap<String,Object> json = (HashMap<String,Object>)result.getBody();
        Helper.setAcceptParams(json, httpRequest);
        return json;
    }
    
	@Override
	public void setEnvironment(Environment arg0) {
		// TODO Auto-generated method stub
		env = arg0;
	}

/**REQUEST AGE CHECK**/
/*	var dataToUpload = {
  "sessionId": zoomSessionResult.sessionId,
  "enrollmentIdentifier": INSERT_ENROLLMENT_IDENTIFIER_HERE,
  "targetAge": INSERT_TARGET_AGE_GROUP_HERE,
  "auditTrailImage": zoomSessionResult.faceMetrics.getAuditTrailBase64JPG()[0]
}*/
	

/**RESPONSE AGE CHECK**/
/*	{
    "meta": {
        "ok": true,
        "code": 200,
        "mode": "dev",
        "message": "The ZoOm 3D FaceMap evaluated and Liveness was proven."
    },
    "data": {
        "ageCheckResult": 0
    }
}*/
	
	
/**REQUEST ESTIMATE AGE**/
/*	var dataToUpload = {  
			  "enrollmentIdentifier": INSERT_ENROLLMENT_IDENTIFIER_HERE
			}	*/
	
/**RESPONSE ESTIMATE AGE**/
/*	{
	    "meta": {
	        "ok": true,
	        "code": 200,
	        "mode": "dev",
	        "message": "The ZoOm 3D FaceMap evaluated and Liveness was proven."
	    },
	    "data": {
	        "ageEstimateGroup": 4
	    }
	}*/	
	
	
	
}