package tera.base;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class ObjectFactory {
	static ApplicationContextProvider applicationContextProvider = new ApplicationContextProvider();
	public static BeanFactory factory = (BeanFactory)new ClassPathXmlApplicationContext("applicationContext.xml");
	//public static BeanFactory factory = null;
	
	public static Object getObject(String beanName)
	{
		//ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		//BeanFactory factory = (BeanFactory)context;

		//factory = applicationContextProvider.getApplicationContext();
				
		return factory.getBean(beanName);
	}
	
}
