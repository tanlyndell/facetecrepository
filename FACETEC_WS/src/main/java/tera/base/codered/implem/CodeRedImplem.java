package tera.base.codered.implem;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;

import tera.base.codered.bean.CodeRedBean;
import tera.base.codered.daointerface.CodeRedDaoInterface;

public class CodeRedImplem extends SqlSessionDaoSupport implements CodeRedDaoInterface  {	
	
	public void insertCodeRed(List<CodeRedBean> list){
		getSqlSession().insert("insertCodeRed",list);
	}
	
	
	public List<CodeRedBean> getCodeRedLog(HashMap values) {
		HttpServletRequest request = ((HttpServletRequest)values.get("Request"));
		List<CodeRedBean> beanList;

		if(request.getSession().getAttribute("fromCodeRedLoginAll") != null){
			SqlSessionFactory sqlSessionFactory = (SqlSessionFactory)request.getSession().getAttribute("cbSqlSessionFactory");
			SqlSession codeRedSqlSession = new SqlSessionTemplate(sqlSessionFactory);
			beanList = codeRedSqlSession.selectList("getCodeRedLog",values);
		}

		else{
			beanList = getSqlSession().selectList("getCodeRedLog", values);
		}

		return beanList;
//		return getSqlSession().selectList("getCodeRedLog", values);

	}
			
	public int getCodeRedLogCount(HashMap values){
		HttpServletRequest request = ((HttpServletRequest)values.get("Request"));
		int count;

		if(request.getSession().getAttribute("fromCodeRedLoginAll") != null){
			SqlSessionFactory sqlSessionFactory = (SqlSessionFactory)request.getSession().getAttribute("cbSqlSessionFactory");
			SqlSession codeRedSqlSession = new SqlSessionTemplate(sqlSessionFactory);
			count = codeRedSqlSession.selectOne("getCodeRedLogCount",values);
		}
		else{
			count = getSqlSession().selectOne("getCodeRedLogCount",values);
		}
		return count;

//		return (Integer)getSqlSession().selectOne("getCodeRedLogCount",values);
	}
	
	public List<CodeRedBean> getCodeRedLogLast(HashMap values) {
		HttpServletRequest request = ((HttpServletRequest)values.get("Request"));
		List<CodeRedBean> beanList;

		if(request.getSession().getAttribute("fromCodeRedLoginAll") != null){
			SqlSessionFactory sqlSessionFactory = (SqlSessionFactory)request.getSession().getAttribute("cbSqlSessionFactory");
			SqlSession codeRedSqlSession = new SqlSessionTemplate(sqlSessionFactory);
			beanList = codeRedSqlSession.selectList("getCodeRedLogLast",values);
		}
		else{
			beanList = getSqlSession().selectList("getCodeRedLogLast",values);
		}
		
//		List<CodeRedBean> beanList = getSqlSession().selectList("getCodeRedLogLast",values);

		
		return beanList;
	}
	
	@Override
	public HashMap getTable(HashMap values) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
