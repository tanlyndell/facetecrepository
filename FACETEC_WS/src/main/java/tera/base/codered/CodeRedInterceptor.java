package tera.base.codered;

import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import tera.base.bean.UserBean;
import tera.base.codered.bean.CodeRedBean;

public class CodeRedInterceptor implements HandlerInterceptor {
	
	/**
	 * This is a HandlerInterceptor callback method that is called 
	 * once the handler is executed and view is rendered.
	 */
	@Override
	public void afterCompletion(HttpServletRequest req, HttpServletResponse resp, Object handler, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
	}
	/**
	 * This HandlerInterceptor interceptor method is called when HandlerAdapter 
	 * has invoked the handler but DispatcherServlet is yet to render the view
	 */
	@Override
	public void postHandle(HttpServletRequest req, HttpServletResponse resp, Object arg2, ModelAndView handler)
			throws Exception {
		// TODO Auto-generated method stub
		String url = req.getRequestURI();
		
		if(!url.contains("CodeRed")){
			String sessId = req.getSession().getId();
			if(req.getAttribute("OLDSESS") != null)
				sessId = (String)req.getAttribute("OLDSESS");
			long time = Calendar.getInstance().getTimeInMillis();
			Date datetime = new Date(time);
			
			/** Start: Additional fields for MBA **/
			Long[] mfResponse = (Long[])req.getAttribute("code_red_mainframe_response_time");
					// Added v3.2 by NSV 20160721: Log mainframe response time in Code Red
			Long mainframeResponseTime1 = null;
			Long mainframeResponseTime2 = null;
					
			if(mfResponse != null)
			{
				mainframeResponseTime1 = mfResponse[0];
				
				if(mfResponse.length > 1)
				{
					mainframeResponseTime2 = mfResponse[1];
				}
			}
			/** End: Additional fields for MBA **/	
					
			if(req.getAttribute("crb") != null){
				CodeRedBean pending = (CodeRedBean)req.getAttribute("crb");
				if(pending.getUsrId() == null){
					UserBean uBean = (UserBean) req.getSession().getAttribute("userBean");
					if(uBean != null)
						pending.setUsrId(uBean.getUsrID() != null?uBean.getUsrID():(String)req.getAttribute("txnLog_userId"));
					else
						pending.setUsrId((String)req.getAttribute("txnLog_userId"));
				}
				if(pending.getTransactionCode() == null){
					String txnCode = req.getParameter("transactionCode");
					
					if(txnCode == null)
						txnCode = (String)req.getSession().getAttribute("transactionCode");
					
					if(txnCode == null)
						txnCode = (String)req.getAttribute("transactionCode");
					
					pending.setTransactionCode(txnCode);
				}
				pending.setOutboundTime(time);
				pending.setResponseTime(pending.getOutboundTime()-pending.getInboundTime());
				pending.setMainframeResponseTime1(mainframeResponseTime1);
				pending.setMainframeResponseTime2(mainframeResponseTime2);
				RequestHub.getInstance().addOutboundActivity(pending);
			}else{
				String method = String.valueOf(req.getMethod().charAt(0));
							
				CodeRedBean crb = new CodeRedBean(sessId, url, method,
						null, time, mainframeResponseTime1, mainframeResponseTime2);
				
				UserBean uBean = (UserBean) req.getSession().getAttribute("userBean");
				String ipAddress = req.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null)
				    ipAddress = req.getHeader("HTTP_X_FORWARDED_FOR");
				if (ipAddress == null)
				    ipAddress = req.getRemoteAddr();
				if (ipAddress.equals("127.0.0.1") || ipAddress.equals("0:0:0:0:0:0:0:1") || ipAddress.equals("localhost"))
					 try{ 
						 ipAddress = java.net.InetAddress.getLocalHost().getHostAddress();}
					 catch (UnknownHostException e) {e.printStackTrace();}
				
				String txnCode = req.getParameter("transactionCode");
				
				if(txnCode == null)
					txnCode = (String)req.getSession().getAttribute("transactionCode");
				
				if(txnCode == null)
					txnCode = (String)req.getAttribute("transactionCode");
				
				crb.setTransactionCode(txnCode);
				if(uBean != null){
//					crb.setRmNo(uBean.getRm_no());
					crb.setUsrId(uBean.getUsrID());
				}
				crb.setIpAddress(ipAddress);
				
				if(req.getParameter("MIS_AMOUNT") != null)
					crb.setAmount(req.getParameter("MIS_AMOUNT"));
				
				RequestHub.getInstance().addUnmatchedActivity(crb);
			}
		}
	}

	@Override
	public boolean preHandle(HttpServletRequest req, HttpServletResponse resp, Object handler) throws Exception {
		// TODO Auto-generated method stub
		String url = req.getRequestURI();
		
		if(!url.contains("CodeRed")){
			long time = Calendar.getInstance().getTimeInMillis();
			
			/** Start: Additional fields for MBA **/
			
			String ipAddress = req.getHeader("X-FORWARDED-FOR");
			if (ipAddress == null)
			    ipAddress = req.getHeader("HTTP_X_FORWARDED_FOR");
			if (ipAddress == null)
			    ipAddress = req.getRemoteAddr();
			if (ipAddress.equals("127.0.0.1") || ipAddress.equals("0:0:0:0:0:0:0:1") || ipAddress.equals("localhost"))
				 try{ 
					 ipAddress = java.net.InetAddress.getLocalHost().getHostAddress();}
				 catch (UnknownHostException e) {e.printStackTrace();}
			/** End: Additional fields for MBA **/
			String sessId = req.getSession().getId();
			
			String method = String.valueOf(req.getMethod().charAt(0));
			
			CodeRedBean crb = new CodeRedBean(sessId, url, method,
					time, null, null, null);
			
			String txnCode = (String)req.getSession().getAttribute("transactionCode");
			if(txnCode == null)
				txnCode = req.getHeader("TransactionCode") != null ? req.getHeader("TransactionCode") : req.getParameter("txncode");
			
			crb.setTransactionCode(txnCode);
			UserBean uBean = (UserBean) req.getSession().getAttribute("userBean");
			if(uBean != null){
//				crb.setRmNo(uBean.getRm_no());
				crb.setUsrId(uBean.getUsrID() != null ? uBean.getUsrID() : (String)req.getAttribute("txnLog_userId"));
			}else
				crb.setUsrId((String)req.getAttribute("txnLog_userId"));
				
			crb.setIpAddress(ipAddress);
			
			if(req.getParameter("MIS_AMOUNT") != null)
				crb.setAmount(req.getParameter("MIS_AMOUNT"));
			
			RequestHub.getInstance().addInboundActivity(crb);
			req.setAttribute("crb", crb);
		}
		
		return true;
	}

}
