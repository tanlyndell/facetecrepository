package tera.base.codered;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.ibatis.builder.xml.XMLMapperBuilder;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.mybatis.spring.SqlSessionTemplate;

import tera.base.codered.bean.CodeRedDataBean;

public class CodeRedSqlSession {
	
	String mapperLocation = "tera/base/codered/sqlresources/";
	String mapperFile;
	String validationQuery;
	BasicDataSource dataSource = null; 
	
	SqlSessionFactory sqlSessionFactory;

	CodeRedDataBean codeRedDataBean = new CodeRedDataBean();

	public void setDBDetails(){
		if(codeRedDataBean.getDbType().equals("MSSQL")){
			codeRedDataBean.setDatabasenameURL("jdbc:jtds:sqlserver://"+codeRedDataBean.getHostName()+":"+codeRedDataBean.getPort()+";databaseName="+codeRedDataBean.getDbName()+";socketTimeout=60");
			codeRedDataBean.setDbDriver("net.sourceforge.jtds.jdbc.Driver");
	
			mapperLocation = "tera/base/codered/sqlresources/";
			mapperFile = "TBL_CODE_RED.xml";
			validationQuery = "SELECT 1";	
			
		}
		else if(codeRedDataBean.getDbType().equals("Oracle")){
			codeRedDataBean.setDatabasenameURL("jdbc:oracle:thin:@"+codeRedDataBean.getHostName()+":"+codeRedDataBean.getPort()+":"+codeRedDataBean.getDbName());
			codeRedDataBean.setDbDriver("oracle.jdbc.OracleDriver");

			mapperLocation = "tera/base/codered/sqlresources/";
			mapperFile = "TBL_CODE_RED_ORACLE.xml";
			validationQuery = "SELECT 1 FROM DUAL";
		}
		else if(codeRedDataBean.getDbType().equals("MySQL")){
			codeRedDataBean.setDatabasenameURL("jdbc:mysql://"+codeRedDataBean.getHostName()+":"+codeRedDataBean.getPort()+"/"+codeRedDataBean.getDbName());
			codeRedDataBean.setDbDriver("com.mysql.jdbc.Driver");
	
			mapperLocation = "tera/base/codered/sqlresources/";
			mapperFile = "TBL_CODE_RED.xml";
			validationQuery = "SELECT 1";	
			
		}
	}

	public void setCodeRedDataBean(CodeRedDataBean codeRedDataBean){
		this.codeRedDataBean = codeRedDataBean;
		setDBDetails();
	}
	
	public boolean createCodeRedSession(){
		closeConnection();

		dataSource = new BasicDataSource();
		dataSource.setDriverClassName(codeRedDataBean.getDbDriver());
		dataSource.setUrl(codeRedDataBean.getDatabasenameURL());
		dataSource.setUsername(codeRedDataBean.getUsername());
		dataSource.setPassword(codeRedDataBean.getPassword());
		dataSource.setTimeBetweenEvictionRunsMillis(30000);
		dataSource.setMaxActive(2);
		dataSource.setMaxIdle(2);
		dataSource.setRemoveAbandoned(true);
		dataSource.setRemoveAbandonedTimeout(600);
		
		TransactionFactory transactionFactory = new JdbcTransactionFactory();
		Environment environment = new Environment("codeRed", transactionFactory, dataSource);
		Configuration configuration = new Configuration(environment);

		// Add XML Mapper
		InputStream in = getClass().getClassLoader().getResourceAsStream(mapperLocation+""+mapperFile) ; 
		if (in != null){
			XMLMapperBuilder xmlMapperBuilder = new XMLMapperBuilder(in, configuration, mapperFile, configuration.getSqlFragments());
		    xmlMapperBuilder.parse();
		}
		else {
			System.out.println("Error file not loaded " + mapperFile);
		}
		
		sqlSessionFactory = new SqlSessionFactoryBuilder().build(configuration);

		SqlSession sqlSession2 =  sqlSessionFactory.openSession();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		// Test if SQL Connection is Valid 
			try{
					
				connection = sqlSession2.getConnection();
				preparedStatement = connection.prepareStatement(validationQuery);
				rs = preparedStatement.executeQuery();
				if(rs.next()){
					System.out.println("Success! Connected to "+codeRedDataBean.getDbName());
					return true;
				}
				else {
					return false;
				}
			} 
			catch(Exception e){
				System.out.println("Failed to make connection to "+codeRedDataBean.getDbName()+"!");
				e.printStackTrace();
				return false;
			}
			finally{
			    try { rs.close(); } catch (Exception e) {  }
			    try { preparedStatement.close(); } catch (Exception e) {  }
			    try { connection.close(); } catch (Exception e) {  }				
			    try { sqlSession2.close(); } catch (Exception e) {  }				
			}

	}
	
	// Close existing connection; if any
	public void closeConnection(){
		try {
			if(dataSource != null && dataSource.getConnection() != null){
				dataSource.getConnection().close();
				dataSource.close();
				dataSource = null;
				System.out.println("Exisiting connection was closed!");
			}
			else{
				System.out.println("No connection was closed!!!!!");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("No connection was closed!!!!!");
		}
	
	}
	
	public SqlSessionFactory getSqlSessionFactory(){
		return sqlSessionFactory;
	}	

	public SqlSessionTemplate getSqlSessionTemplate(){
		return new SqlSessionTemplate(sqlSessionFactory);
	}	

}
