package tera.base.codered.bean;

import java.text.SimpleDateFormat;
import java.util.Date;

import tera.base.bean.DataBean;

public class CodeRedBean extends DataBean
{
	/*private String date;
	private String time;*/
	private String ipAddress;
	private String rmNo;
	private String usrId;
	private String usrIdMasked;
	private String url;
	private String amount;
	
	private String sessionId;
	private String requestType;
	private Long inboundTime;
	private Long outboundTime;
	private Long responseTime;
	private String inboundTimeDisp;
	private String outboundTimeDisp;
	private String preHandle;
	private String transactionCode;
	private String transactionName;
	
	private Long mainframeResponseTime1;
	private Long mainframeResponseTime2;
	
	private String sMainframeResponseTime1;
	private String sMainframeResponseTime2;
	private String dateTime;
	
//	private final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM-dd-yyyy");
//	private final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss.SSS");
	
	private final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	public CodeRedBean(){}
	
	public CodeRedBean(String sessionId, String url, String requestType, 
			Long inboundTime, Long outboundTime,
			Long mainframeResponseTime1, Long mainframeResponseTime2){
		this.sessionId = sessionId;
		this.url = url;
		this.outboundTime = outboundTime;
		this.requestType = requestType;
		this.inboundTime = inboundTime;
		
		/** Additional fields for MBA **/
		Date datetime = new Date(inboundTime != null ? inboundTime : outboundTime);
//		date = DATE_FORMAT.format(datetime);
//		time = TIME_FORMAT.format(datetime);
		dateTime = DATE_TIME_FORMAT.format(datetime);
		
		this.mainframeResponseTime1 = mainframeResponseTime1;
		this.mainframeResponseTime2 = mainframeResponseTime2;
	}
	
	public String getSessionId()
	{
		return sessionId;
	}
	
	public void setSessionId(String sessionId)
	{
		this.sessionId = sessionId;
	}
	
	public String getUrl()
	{
		return url;
	}
	
	public void setUrl(String url)
	{
		this.url = url;
	}
	
	public String getRequestType()
	{
		return requestType;
	}
	
	public void setRequestType(String requestType)
	{
		this.requestType = requestType;
	}
	
	public Long getInboundTime()
	{
		return inboundTime;
	}
	
	public void setInboundTime(Long inboundTime)
	{
		this.inboundTime = inboundTime;
	}
	
	public Long getOutboundTime()
	{
		return outboundTime;
	}
	
	public void setOutboundTime(Long outboundTime)
	{
		this.outboundTime = outboundTime;
	}
	
	public Long getResponseTime()
	{
		return responseTime;
	}
	
	public void setResponseTime(Long responseTime)
	{
		this.responseTime = responseTime;
	}
	
	public String getOutboundTimeDisp()
	{
		return outboundTimeDisp;
	}
	
	public void setOutboundTimeDisp(String outboundTimeDisp)
	{
		this.outboundTimeDisp = outboundTimeDisp;
	}
	
	public String getInboundTimeDisp()
	{
		return inboundTimeDisp;
	}
	
	public void setInboundTimeDisp(String inboundTimeDisp)
	{
		this.inboundTimeDisp = inboundTimeDisp;
	}
	
	public String getPreHandle()
	{
		return preHandle;
	}
	
	public void setPreHandle(String preHandle)
	{
		this.preHandle = preHandle;
	}
	
	public String getTransactionCode()
	{
		return transactionCode;
	}
	
	public void setTransactionCode(String transactionCode)
	{
		this.transactionCode = transactionCode;
	}

	public String getTransactionName() {
		return transactionName;
	}

	public void setTransactionName(String transactionName) {
		this.transactionName = transactionName;
	}

	public Long getMainframeResponseTime1() {
		return mainframeResponseTime1;
	}

	public void setMainframeResponseTime1(Long mainframeResponseTime1) {
		this.mainframeResponseTime1 = mainframeResponseTime1;
	}

	public Long getMainframeResponseTime2() {
		return mainframeResponseTime2;
	}

	public void setMainframeResponseTime2(Long mainframeResponseTime2) {
		this.mainframeResponseTime2 = mainframeResponseTime2;
	}

	public String getSMainframeResponseTime1() {
		return sMainframeResponseTime1;
	}

	public void setSMainframeResponseTime1(String sMainframeResponseTime1) {
		this.sMainframeResponseTime1 = sMainframeResponseTime1;
	}

	public String getSMainframeResponseTime2() {
		return sMainframeResponseTime2;
	}

	public void setSMainframeResponseTime2(String sMainframeResponseTime2) {
		this.sMainframeResponseTime2 = sMainframeResponseTime2;
	}

//	public String getDate() {
//		return date;
//	}
//
//	public void setDate(String date) {
//		this.date = date;
//	}
//
//	public String getTime() {
//		return time;
//	}
//
//	public void setTime(String time) {
//		this.time = time;
//	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getRmNo() {
		return rmNo;
	}

	public void setRmNo(String rmNo) {
		this.rmNo = rmNo;
	}

	public String getUsrId() {
		return usrId;
	}

	public String getUsrIdMasked() {
		String mask = "XXXX";
		if(usrId != null){
			if(usrId.length() <= 4)
				return usrId+mask;
			else
				return usrId.substring(0,4) + mask;			
		}
		return usrId;
	}

	public void setUsrIdMasked(String usrIdMasked) {
		this.usrIdMasked = usrIdMasked;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getsMainframeResponseTime1() {
		return sMainframeResponseTime1;
	}

	public void setsMainframeResponseTime1(String sMainframeResponseTime1) {
		this.sMainframeResponseTime1 = sMainframeResponseTime1;
	}

	public String getsMainframeResponseTime2() {
		return sMainframeResponseTime2;
	}

	public void setsMainframeResponseTime2(String sMainframeResponseTime2) {
		this.sMainframeResponseTime2 = sMainframeResponseTime2;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
}
