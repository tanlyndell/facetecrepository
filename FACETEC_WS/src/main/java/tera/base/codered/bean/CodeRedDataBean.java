package tera.base.codered.bean;

import java.util.ArrayList;
import java.util.List;

public class CodeRedDataBean {

	String username;
	String password;
	String databasenameURL;
	String dbDriver;
	
	String hostName;
	String port;
	String dbName;
	String dbType; 
	
	List<String> databaseDrivers;
	List<String> dbTypeList;
	
	public CodeRedDataBean(){
		databaseDrivers = new ArrayList();
		databaseDrivers.add("net.sourceforge.jtds.jdbc.Driver");
		databaseDrivers.add("com.mysql.jdbc.Driver");
		databaseDrivers.add("oracle.jdbc.OracleDriver");
	
		dbTypeList = new ArrayList();
		dbTypeList.add("MSSQL");
		dbTypeList.add("Oracle");
		dbTypeList.add("MySQL");
	}
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDatabasenameURL() {
		return databasenameURL;
	}
	public void setDatabasenameURL(String databasenameURL) {
		this.databasenameURL = databasenameURL;
	}
	public String getDbDriver() {
		return dbDriver;
	}
	public void setDbDriver(String dbDriver) {
		this.dbDriver = dbDriver;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}

	public List<String> getDatabaseDrivers() {
		return databaseDrivers;
	}

	public void setDatabaseDrivers(List<String> databaseDrivers) {
		this.databaseDrivers = databaseDrivers;
	}

	public List<String> getDbTypeList() {
		return dbTypeList;
	}

	public void setDbTypeList(List<String> dbTypeList) {
		this.dbTypeList = dbTypeList;
	}

}
