package tera.base.codered.bean;

import java.util.List;

import tera.base.bean.DataBean;

public class CodeRedSearchBean extends DataBean{

	
	private String USR_ID;
	private String TRAN_CODE;
	private String DATE_FROM;
	private String DATE_TO;
	private String TIME_FROM;
	private String TIME_FROM_MS;
	private String TIME_TO;
	private String IP_ADDRESS;
	private String count;
	
	private String orderBy;
	private String orderSort;
	
	private String responseTimeFrom;
	private String responseTimeTo;
	private String mfResponseTimeFrom;
	private String mfResponseTimeTo;
	private String mfResponseTimeFrom2;
	private String mfResponseTimeTo2;
	
	private String URL;
	private String TRAN_NAME;
	
	private List TRANS_LIST;

	public String getUSR_ID() {
		return USR_ID;
	}

	public void setUSR_ID(String uSR_ID) {
		USR_ID = uSR_ID;
	}

	public String getTRAN_CODE() {
		return TRAN_CODE;
	}

	public void setTRAN_CODE(String tRAN_CODE) {
		TRAN_CODE = tRAN_CODE;
	}

	public String getDATE_FROM() {
		return DATE_FROM;
	}

	public void setDATE_FROM(String dATE_FROM) {
		DATE_FROM = dATE_FROM;
	}

	public String getDATE_TO() {
		return DATE_TO;
	}

	public void setDATE_TO(String dATE_TO) {
		DATE_TO = dATE_TO;
	}

	public String getTIME_FROM() {
		return TIME_FROM;
	}

	public void setTIME_FROM(String tIME_FROM) {
		TIME_FROM = tIME_FROM;
	}

	public String getTIME_FROM_MS() {
		return TIME_FROM_MS;
	}

	public void setTIME_FROM_MS(String tIME_FROM_MS) {
		TIME_FROM_MS = tIME_FROM_MS;
	}

	public String getTIME_TO() {
		return TIME_TO;
	}

	public void setTIME_TO(String tIME_TO) {
		TIME_TO = tIME_TO;
	}

	public String getIP_ADDRESS() {
		return IP_ADDRESS;
	}

	public void setIP_ADDRESS(String iP_ADDRESS) {
		IP_ADDRESS = iP_ADDRESS;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getOrderSort() {
		return orderSort;
	}

	public void setOrderSort(String orderSort) {
		this.orderSort = orderSort;
	}

	public String getResponseTimeFrom() {
		return responseTimeFrom;
	}

	public void setResponseTimeFrom(String responseTimeFrom) {
		this.responseTimeFrom = responseTimeFrom;
	}

	public String getResponseTimeTo() {
		return responseTimeTo;
	}

	public void setResponseTimeTo(String responseTimeTo) {
		this.responseTimeTo = responseTimeTo;
	}

	public List getTRANS_LIST() {
		return TRANS_LIST;
	}

	public void setTRANS_LIST(List tRANS_LIST) {
		TRANS_LIST = tRANS_LIST;
	}

	public String getMfResponseTimeFrom() {
		return mfResponseTimeFrom;
	}

	public void setMfResponseTimeFrom(String mfResponseTimeFrom) {
		this.mfResponseTimeFrom = mfResponseTimeFrom;
	}

	public String getMfResponseTimeTo() {
		return mfResponseTimeTo;
	}

	public void setMfResponseTimeTo(String mfResponseTimeTo) {
		this.mfResponseTimeTo = mfResponseTimeTo;
	}

	public String getMfResponseTimeFrom2() {
		return mfResponseTimeFrom2;
	}

	public void setMfResponseTimeFrom2(String mfResponseTimeFrom2) {
		this.mfResponseTimeFrom2 = mfResponseTimeFrom2;
	}

	public String getMfResponseTimeTo2() {
		return mfResponseTimeTo2;
	}

	public void setMfResponseTimeTo2(String mfResponseTimeTo2) {
		this.mfResponseTimeTo2 = mfResponseTimeTo2;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public String getTRAN_NAME() {
		return TRAN_NAME;
	}

	public void setTRAN_NAME(String tRAN_NAME) {
		TRAN_NAME = tRAN_NAME;
	}
}

