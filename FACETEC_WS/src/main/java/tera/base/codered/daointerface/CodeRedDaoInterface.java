package tera.base.codered.daointerface;
import java.util.HashMap;
import java.util.List;

import tera.base.DAOInterface;
import tera.base.codered.bean.CodeRedBean;

public interface CodeRedDaoInterface extends DAOInterface{
	public void insertCodeRed(List<CodeRedBean> list);
	public List<CodeRedBean> getCodeRedLog(HashMap values);
	public List<CodeRedBean> getCodeRedLogLast(HashMap values);
	public int getCodeRedLogCount(HashMap values);
}