package tera.base.codered;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tera.base.codered.bean.CodeRedBean;

public class CodeRedServlet extends HttpServlet {
	private final SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm:ss.s");
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(req.getSession().getAttribute("fromCodeRedLogin") == null)
			resp.sendRedirect("CodeRedLogin.htm?b=CodeRedServlet");
		
		resp.setContentType("text/html"); 
		PrintWriter out = resp.getWriter();
		
		String showTables = req.getParameter("showTables");
		String refresh = req.getParameter("refresh");
		String clear = req.getParameter("clear");
		if(refresh != null)
			resp.setIntHeader("Refresh", Integer.valueOf(refresh));

		List<CodeRedBean> inboundActivity = RequestHub.getInstance().getInboundActivity();
		List<CodeRedBean> outboundActivityList = RequestHub.getInstance().getOutboundActivity();
		List<CodeRedBean> unmatchedActivityList = RequestHub.getInstance().getUnmatchedActivity();
		
		if("1".equalsIgnoreCase(clear)){
			RequestHub.getInstance().getInboundActivity().clear();
			RequestHub.getInstance().getOutboundActivity().clear();
			RequestHub.getInstance().getUnmatchedActivity().clear();
		}
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("<div class=\"col-md-12\">");
		sb.append("<h1>Code Red</h1>");
		sb.append("<h4>As of: "+new Date().toLocaleString()+"</h4>");
		sb.append(showDefaultView(inboundActivity.size(), outboundActivityList.size(), unmatchedActivityList.size(), showTables));
		
		if(showTables == null || !"2".equalsIgnoreCase(showTables)){
			sb.append("<br>");
			sb.append(showTables(inboundActivity, "Inbound Activity", true));
		}
		
		if(showTables == null || !"1".equalsIgnoreCase(showTables)){
			sb.append("<br>");
			sb.append(showTables(outboundActivityList, "Outbound Activity", false));
		}
		sb.append("</div>");
		out.println(sb.toString());
		
		if(req.getParameter("wakeup") != null){
			CodeRedScheduler.getInstance().setMaxIdle(req.getParameter("idle") != null ?
					Integer.parseInt(req.getParameter("idle")) : 0);
			CodeRedScheduler.getInstance().setJobInterval(req.getParameter("int"));
			CodeRedScheduler.getInstance().setWakeup(req.getParameter("wakeup"));
		}	
	}
	
	private String showDefaultView(int inboundSize, int outboundSize, int unmatchedSize, String showTables){
		StringBuilder sb = new StringBuilder();
		sb.append("<dl>");
		
		if(showTables == null || !"2".equalsIgnoreCase(showTables))
			sb.append("<dt>").append("Inbound Activity: ").append(String.valueOf(inboundSize)).append("</dt>");
		if(showTables == null || !"1".equalsIgnoreCase(showTables))
			sb.append("<dt>").append("Outbound Activity: ").append(String.valueOf(outboundSize)).append("</dt>");
		sb.append("<dt>").append("Unmatched Activity: ").append(String.valueOf(unmatchedSize)).append("</dt>");
		sb.append("</dl>");
		return sb.toString();
	}
	
	private String showTables(List<CodeRedBean> list, String header, boolean inbound){
		StringBuilder sb = new StringBuilder();
		sb.append("<h3>").append(header).append("</h3>");
		sb.append("<table class=\"table table-bordered\">");
		sb.append("<thead>").append("<tr>");
		sb.append("<th>").append("User ID").append("</th>");
		/*sb.append("<td>").append("Session ID").append("</td>");*/
		sb.append("<th>").append("URL").append("</th>");
		sb.append("<th>").append("Inbound Time").append("</th>");
		if(!inbound){
//			sb.append("<th>").append("Outbound Time").append("</th>");
			sb.append("<th>").append("Response Time (ms)").append("</th>");
			sb.append("<th>").append("MF Response 1").append("</th>");
			sb.append("<th>").append("MF Response 2").append("</th>");
			sb.append("<th>").append("Request Type").append("</th>");
			sb.append("</tr>").append("</thead>");
		}
		sb.append("<tbody>");
		
		for(CodeRedBean cb : list){
			sb.append("<tr>");		
			/*sb.append("<td>").append(cb.getSessionId()).append("</td>");*/
			sb.append("<td>").append(cb.getUsrIdMasked()).append("</td>");
			sb.append("<td>").append(cb.getUrl()).append("</td>");
			sb.append("<td>").append(timeFormat.format(new Date(cb.getInboundTime()))).append("</td>");
			if(!inbound){
//				sb.append("<td>").append(timeFormat.format(new Date(cb.getOutboundTime()))).append("</td>");
//				sb.append("<td>").append(cb.getInboundTime()).append("</td>");
//				sb.append("<td>").append(cb.getOutboundTime()).append("</td>");
				sb.append("<td>").append(cb.getResponseTime()).append("</td>");
			
				sb.append("<td>").append(cb.getMainframeResponseTime1()).append("</td>");
				sb.append("<td>").append(cb.getMainframeResponseTime2()).append("</td>");
				String requestType = "G".equalsIgnoreCase(cb.getRequestType()) ? "GET" : "POST";
				sb.append("<td>").append(requestType).append("</td>");
				sb.append("</tr>");
			}
		}
		
		
		sb.append("</tbody>");
		sb.append("</table>");
		
		
		return sb.toString();
	}
}
