package tera.base.codered;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import tera.base.codered.bean.CodeRedBean;

public class RequestHub {

	private static RequestHub instance = null;	
	private static final List<CodeRedBean> inboundActivity = new CopyOnWriteArrayList<CodeRedBean>();
	private static final List<CodeRedBean> outboundActivity = new CopyOnWriteArrayList<CodeRedBean>();
	private static final List<CodeRedBean> unmatchedActivity = new CopyOnWriteArrayList<CodeRedBean>();
	
	
    public static RequestHub getInstance(){
		if(instance == null)
			instance = new RequestHub();
		return instance;
	}

    public void addInboundActivity(CodeRedBean codeRedBean){
    	inboundActivity.add(codeRedBean);
	}
    
	public void addOutboundActivity(CodeRedBean codeRedBean){
		
		if(inboundActivity.contains(codeRedBean)){
			outboundActivity.add(codeRedBean);
			inboundActivity.remove(codeRedBean);
		}else
			unmatchedActivity.add(codeRedBean);
	}
	
	public void addUnmatchedActivity(CodeRedBean codeRedBean){
		unmatchedActivity.add(codeRedBean);
	}
	
	public void removeInboundActivity(List<CodeRedBean> list){
		inboundActivity.removeAll(list);
	}	
	public void removeOutboundActivity(List<CodeRedBean> list){
		outboundActivity.removeAll(list);
	}	
	public void removeUnmatchedActivity(List<CodeRedBean> list){
		unmatchedActivity.removeAll(list);
	}

	public List<CodeRedBean> getInboundActivity(){
		return inboundActivity;
	}	
	public List<CodeRedBean> getOutboundActivity(){
		return outboundActivity;
	}	
	public List<CodeRedBean> getUnmatchedActivity(){
		return unmatchedActivity;
	}
}
