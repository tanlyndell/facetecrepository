package tera.base.codered.job;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.stereotype.Component;

import tera.base.ObjectFactory;

import tera.base.codered.CodeRedScheduler;
import tera.base.codered.RequestHub;
import tera.base.codered.bean.CodeRedBean;
import tera.base.codered.daointerface.CodeRedDaoInterface;

@Component
public class CodeRedCleanupJob implements StatefulJob {
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		// TODO Auto-generated method stub
//		System.out.println("Cleaning idle inbound request and unmatched request...");
		long currTime = Calendar.getInstance().getTimeInMillis();
		List<CodeRedBean> inboundList = RequestHub.getInstance().getInboundActivity();
		CodeRedDaoInterface doer = (CodeRedDaoInterface)ObjectFactory.getObject("CodeRedDoer");
		if(!inboundList.isEmpty()){
//			System.out.println("Total list size for cleanup: "+inboundList.size());
			List<CodeRedBean> forCleanUp = new ArrayList<CodeRedBean>();
			
			int maxIdle = CodeRedScheduler.getInstance().getMaxIdle();
						
			for(int x = 0; x < inboundList.size(); x++){
				CodeRedBean crb = inboundList.get(x);
				if(currTime - crb.getInboundTime() > maxIdle){
					forCleanUp.add(crb);
				}	
			}
			
			//Clean in memory (Inbound list)
			if(!forCleanUp.isEmpty()){
				List<CodeRedBean> forCleanUpInbound = new ArrayList<CodeRedBean>();
				forCleanUpInbound.addAll(forCleanUp);
				while(forCleanUp.size() > 0){
					List<CodeRedBean> list = new ArrayList(forCleanUp.size() > 100 ? forCleanUp.subList(0, 100) : forCleanUp);
					try{
						doer.insertCodeRed(list);
					}catch(Exception e){
//						System.out.println("CODERED: Error with DB insert. Removing list from memory...");
					}	
					forCleanUp.removeAll(list);
				}
				RequestHub.getInstance().removeInboundActivity(forCleanUpInbound);
			}
			//Get unmatched request
			
		}/*else
			System.out.println("Inbound: No records yet...");*/
		
		List<CodeRedBean> unmatched = RequestHub.getInstance().getUnmatchedActivity();
		if(!unmatched.isEmpty()){
//			System.out.println("Total list size of unmatched data: "+inboundList.size());
			List<CodeRedBean> forCleanUp = new ArrayList<CodeRedBean>();
			forCleanUp = RequestHub.getInstance().getUnmatchedActivity();
			//Clean in memory (Unmatched list)
			if(!forCleanUp.isEmpty()){
				while(forCleanUp.size() > 0){
					List<CodeRedBean> list = new ArrayList(forCleanUp.size() > 100 ? forCleanUp.subList(0, 100) : forCleanUp);
					try{
						doer.insertCodeRed(list);
					}catch(Exception e){
//						System.out.println("CODERED: Error with DB insert. Removing list from memory...");
					}
					forCleanUp.removeAll(list);
				}
				RequestHub.getInstance().removeUnmatchedActivity(unmatched);
			}
		}/*else
			System.out.println("Unmatched: No records yet...");*/
	}
}
