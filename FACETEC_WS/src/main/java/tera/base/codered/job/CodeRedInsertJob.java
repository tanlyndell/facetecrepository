package tera.base.codered.job;

import java.util.ArrayList;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import tera.base.ObjectFactory;

import tera.base.codered.RequestHub;
import tera.base.codered.bean.CodeRedBean;
import tera.base.codered.daointerface.CodeRedDaoInterface;


public class CodeRedInsertJob implements StatefulJob {

	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub
//		System.out.println("CodeRedInsertJob...");
		List<CodeRedBean> forInsert = RequestHub.getInstance().getOutboundActivity();
		
		if(!forInsert.isEmpty()){
			CodeRedDaoInterface doer = (CodeRedDaoInterface)ObjectFactory.getObject("CodeRedDoer");
			while(forInsert.size() > 0){
				List<CodeRedBean> list = new ArrayList(forInsert.size() > 100 ? forInsert.subList(0, 100) : forInsert);
				try{
					doer.insertCodeRed(list);
//					System.out.println("CODERED: Done with DB insert. Removing list from memory...");
				}catch(Exception e){
//					System.out.println("CODERED: Error with DB insert. Removing list from memory...");
				}
				
				forInsert.removeAll(list);
			}
			RequestHub.getInstance().removeOutboundActivity(forInsert);
		}/*else
			System.out.println("No records yet...");*/
	}
}
