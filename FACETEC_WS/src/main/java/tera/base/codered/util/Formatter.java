package tera.base.codered.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.ui.ModelMap;

import tera.base.ObjectFactory;

public class Formatter {

	public void setDefaultDateAndTime(ModelMap modelMap, Date thisDate){
		
		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		
		modelMap.addAttribute("defaultDate", dateFormat.format( thisDate ));
		
		dateFormat = new SimpleDateFormat("hh:mm a");
		
		modelMap.addAttribute("defaultTime", dateFormat.format(thisDate));
		
		dateFormat = null;
	}
	
	public boolean isValidDate(String date, String format){
		SimpleDateFormat fmt = new SimpleDateFormat(format);
		
		try{
			fmt.parse(date);
			
			return true;
		}
		catch(Exception e){
			return false;
		}
	}
	
	public int compareStringDates(String date1, String date2, String dateFormat) throws ParseException{
		SimpleDateFormat fmt = new SimpleDateFormat(dateFormat);
		
		Date dateObj1 = fmt.parse(date1);
		Date dateObj2 = fmt.parse(date2);
		
		if(dateObj1.before(dateObj2))
			return -1;
		else if(dateObj1.after(dateObj2))
			return 1;
		else
			return 0;
	}
}
