/**
*CLASS: RegularExpression.java
*AUTHOR: KEVIN JON VALDEZ ORIT
*DATE CREATED: 2013-07-10
*DESCRIPTION: UTILITY CLASS FOR VALIDATING STRINGS THROUGH REGULAR EXPRESSIONS
*
*	+:Added, -:Deleted, *:Modified
*								--==MODIFICATION HISTORY==--
*
*Date			Programmer		Fields/Methods					Action		Description
*-----------------------------------------------------------------------------------------------------------------------
*20130906		LEL				REGEX_SEC_ANSWER,				+			new regex for security questions and answer
*								REGEX_SEC_QSTN
*/

package tera.base.codered.util;

public class RegularExpression {
	
	public final static String REGEX_NUMDATE = "^[0-9\\-/ ,]+$";
	private final static String REGEX_VALID = "[^<>=]+";
	private final static String REGEX_GT = "&amp;gt;";
	private final static String REGEX_LT = "&amp;lt;";
	
	/**
	 * 
	 * @param REGEX - regular expression to match the inputString with
	 * @param inputString - String to be validated.
	 * @return returns true if the inputString matches the REGEX, false if it doesn't.
	 */
	public boolean test(String REGEX, String inputString){
		if(REGEX == null)
			REGEX = "";
		if(inputString == null)
			inputString = "";
		
		return testValid(inputString) && inputString.matches(REGEX);
	}
	
	/**
	 * 
	 * @param inputString - String to be validated.
	 * @return returns true if the inputString doesn't contain '<', '>', or '=', false if it does.
	 */
	public boolean testValid(String inputString){
		if(inputString.matches(REGEX_VALID)){
			return !inputString.contains(REGEX_GT) && !inputString.contains(REGEX_LT);
		}
		else 
			return false;
	}
}
