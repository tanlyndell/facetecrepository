package tera.base.codered;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

public class CodeRedScheduler {

	private static CodeRedScheduler instance = null;	
	private Scheduler scheduler;	
	private String jobInterval;
	private String cleanupInterval;
	private String wakeup;
	private int maxIdle;
	private boolean jobUp;
	
	private CodeRedScheduler(){}
	
	public static CodeRedScheduler getInstance(){
		if(instance == null)
			instance = new CodeRedScheduler();
		return instance;	
	}
	
	
	public String getJobInterval() {
		return jobInterval != null ? jobInterval : "10";
	}
	public String getWakeup() {
		return wakeup;
	}
	public void setJobInterval(String jobInterval) {
		this.jobInterval = jobInterval != null ? jobInterval : "10";//Change this to 300
	}
	public void setWakeup(String wakeup) {
		System.out.println("Requesting to wakeup job: " + wakeup + " Job is awake: " + jobUp);
		if("Y".equalsIgnoreCase(wakeup) && !jobUp){
			SchedulerFactory schedulerFactory = new StdSchedulerFactory();
			try {
				scheduler = schedulerFactory.getScheduler();
				scheduler.start();
				
				JobDetail jobDetail = new JobDetail("coderedJob", "codered", tera.base.codered.job.CodeRedInsertJob.class); 
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("threadCode", "40");
				map.put("jobId", "coderedInsert");					
				map.put("wakeInterval", getJobInterval());
				map.put("jobName", "coderedJob");
				map.put("jobGrpName", "codered");
				map.put("triggerName", "coderedTrigger");
				map.put("triggerGrpName", "coderedTriggerGrp");
				
				scheduler.scheduleJob(jobDetail, createTrigger(map));
				System.out.println("Codered Insert Job is up...");
				jobDetail = new JobDetail("coderedCleanupJob", "coderedCleanup", tera.base.codered.job.CodeRedCleanupJob.class); 
				
				map.put("threadCode", "41");
				map.put("jobId", "coderedCleanup");					
				map.put("wakeInterval", getCleanupInterval());
				map.put("jobName", "coderedCleanupJob");
				map.put("jobGrpName", "coderedCleanup");
				map.put("triggerName", "coderedCleanupTrigger");
				map.put("triggerGrpName", "coderedCleanupTriggerGrp");
				
				scheduler.scheduleJob(jobDetail, createTrigger(map));
				System.out.println("Codered Cleanup Job is up...");
				jobUp = true;
			} catch (SchedulerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (ParseException e){
				e.printStackTrace();
			}
		}else if("N".equalsIgnoreCase(wakeup) && jobUp){
			
			try {	
				scheduler.shutdown(true);
				System.out.println("Job is shutdown...");
				jobUp = false;
			} catch (SchedulerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
	}

	public int getMaxIdle() {
		//default maxIdle time is 60 sec
		return maxIdle == 0 ? 60000 : maxIdle;
	}
	
	public void setMaxIdle(int maxIdle) {
		this.maxIdle = (maxIdle == 0 ? 60000 : maxIdle);
	}
	
	public String getCleanupInterval() {
		return cleanupInterval;
	}


	public void setCleanupInterval(String cleanupInterval) {
		this.cleanupInterval = cleanupInterval;
	}


	public boolean isJobUp() {
		return jobUp;
	}


	public void setJobUp(boolean jobUp) {
		this.jobUp = jobUp;
	}


	private Trigger createTrigger(HashMap map) throws ParseException{		 		 
		 
		 String jobName = (String)map.get("jobName");
		 String jobGrpName = (String)map.get("jobGrpName");
		 String triggerName = (String)map.get("triggerName");
		 String triggerGrpName = (String)map.get("triggerGrpName"); 
		 
		 Trigger trigger = null;
		 		 
		 int wakeUpInt = Integer.parseInt((String)map.get("wakeInterval"));
		 
		 SimpleTrigger simpleTrigger = new SimpleTrigger(triggerName, triggerGrpName);				 
		 simpleTrigger.setStartTime(new Date());
		 simpleTrigger.setRepeatInterval(wakeUpInt * 1000);
		 simpleTrigger.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);	
		 trigger = simpleTrigger;
			 
		 trigger.setJobName(jobName);
		 trigger.setJobGroup(jobGrpName);
		 						 
		 return trigger;
	 }
}
