package tera.base.codered.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import tera.base.ObjectFactory;
import tera.base.codered.bean.CodeRedLoginBean;

@Controller
@SessionAttributes("txnBean")
public class CodeRedLoginController
{	
	
	@RequestMapping(value="/CodeRedLogin.htm", method = RequestMethod.GET)
	public String formBackingObject(HttpServletRequest request, ModelMap modelMap)
	{
		CodeRedLoginBean txnBean = new CodeRedLoginBean();
		request.getSession().removeAttribute("fromCodeRedLogin");
		request.getSession().removeAttribute("fromCodeRedLoginAll"); //Added ADO 20170116
		modelMap.addAttribute("txnBean",txnBean);
		return "base/CodeRedLogin";
	}
	
	@RequestMapping(value="/CodeRedLogin.htm", method = RequestMethod.POST)
	public String onSubmit( @ModelAttribute("txnBean") CodeRedLoginBean command,Errors errors, ModelMap modelMap, HttpServletRequest request,  HttpServletResponse response) throws Exception
	{
		String usrid = request.getParameter("usr_id");
		String pass = request.getParameter("usr_password");
		String urlBack = request.getParameter("b");
		
		if(usrid == null || (usrid != null && usrid.isEmpty()) ||
				pass == null || (pass != null && pass.isEmpty())){
			request.setAttribute("errMessage","Please fill up all mandatory fields");
			return "base/CodeRedLogin";
		}
		
		CodeRedLoginBean loginBean = (CodeRedLoginBean)ObjectFactory.getObject("CodeRedLogin");
		
		if(usrid == null || 
//				(usrid != null && loginBean !=null &&
				!usrid.equalsIgnoreCase(loginBean.getUserName()) //LEL20170710: Temp hard code
//				!usrid.equalsIgnoreCase("teramba")
				){
			request.setAttribute("errMessage","Invalid User ID");
			return "base/CodeRedLogin";
		}
		
		if(pass == null ||
//				(pass != null && loginBean != null &&
				!pass.equals(loginBean.getPassword()) //LEL20170710: Temp hard code
//				!pass.equals("P@ssw0rd")
				){
			request.setAttribute("errMessage","Invalid Password");
			return "base/CodeRedLogin";
		}		
		
		request.getSession().setAttribute("fromCodeRedLogin",true);
		
		if("CodeRedServlet".equals(urlBack))
			urlBack += "?showTables=all&refresh=2";
		
		
		return "redirect:"+urlBack;
	}
}
