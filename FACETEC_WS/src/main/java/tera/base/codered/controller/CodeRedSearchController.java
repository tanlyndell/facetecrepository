package tera.base.codered.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import net.sf.cglib.beans.BeanMap;
import tera.base.ObjectFactory;
import tera.base.bean.UserBean;
import tera.base.codered.bean.CodeRedSearchBean;
import tera.base.codered.daointerface.CodeRedDaoInterface;
import tera.base.codered.util.Formatter;
import tera.base.codered.util.RegularExpression;
@Controller
@SessionAttributes("transaxnBean")
@RequestMapping("CodeRedSearch")
public class CodeRedSearchController
{
	
	@RequestMapping(method = RequestMethod.GET)
	protected String formBackingObject(ModelMap modelMap, HttpServletRequest request)throws Exception
	{
		if(request.getSession().getAttribute("fromCodeRedLogin") == null
				&& request.getSession().getAttribute("fromCodeRedLoginAll") == null)
			return "redirect:CodeRedLogin.htm?b=CodeRedSearch.htm";
		
		CodeRedSearchBean transaxnBean = new CodeRedSearchBean();
		String path = request.getRequestURI() + "?transactionCode=" + request.getParameter("transactionCode");
		Calendar cal = Calendar.getInstance();
		Date thisDate = cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		transaxnBean.setUSR_ID("");
		transaxnBean.setTRAN_CODE("");
		transaxnBean.setDATE_FROM(dateFormat.format( thisDate ));
		transaxnBean.setDATE_TO(dateFormat.format( thisDate ));
		
		request.getSession().setAttribute( "transDT" , dateFormat.format( thisDate ) );
		request.getSession().setAttribute( "transDF" , dateFormat.format( thisDate ) );
				
		Formatter formatter = new Formatter();
		formatter.setDefaultDateAndTime(modelMap, thisDate);
		
		request.getSession().setAttribute( "displayPage" , path );
		
		request.getSession().setAttribute( "transaxnBean" , transaxnBean );

		/******* Transaction logging *********/
		/*java.sql.Timestamp new_date = new java.sql.Timestamp(cal.getTime().getTime());
		
		HashMap param = new HashMap();
		param.put("request", request);
		param.put("refNo", "");
		param.put("transactionDateTS", new_date);
		param.put("status", "");
		GenericDaoInterface genericDoer = (GenericDaoInterface) ObjectFactory.getObject("GenericDoer");
		genericDoer.exempt_insertToTransLog(param);*/
		/***/

		request.getSession().setAttribute("userBean",new UserBean());
		request.getSession().setAttribute("MAX_NUM_ROWS", "20");
		request.getSession().setAttribute("pages", 0);
		request.getSession().setAttribute("currPage",0);
		request.getSession().setAttribute("totalRecordNum",0);
		setTime(request);
		modelMap.addAttribute("transaxnBean", transaxnBean);
		request.setAttribute("fromGet",true);
		return "base/CodeRedSearch";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	protected String onSubmit(ModelMap modelMap,
									  HttpServletRequest request, 
									  HttpServletResponse response, 
									  @ModelAttribute("transaxnBean")CodeRedSearchBean command, 
									  Errors errors,
									  String commandName)throws Exception{
		setTime(request);
		if( !onBindAndValidate(modelMap, request, response, command, errors) )
			return "base/CodeRedSearch";
		
		request.setAttribute("submittedTransLog", "true");
		request.getSession().setAttribute( "beanName" , command );
		request.getSession().setAttribute( "transaxnBean", command );
		request.getSession().setAttribute( "historyType" , request.getParameter( "historyType" ) );
		
		String action = request.getParameter("action");
		HashMap<String, Object> values = new HashMap<String, Object>();
		values.putAll(BeanMap.create(command));
		values.put("DATE_TIME_FROM", command.getDATE_FROM() + " " + command.getTIME_FROM());
		values.put("DATE_TIME_TO", command.getDATE_TO() + " " + command.getTIME_TO());
		values.put("Request",request);
		CodeRedDaoInterface doer = (CodeRedDaoInterface)ObjectFactory.getObject("CodeRedDoer");
		
		int totalRecordNum = 0;
		int pages = 0;
		int currPage = request.getSession().getAttribute("currPage") != null ?
				(Integer)request.getSession().getAttribute("currPage") : 0;
		
		String dispLimit = request.getParameter("dispLimit");
		if(dispLimit == null || (dispLimit != null && "".equals(dispLimit.trim())))
			dispLimit = "20";
		
		int dispLimiti = Integer.parseInt(dispLimit != null ? dispLimit : "20");
		
		if("search".equalsIgnoreCase(action)){
			
			
			//Count total number of records
			totalRecordNum = doer.getCodeRedLogCount(values);
			if(totalRecordNum > 0){
				
				int quo = totalRecordNum / dispLimiti;
				int rem = totalRecordNum % dispLimiti;
				
				pages = rem > 0 ? quo+1 : quo;
				currPage = 1;
			}
			request.getSession().setAttribute("totalRecordNum",totalRecordNum);
		}else if("prevPage".equalsIgnoreCase(action)){
			if(currPage > 1)
				currPage--;
		}else if("nextPage".equalsIgnoreCase(action)){
			if(currPage < (Integer)request.getSession().getAttribute("pages"))
				currPage++;
		}else if("firstPage".equalsIgnoreCase(action)){
			currPage = 1;
		}else if("lastPage".equalsIgnoreCase(action)){
			currPage = (Integer)request.getSession().getAttribute("pages");
		}else if("goto".equalsIgnoreCase(action)){
			int gotoPage = 1;
			
			try{
				gotoPage = Integer.parseInt(request.getParameter("gotoValue"));
			}catch(Exception e){
				e.printStackTrace();
			}
			if(gotoPage <= (Integer)request.getSession().getAttribute("pages") && gotoPage > 0)
				currPage = gotoPage;
		}
		
		totalRecordNum = (Integer)request.getSession().getAttribute("totalRecordNum");
		
		if(totalRecordNum > 0){
			
			values.put("rowFrom",(currPage * dispLimiti) - (dispLimiti - 1));
			values.put("rowTo",currPage * dispLimiti);
			request.setAttribute("searchList", doer.getCodeRedLog(values));
		}else{
			pages= 0;
			currPage=0;
		}
			
		int quo = totalRecordNum / dispLimiti;
		int rem = totalRecordNum % dispLimiti;
		pages = rem > 0 ? quo+1 : quo;
	
		request.getSession().setAttribute("pages", pages);
		request.getSession().setAttribute("currPage",currPage);
		request.getSession().setAttribute("MAX_NUM_ROWS", dispLimit);
		
		modelMap.addAttribute("transaxnBean", command);
		return "base/CodeRedSearch";
	}
	
	private boolean onBindAndValidate(ModelMap modelMap,HttpServletRequest request, HttpServletResponse response, CodeRedSearchBean command, Errors errors){
		
		String jsEnabled = String.valueOf( request.getParameter("jsEnabled") == null ? "" : request.getParameter("jsEnabled").trim() );
		
		if(!"true".equalsIgnoreCase(jsEnabled))
			return false;
		
		boolean err = false;
		List<String> addErrClass = new ArrayList<String>();
		Formatter formatter = new Formatter();
		
		//mandatory
		if(command.getDATE_FROM() == null || (command.getDATE_FROM() != null && command.getDATE_FROM().trim().length() < 1)){
			errors.rejectValue("DATE_FROM", "mandatory");
			addErrClass.add("datetime");
			err = true;
		}
		
		if(err){
			modelMap.addAttribute("hasError", true);
			errors.rejectValue("serverError", "mandatory.serverError");
			modelMap.addAttribute("addErrClass", addErrClass);
			return false;
		}
		
		//invalid chars
		RegularExpression regex = new RegularExpression();
		
		command.setUSR_ID(command.getUSR_ID()!= null ? command.getUSR_ID().trim() : "");
		if(command.getUSR_ID() != null && command.getUSR_ID().length() > 0 && !command.getUSR_ID().matches("^[a-zA-Z0-9 _,]+$")){
			errors.rejectValue("USR_ID", "error.invChar");
			addErrClass.add("USR_ID");
			err = true;
		}
		
		
		command.setDATE_FROM(command.getDATE_FROM().trim());
		if(!regex.test(RegularExpression.REGEX_NUMDATE, command.getDATE_FROM())){
			errors.rejectValue("DATE_FROM", "error.invChar");
			addErrClass.add("DATE_FROM");
			err = true;
		}
				
		command.setDATE_TO(command.getDATE_TO() != null && command.getDATE_TO().trim().length() > 0 ? command.getDATE_TO().trim() : "");
		if(!regex.test(RegularExpression.REGEX_NUMDATE, command.getDATE_TO())){
			errors.rejectValue("DATE_TO", "error.invChar");
			addErrClass.add("DATE_TO");
			err = true;
		}		
		String REGEX_24HOUR_SEC = "^[0-9]{1,2}[:][0-9]{2}[:][0-9]{2}$";
		if(command.getTIME_TO() != null && !command.getTIME_TO().trim().equals("")){
			if(!regex.test(REGEX_24HOUR_SEC, command.getTIME_TO())){
				errors.rejectValue("TIME_TO", "error.invChar");
				addErrClass.add("TIME_TO");
				err = true;
			}
			
			if(command.getTIME_TO() != null && !command.getTIME_TO().trim().equals("") && !formatter.isValidDate(command.getTIME_TO(), "hh:mm:ss")){
				errors.rejectValue("TIME_TO", "error.time.invFmt", new Object[]{"HH:MM:SS"}, "");
				addErrClass.add("TIME_TO");
				err = true;
			}
		}
		
		if(err){
			modelMap.addAttribute("hasError", true);
			modelMap.addAttribute("addErrClass", addErrClass);
			errors.rejectValue("serverError", "formError.serverError");
			return false;
		}
		
		if(command.getTIME_FROM() != null && !command.getTIME_FROM().trim().equals("")){
			
			if(!regex.test(REGEX_24HOUR_SEC, command.getTIME_FROM())){
				errors.rejectValue("TIME_FROM", "error.invChar");
				addErrClass.add("TIME_FROM");
				err = true;
			}
			
			if(!formatter.isValidDate(command.getTIME_FROM(), "hh:mm:ss")){
				errors.rejectValue("TIME_FROM", "error.time.invFmt", new Object[]{"HH:MM:SS"}, "");
				addErrClass.add("TIME_FROM");
				err = true;
			}
		}
		
		if(err){
			modelMap.addAttribute("hasError", true);
			errors.rejectValue("serverError", "formError.serverError");
			modelMap.addAttribute("addErrClass", addErrClass);
			return false;
		}
		
		//invalid date
		String errMsg = "";
		String df = "yyyy-MM-dd";
		
		//invalidDate
		/*if(!Helper.isValidDate(command.getDATE_FROM(), df)){
			errMsg = formatter.convertCodeToMessage("invalidDate", new Object[]{formatter.convertCodeToMessage("date_from", request)}, request) + "<br/>";
			addErrClass.add("dateFromClass");
			err = true;
		}*/
		/*if(!Helper.isValidDate(command.getDATE_TO(), df)){
			errMsg = formatter.convertCodeToMessage("invalidDate", new Object[]{formatter.convertCodeToMessage("date_to", request)}, request);
			addErrClass.add("dateToClass");
			err = true;
		}*/
		
		if(err){
			errors.rejectValue("serverError", "", null, errMsg);
			modelMap.addAttribute("hasError", true);
			modelMap.addAttribute("addErrClass", addErrClass);
			return false;
		}
		else{
			try {
				String dateFrom = command.getDATE_FROM() + " " + command.getTIME_FROM();
				String dateTo = command.getDATE_TO() + " " + command.getTIME_TO();
				
				//datefrom > dateTo
				if(formatter.compareStringDates(dateFrom, dateTo, df) > 0){
					errors.rejectValue("serverError", "Date From  must be greater than Date To");
					addErrClass.add("datetime");
					err = true;
				}
			} catch (ParseException e) {
				
				e.printStackTrace();
				errMsg = "Invalid date." + "<br/>";
				addErrClass.add("dateFromClass");
				addErrClass.add("dateToClass");
				errors.rejectValue("serverError", errMsg);
				err = true;
			}
		}
		
		if(err){
			
			modelMap.addAttribute("hasError", true);
			modelMap.addAttribute("addErrClass", addErrClass);
			return false;
		}
		
		if(command.getTIME_TO() != null && command.getTIME_TO().trim().length() > 0)
			command.setTIME_TO(command.getTIME_TO()+".999");
		
		if(command.getTIME_FROM() != null && command.getTIME_FROM().trim().length() > 0)
			command.setTIME_FROM(command.getTIME_FROM()+".000");
		
		return !err;
	}
	
	private void setTime(HttpServletRequest request){
		Calendar calendar = Calendar.getInstance();
	    int year = calendar.get(Calendar.YEAR);
	    int month = calendar.get(Calendar.MONTH);
	    int day = calendar.get(Calendar.DATE);
	    calendar.set(year, month, day, 23, 59, 59);
	    Date today = calendar.getTime();
	    request.setAttribute("todayDate", today);
	}
}