package tera.base.codered.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import tera.base.ObjectFactory;
import tera.base.bean.UserBean;
import tera.base.codered.bean.CodeRedBean;
import tera.base.codered.bean.CodeRedSearchBean;
import tera.base.codered.implem.CodeRedImplem;
import tera.base.codered.util.Formatter;
import tera.base.codered.util.RegularExpression;

@Controller
@SessionAttributes("transaxnBean")
@RequestMapping("CodeRedSearchLast")
public class CodeRedSearchLastController
{
	@RequestMapping(method = RequestMethod.GET)
	protected String formBackingObject(ModelMap modelMap, HttpServletRequest request)throws Exception
	{
		
		if(request.getSession().getAttribute("fromCodeRedLogin") == null
				&& request.getSession().getAttribute("fromCodeRedLoginAll") == null)
			return "redirect:CodeRedLogin.htm?b=CodeRedSearchLast.htm";
		
		CodeRedSearchBean transaxnBean = new CodeRedSearchBean();
		String path = request.getRequestURI() + "?transactionCode=" + request.getParameter("transactionCode");
		
		request.getSession().setAttribute( "displayPage" , path );
		
		request.getSession().setAttribute( "transaxnBean" , transaxnBean );

		/******* Transaction logging *********/
		/*Calendar cal = Calendar.getInstance();
		java.sql.Timestamp new_date = new java.sql.Timestamp(cal.getTime().getTime());
		
		HashMap param = new HashMap();
		param.put("request", request);
		param.put("refNo", "");
		param.put("transactionDateTS", new_date);
		param.put("status", "");
		GenericDaoInterface genericDoer = (GenericDaoInterface) ObjectFactory.getObject("GenericDoer");
		genericDoer.exempt_insertToTransLog(param);*/
		/***/
		setTime(request);
		request.getSession().setAttribute("userBean",new UserBean());
		request.getSession().setAttribute("MAX_NUM_ROWS", "20");
		request.getSession().removeAttribute("beanList");
		modelMap.addAttribute("transaxnBean", transaxnBean);
		return "base/CodeRedSearchLast";
	}
	
	private boolean onBindAndValidate(ModelMap modelMap,HttpServletRequest request, HttpServletResponse response, CodeRedSearchBean command, Errors errors){
		String jsEnabled = String.valueOf( request.getParameter("jsEnabled") == null ? "" : request.getParameter("jsEnabled").trim() );
		
		if(!"true".equalsIgnoreCase(jsEnabled))
			return false;
		
		boolean err = false;
		List<String> addErrClass = new ArrayList<String>();
		Formatter formatter = new Formatter();
		
		
		//invalid chars
		RegularExpression regex = new RegularExpression();
		
		
		command.setDATE_FROM(command.getDATE_FROM() != null ? command.getDATE_FROM().trim() : "");
		if(!command.getDATE_FROM().isEmpty() && !regex.test(RegularExpression.REGEX_NUMDATE, command.getDATE_FROM())){
			errors.rejectValue("DATE_FROM", "error.invChar");
			addErrClass.add("DATE_FROM");
			err = true;
		}
		String REGEX_24HOUR_SEC = "^[0-9]{1,2}[:][0-9]{2}[:][0-9]{2}$";
		if(command.getTIME_FROM() != null && !command.getTIME_FROM().trim().equals("")){
			
			if(!regex.test(REGEX_24HOUR_SEC, command.getTIME_FROM())){
				errors.rejectValue("TIME_FROM", "error.invChar");
				addErrClass.add("TIME_FROM");
				err = true;
			}
			
			if(!formatter.isValidDate(command.getTIME_FROM(), "hh:mm:ss")){
				errors.rejectValue("TIME_FROM", "error.time.invFmt", new Object[]{"HH:MM:SS"}, "");
				addErrClass.add("TIME_FROM");
				err = true;
			}
		}
		
		
		
		
		if(err){
			modelMap.addAttribute("hasError", true);
			errors.rejectValue("serverError", "formError.serverError");
			modelMap.addAttribute("addErrClass", addErrClass);
			return false;
		}
		
		//invalid date
		String errMsg = "";
//		String df = "yyyy-MM-dd";
		
		//invalidDate
		/*if(command.getDATE_FROM() != null && !command.getDATE_FROM().trim().isEmpty() && !Helper.isValidDate(command.getDATE_FROM(), df)){
			errMsg = formatter.convertCodeToMessage("invalidDate", new Object[]{formatter.convertCodeToMessage("date_from", request)}, request) + "<br/>";
			addErrClass.add("dateFromClass");
			err = true;
		}*/
		
		if(err){
			errors.rejectValue("serverError", "", null, errMsg);
			modelMap.addAttribute("hasError", true);
			modelMap.addAttribute("addErrClass", addErrClass);
			return false;
		}
		
		return !err;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	protected String onSubmit(ModelMap modelMap,
									  HttpServletRequest request, 
									  HttpServletResponse response, 
									  @ModelAttribute("transaxnBean")CodeRedSearchBean command, 
									  Errors errors,
									  String commandName)throws Exception{
		CodeRedSearchBean transLogBean = (CodeRedSearchBean)command;
		setTime(request);
		if( !onBindAndValidate(modelMap, request, response, command, errors) )
			return "base/CodeRedSearchLast";
		
		request.setAttribute("submittedTransLog", "true");
		request.getSession().setAttribute( "beanName" , transLogBean );
		request.getSession().setAttribute( "transaxnBean", transLogBean );
		
		
		String dispLimit = request.getParameter("dispLimit");
		if(dispLimit == null || (dispLimit != null && "".equalsIgnoreCase(dispLimit.trim())))
			dispLimit = "20";
		
		request.getSession().setAttribute("MAX_NUM_ROWS", dispLimit);
		
		String action = request.getParameter("tbAction");
		
		CodeRedImplem crDao = (CodeRedImplem)ObjectFactory.getObject("CodeRedDoer");
		HashMap<String, Object> values = new HashMap<String, Object>();
		values.put("limit",dispLimit);
		values.put("Request",request);
		List<CodeRedBean> beanList = (List<CodeRedBean>)request.getSession().getAttribute("beanList");
		
		CodeRedBean searchBean = new CodeRedBean();
		
		if("Previous".equalsIgnoreCase(action) && beanList != null)
			searchBean = beanList.get(0);
		else if("Next".equalsIgnoreCase(action) && beanList != null)
			searchBean = beanList.get(beanList.size() - 1);
		else{
//			searchBean.setDate(transLogBean.getDATE_FROM());
//			searchBean.setTime(transLogBean.getTIME_FROM());
			searchBean.setDateTime(transLogBean.getTIME_FROM_MS());
		}
		
		if("Previous".equalsIgnoreCase(action)){
//			values.put("DATE_TO", searchBean.getDate());
//			values.put("TIME_TO", searchBean.getTime());
			values.put("DATE_TIME_TO", searchBean.getDateTime());
		}else{
//			values.put("DATE_FROM", searchBean.getDate());
//			values.put("TIME_FROM", searchBean.getTime());
			values.put("DATE_TIME_FROM", searchBean.getDateTime() != null ? searchBean.getDateTime().trim() : "");
		}

		if(beanList == null)
			values.put("includeLast",true);
		
		List<CodeRedBean> resultList = crDao.getCodeRedLogLast(values);
		
		String[] dateTime;
		if(resultList == null || (resultList != null && resultList.isEmpty())){
			dateTime = searchBean.getDateTime().split(" ");
			request.getSession().removeAttribute("beanList");
		}else{
			dateTime = resultList.get(0).getDateTime().split(" ");
			request.getSession().setAttribute("beanList", resultList);
		}	
		
		transLogBean.setDATE_FROM(dateTime[0]);
		transLogBean.setTIME_FROM(dateTime[1]);		
		transLogBean.setTIME_FROM_MS(searchBean.getDateTime());
		modelMap.addAttribute("transaxnBean", transLogBean);
		return "base/CodeRedSearchLast";
	}
	
	private void setTime(HttpServletRequest request){
		Calendar calendar = Calendar.getInstance();
	    int year = calendar.get(Calendar.YEAR);
	    int month = calendar.get(Calendar.MONTH);
	    int day = calendar.get(Calendar.DATE);
	    calendar.set(year, month, day, 23, 59, 59);
	    Date today = calendar.getTime();
	    request.setAttribute("todayDate", today);
	}
}