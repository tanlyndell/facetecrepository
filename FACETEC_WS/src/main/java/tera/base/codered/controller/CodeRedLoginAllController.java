package tera.base.codered.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import tera.base.ObjectFactory;
import tera.base.codered.CodeRedSqlSession;
import tera.base.codered.bean.CodeRedDataBean;
import tera.base.codered.bean.CodeRedLoginBean;

@Controller
@SessionAttributes("transaxnBean")
@RequestMapping("CodeRedLoginAll") //CodeRedSearchAllLoginController
public class CodeRedLoginAllController {

	CodeRedSqlSession cbSqlSession = new CodeRedSqlSession();
	
	@RequestMapping(method = RequestMethod.GET)
	protected String formBackingObject(ModelMap modelMap, HttpServletRequest request)throws Exception {
		
		CodeRedDataBean crDataBean = new CodeRedDataBean();

		modelMap.addAttribute("transaxnBean", crDataBean);
		request.getSession().removeAttribute("fromCodeRedLoginAll");
		request.getSession().removeAttribute("fromCodeRedLogin");
		cbSqlSession.closeConnection();
		return "base/CodeRedLoginAll";
	}
	
	private boolean onBindAndValidate(ModelMap modelMap,HttpServletRequest request, HttpServletResponse response, CodeRedDataBean crDataBean, Errors errors){
		
		boolean err = false;

		// Login
		String usrid = request.getParameter("usr_id");
		String pass = request.getParameter("usr_password");
		
		CodeRedLoginBean loginBean = (CodeRedLoginBean)ObjectFactory.getObject("CodeRedLogin");

		if(usrid == null || (usrid != null && usrid.isEmpty()) ||
				pass == null || (pass != null && pass.isEmpty())){
			err = true;
			request.setAttribute("errMessage","Please fill up all mandatory fields");
		}
		
		else if(usrid == null || 
//				(usrid != null && loginBean !=null &&
				!usrid.equalsIgnoreCase(loginBean.getUserName()) //LEL20170710: Temp hard code
//				!usrid.equalsIgnoreCase("teramba")
				){
			err = true;
			request.setAttribute("errMessage","Invalid User ID");
		}
		
		else if(pass == null ||
//				(pass != null && loginBean != null &&
				!pass.equals(loginBean.getPassword()) //LEL20170710: Temp hard code
//				!pass.equals("P@ssw0rd")
				){
			err = true;
			request.setAttribute("errMessage","Invalid Password");
		}	
		
		// Database Details
		
		else if(crDataBean.getDbType() == null  || crDataBean.getDbType().isEmpty()){
			errors.rejectValue("dbType", "mandatory");
			err = true;
			request.setAttribute("errMessage","Please fill up all mandatory fields");
		}
		else if(crDataBean.getHostName() == null  || crDataBean.getHostName().isEmpty()){
			errors.rejectValue("hostName", "mandatory");
			err = true;
			request.setAttribute("errMessage","Please fill up all mandatory fields");
		}
		else if(crDataBean.getPort() == null  || crDataBean.getPort().isEmpty()){
			errors.rejectValue("port", "mandatory");
			err = true;
			request.setAttribute("errMessage","Please fill up all mandatory fields");
		}
		else if(crDataBean.getDbName() == null  || crDataBean.getDbName().isEmpty()){
			errors.rejectValue("dbName", "mandatory");
			err = true;
			request.setAttribute("errMessage","Please fill up all mandatory fields");
		}

		else if(crDataBean.getUsername() == null  || crDataBean.getUsername().isEmpty()){
			errors.rejectValue("username", "mandatory");
			err = true;
			request.setAttribute("errMessage","Please fill up all mandatory fields");
		}
		else if(crDataBean.getPassword() == null  || crDataBean.getPassword().isEmpty()){
			errors.rejectValue("password", "mandatory");
			err = true;
			request.setAttribute("errMessage","Please fill up all mandatory fields");
		}

		return err;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	protected String onSubmit(ModelMap modelMap,
									  HttpServletRequest request, 
									  HttpServletResponse response, 
									  @ModelAttribute("transaxnBean")CodeRedDataBean command, 
									  Errors errors,
									  String commandName)throws Exception{
		
		CodeRedDataBean crDataBean = (CodeRedDataBean)command;
		
		if(onBindAndValidate(modelMap, request, response, command, errors)){
			return "base/CodeRedLoginAll";
		}
		
		cbSqlSession.setCodeRedDataBean(crDataBean);
		if(!cbSqlSession.createCodeRedSession()){
			request.setAttribute("errMessage","Error connecting to database..");
			return "base/CodeRedLoginAll";
		}

		request.getSession().setAttribute("cbSqlSessionFactory",cbSqlSession.getSqlSessionFactory());

		request.getSession().setAttribute("fromCodeRedLoginAll",true);

		request.getSession().setAttribute("databaseName",crDataBean.getHostName()+":"+crDataBean.getDbName());

		String urlBack = request.getParameter("b");
		if(urlBack == null || urlBack.isEmpty()){
//			urlBack = "CodeRedSearchAll.htm";
			urlBack = "CodeRedSearch.htm";
		}
		
		return "redirect:"+urlBack;
	}

}
