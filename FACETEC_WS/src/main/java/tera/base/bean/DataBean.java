/**
 * Created: 01.10.2006
 * CLASS: DataBean.java
 * DESCRIPTION: 
 * 
 * 														--==MODIFICATION HISTORY==--
 * +: Added, -: Deleted, *: Modified
 * 
 * Date				Programmer		Fields/Methods						Action		Description
 * -----------------------------------------------------------------------------------------------------------------------------------------------------------------
 * 2013-05-20		NLA				serverError							+			attribute field for server type (not field field specific) errors
 */
package tera.base.bean;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreType;

/**
 * Insert the type's description here. Creation date: (1/10/2006 10:14:15 AM)
 * 
 * @author: Administrator
 */
@JsonIgnoreType
public abstract class DataBean implements Cloneable {

	@JsonIgnore
	private java.lang.String[] columnNames;

	@JsonIgnore
	private java.lang.Object[] data;
	@JsonIgnore
	private java.lang.String tableName;
	@JsonIgnore
	private java.util.HashMap resultMap;
	@JsonIgnore
	private int[] primaryKeys;
	@JsonIgnore
	private String[] columnTypes;
	@JsonIgnore
	private String[] dbType;
	@JsonIgnore
	private java.lang.String[] columnDesc;

	@JsonIgnore
	private int numlist;
	@JsonIgnore
	private int recordnum;

	@JsonIgnore
	private List pkey = new LinkedList();

	@JsonIgnore
	private String orderBy;
	@JsonIgnore
	private String orderSort;
	@JsonIgnore
	private String columnNamesStr;
	@JsonIgnore
	private String columnDBNamesStr;
	@JsonIgnore
	private String dbTypeStr;	
	@JsonIgnore
	private String ignoreCaseStr;	
	@JsonIgnore
	private String prevOrderBy;
	@JsonIgnore
	private String doSort;
	@JsonIgnore
	private int gotopage;
	@JsonIgnore
	private int gotomax;
	@JsonIgnore
	private String count;
	/* Added by LEL20130325 - For confirmation labels and values */
	@JsonIgnore
	private String tranDetails;
	/* Added by LEL20130325 */
	/* Added by FNP: 20140113 - For non-visible extra details */
	@JsonIgnore
	private String extTranDetails;
	/* End of Added by FNP: 20140113 */

	/** LGT 20170110: For audit on inquiry **/
	@JsonIgnore
	private String tableKey;
	@JsonIgnore
	private String tableKeyValue;
	/** LGT 20170110: For audit on inquiry **/
	@JsonIgnore
	private LinkedHashMap<String, String> confVal;
	@JsonIgnore
	private String serverError;
	
	@JsonIgnore
	private boolean search;
	
	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public String getServerError() {
		return serverError;
	}

	public void setServerError(String serverError) {
		this.serverError = serverError;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getOrderSort() {
		return orderSort;
	}

	public void setOrderSort(String orderSort) {
		this.orderSort = orderSort;
	}

	
	public String getColumnNamesStr() {
		return columnNamesStr;
	}

	public void setColumnNamesStr(String columnNamesStr) {
		this.columnNamesStr = columnNamesStr;
	}

	public String getColumnDBNamesStr() {
		return columnDBNamesStr;
	}

	public void setColumnDBNamesStr(String columnDBNamesStr) {
		this.columnDBNamesStr = columnDBNamesStr;
	}

	public String getDbTypeStr() {
		return dbTypeStr;
	}

	public void setDbTypeStr(String dbTypeStr) {
		this.dbTypeStr = dbTypeStr;
	}

	public String getIgnoreCaseStr() {
		return ignoreCaseStr;
	}

	public void setIgnoreCaseStr(String ignoreCaseStr) {
		this.ignoreCaseStr = ignoreCaseStr;
	}

	public String getPrevOrderBy() {
		return prevOrderBy;
	}

	public void setPrevOrderBy(String prevOrderBy) {
		this.prevOrderBy = prevOrderBy;
	}

	public String getDoSort() {
		return doSort;
	}

	public void setDoSort(String doSort) {
		this.doSort = doSort;
	}
	public int getGotopage() {
		return gotopage;
	}

	public void setGotopage(int gotopage) {
		this.gotopage = gotopage;
	}

	public int getGotomax() {
		return gotomax;
	}

	public void setGotomax(int gotomax) {
		this.gotomax = gotomax;
	}

	public void addPkey(String pk_key) {
		List tempList = this.getPkey();
		tempList.add(pk_key);
	}

	public List getPkey() {
		return pkey;
	}

	public void setPkey(List pkey) {
		this.pkey = pkey;
	}

	/**
	 * Returns a clone of this bean object
	 */
	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}

	@JsonIgnore
	private Object cloneBean;

	public Object getCloneBean() {
		return cloneBean;

	}

	public void setCloneBean(Object cloneBean) {
		this.cloneBean = cloneBean;
	}

	public int getRecordnum() {
		return recordnum;
	}

	public void setRecordnum(int recordnum) {
		this.recordnum = recordnum;
	}

	/**
	 * TransaxnBean constructor comment.
	 */
	public DataBean() {
		super();
		resultMap = new java.util.HashMap();
	}

	public int getNumlist() {
		return numlist;
	}

	public void setNumlist(int numlist) {
		this.numlist = numlist;
	}

	public void clearData() {
		data = new Object[data.length];
	}

	/**
	 * Insert the method's description here. Creation date: (1/11/2006 8:30:15
	 * AM)
	 * 
	 * @param c
	 *            java.lang.Class
	 */
	public void clearDataFields() {
		try {
			for (int i = 0; i < getColumnNames().length; i++) {
				this.getClass().getDeclaredField(getColumnNames()[i]).set(this, "");
			}
			resultMap.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void nullFields() {
		try {
			for (int i = 0; i < getColumnNames().length; i++) {
				this.getClass().getDeclaredField(getColumnNames()[i]).set(this, null);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void nullEmptyFields() {
		try {
			for (int i = 0; i < getColumnNames().length; i++) {

				if (this.getClass().getDeclaredField(columnNames[i]).get(this).equals("")) {
					// tera.servlet.TransaxnServlet.lastInstance.outputStatus(
					// (this.getClass().getDeclaredField(getColumnNames()[i])).toString()
					// );
					this.getClass().getDeclaredField(getColumnNames()[i]).set(this, null);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void clearFields() {
		try {
			for (int i = 0; i < getColumnNames().length; i++) {
				this.getClass().getDeclaredField(getColumnNames()[i]).set(this, "");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Insert the method's description here. Creation date: (1/11/2006 8:30:15
	 * AM)
	 * 
	 * @param c
	 *            java.lang.Class
	 */

	public void constructDataArray2() {
		try {
			System.out.println("column names length: " + columnNames.length);
			for (int i = 0; i < columnNames.length; i++) {
				System.out.println(columnNames[i]);
				System.out.println(this.getClass().getDeclaredField(columnNames[i]).get(this));
				setDataAt2(

						this.getClass().getDeclaredField(columnNames[i]).get(this), columnNames[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int findColumnIndex(String column) {
		for (int i = 0; i < columnNames.length; i++)
			if (columnNames[i].equals(column))
				return i;
		return -1;
	}

	/**
	 * Insert the method's description here. Creation date: (2/8/2006 2:57:05
	 * PM)
	 * 
	 * @return java.lang.String[]
	 */
	public java.lang.String[] getColumnDesc() {
		return columnDesc;
	}

	/**
	 * Insert the method's description here. Creation date: (2/8/2006 2:57:05
	 * PM)
	 * 
	 * @return java.lang.String[]
	 */
	public java.lang.String getColumnDesc(int i) {
		return columnDesc[i];
	}

	public java.lang.String getColumnDesc(String column) {
		int ind = findColumnIndex(column);
		if (ind != -1) {
			return columnDesc[ind];
		} else
			return column;
	}

	/**
	 * Insert the method's description here. Creation date: (1/10/2006 10:16:17
	 * AM)
	 * 
	 * @return java.lang.String[]
	 */
	public java.lang.String[] getColumnNames() {
		return columnNames;
	}

	/**
	 * Insert the method's description here. Creation date: (1/20/2006 9:05:27
	 * AM)
	 * 
	 * @return java.lang.String
	 * @param colNum
	 *            int
	 */

	public String getColumnTypeOf(String colName) {
		for (int i = 0; i < columnNames.length; i++) {
			if (columnNames[i].equalsIgnoreCase(colName)) {
				return columnTypes[i];
			}
		}
		return null;
	}

	public String getColumnTypeAt(int colNum) {
		return columnTypes[colNum];
	}

	/**
	 * Insert the method's description here. Creation date: (1/20/2006 9:04:48
	 * AM)
	 * 
	 * @return java.lang.String[]
	 */
	public String[] getColumnTypes() {
		return columnTypes;
	}

	/**
	 * Insert the method's description here. Creation date: (1/10/2006 10:16:38
	 * AM)
	 * 
	 * @return java.lang.Object[]
	 */
	public java.lang.Object[] getData() {
		return data;
	}

	/**
	 * Insert the method's description here. Creation date: (1/19/2006 5:36:56
	 * PM)
	 * 
	 * @return int[]
	 */
	public int[] getPrimaryKeys() {
		return primaryKeys;
	}

	/**
	 * Insert the method's description here. Creation date: (1/10/2006 10:43:41
	 * AM)
	 * 
	 * @return java.util.HashMap
	 */
	public java.util.HashMap getResultMap() {
		return resultMap;
	}

	/**
	 * Insert the method's description here. Creation date: (1/10/2006 10:43:41
	 * AM)
	 * 
	 * @return java.util.HashMap
	 */
	public Object getResultMap(String key) {
		return resultMap.get(key);
	}

	/**
	 * Insert the method's description here. Creation date: (1/10/2006 10:17:01
	 * AM)
	 * 
	 * @return java.lang.String
	 */
	public java.lang.String getTableName() {
		return tableName;
	}

	/**
	 * Insert the method's description here. Creation date: (1/11/2006 8:30:15
	 * AM)
	 * 
	 * @param c
	 *            java.lang.Class
	 */
	public void mapDataFields(String[] _columnNames, Object[] _data) {
		int i = 0;
		int index;

		try {
			for (i = 0; i < _columnNames.length; i++) {
				index = findColumnIndex(_columnNames[i].toLowerCase());

				if (index == -1)
					continue;

				this.getClass().getDeclaredField(_columnNames[i].toLowerCase()).set(this, _data[i]);
			}

		} catch (Exception e) {
			// TransaxnServlet.lastInstance.outputStatus(
			// "Error Occurredx! " + e.getMessage() + i);
		}

	}

	public void mapField(String _columnname, Object _data) {
		try {

			this.getClass().getDeclaredField(_columnname.toLowerCase()).set(this, _data);
		} catch (Exception e) {
			// TransaxnServlet.lastInstance.outputStatus(
			// "Error Occurredx! " + e.getMessage() + _columnname);
		}
	}


	/**
	 * Insert the method's description here. Creation date: (2/8/2006 2:57:05
	 * PM)
	 * 
	 * @param newColumnDesc
	 *            java.lang.String[]
	 */
	public void setColumnDesc(java.lang.String[] newColumnDesc) {
		columnDesc = newColumnDesc;
	}

	/**
	 * Insert the method's description here. Creation date: (1/10/2006 10:16:17
	 * AM)
	 * 
	 * @param newColumnNames
	 *            java.lang.String[]
	 */
	public void setColumnNames(java.lang.String[] newColumnNames) {
		columnNames = newColumnNames;
	}

	/**
	 * Insert the method's description here. Creation date: (1/20/2006 9:06:19
	 * AM)
	 * 
	 * @param colNum
	 *            int
	 * @param colType
	 *            java.lang.String
	 */
	public void setColumnTypeAt(int colNum, String colType) {
		this.columnTypes[colNum] = colType;
	}

	/**
	 * Insert the method's description here. Creation date: (1/20/2006 9:04:48
	 * AM)
	 * 
	 * @param newColumnTypes
	 *            java.lang.String[]
	 */
	public void setColumnTypes(String[] newColumnTypes) {
		columnTypes = newColumnTypes;
	}

	/**
	 * Insert the method's description here. Creation date: (1/10/2006 10:16:38
	 * AM)
	 * 
	 * @param newData
	 *            java.lang.Object[]
	 */
	public void setData(java.lang.Object[] newData) {
		data = newData;
	}

	public void setDataAt2(Object obj, String column) {
		int index = findColumnIndex(column);
		System.out.println("index: " + index);
		System.out.println("column: " + column);
		System.out.println("" + obj);
		if (index >= 0) {
			if (obj == null)
				data[index] = obj;
			else
				data[index] = "" + obj; // .toString();
		} else {
			System.out.println("Column not found! " + column);
		}

	}

	/**
	 * Insert the method's description here. Creation date: (1/19/2006 5:36:56
	 * PM)
	 * 
	 * @param newPrimaryColumn
	 *            int[]
	 */
	public void setPrimaryKeys(int[] newPrimaryColumn) {
		primaryKeys = newPrimaryColumn;
	}

	/**
	 * Insert the method's description here. Creation date: (1/10/2006 10:43:41
	 * AM)
	 * 
	 * @param newResultMap
	 *            java.util.HashMap
	 */
	public void setResultMap(String key, Object value) {
		resultMap.put(key, value);
	}

	/**
	 * Insert the method's description here. Creation date: (1/10/2006 10:43:41
	 * AM)
	 * 
	 * @param newResultMap
	 *            java.util.HashMap
	 */
	public void setResultMap(java.util.HashMap hm) {
		resultMap = hm;
	}

	/**
	 * Insert the method's description here. Creation date: (1/10/2006 10:17:01
	 * AM)
	 * 
	 * @param newTableName
	 *            java.lang.String
	 */
	public void setTableName(java.lang.String newTableName) {
		tableName = newTableName;
	}

	/**
	 * @return
	 */


	/**
	 * @param data
	 */

	public String escapeQuote(String parseString) {
		StringBuffer sb = new StringBuffer(parseString);
		for (int i = 0; i < sb.length(); i++) {
			if (sb.charAt(i) == '"') {
				// System.out.println("Before: "+sb.toString());
				sb.replace(i, i + 1, "\\\"");
				// System.out.println("After: "+sb.toString());
				i += 1;
			} else if (sb.charAt(i) == '\'') {
				// System.out.println("Before: "+sb.toString());
				sb.replace(i, i + 1, "\\'");
				// System.out.println("After: "+sb.toString());
				i += 1;
			}
		}
		return (sb.toString());
	}

	/**
	 * @return
	 */
	public String[] getDbType() {
		return dbType;
	}

	/**
	 * @param strings
	 */
	public void setDbType(String[] strings) {
		dbType = strings;
	}

	public String getTranDetails() {
		return tranDetails;
	}

	public String getExtTranDetails() {
		return extTranDetails;
	}

	public void setTranDetails(String tranDetails) {
		this.tranDetails = tranDetails;
	}

	public void setExtTranDetails(String extTranDetails) {
		this.extTranDetails = extTranDetails;
	}

	public LinkedHashMap<String, String> getConfVal() {
		return confVal;
	}

	public void setConfVal(LinkedHashMap<String, String> confVal) {
		this.confVal = confVal;
	}

	public String getTableKey() {
		return tableKey;
	}

	public void setTableKey(String tableKey) {
		this.tableKey = tableKey;
	}

	public String getTableKeyValue() {
		return tableKeyValue;
	}

	public void setTableKeyValue(String tableKeyValue) {
		this.tableKeyValue = tableKeyValue;
	}

}
