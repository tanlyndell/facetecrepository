/**
 * CLASS: InitParamBean.java
 * 
 * 														--==MODIFICATION HISTORY==--
 * +: Added, -: Deleted, *: Modified
 * 
 * Date				Programmer		Fields/Methods						Action		Description
 * -----------------------------------------------------------------------------------------------------------------------------------------------------------------
 * 20110202			NSV,MTA			txnLogFilesDirectory,				+			For Logging
 * 									maintLogFilesDirectory,	
 * 									txnServletLogFilesDirectory,
 * 									maxLogFiles, appLocation,
 * 									runtimeLocation
 * 20111220			MTA				serverCompName						+			For printer status
 * 20120119			MTA				printerIconTimer					+			Added Printer Icon Refresh Timer
 * 20130410			FNP				preLaunch							+			Indicator if transactions with pre_launch=Y are to be loaded upon startup
 * 20130417			FNP				emailHandler						+			Indicator if EmailHandlerThread should be automatically started
 * 20130831			FNP				threadLimit, emailHandlerThreshold	+			Indicator if for thread limit, wait threshol, wake-up interval for EmailHandler									
 *									emailHandlerWakeUpInt
 * 20131113			FNP				emailHost, emailPort				+
 * 									smsHost, smsPort
 * 20131219			LEL				siebelBackupEmail					+			Email address to which Siebel backup email will be sent
 * 20141118			FNP				ws_conn_timeout, ws_req_timeout,	+			Timeout settings for web service clients
 * 									ws_resp_timeout	
 * 20150304			FNP				misHandler							+			Indicator if MISLoggingJob should be automatically started
 * 20160107			LEL				heartbeatHandler,					+			Indicator if HeartbeatJob should be automatically started
 * 									heartbeatHandlerWakeUpInt
 * 20160120			LEL				heartbeatTimeoutThreshold			+			Time out threshold before legacy system warning
 **/
package tera.base.bean;

/**
 * @author tsi-kjvorit
 *
 */
public class InitParamBean 
{	

	protected int codeRedCleanupCounter = 0;
	protected int codeRedCleanupThreshold;
	protected String codeRedActivityMaxTime;
	
	public int getCodeRedCleanupCounter() {
		return codeRedCleanupCounter;
	}
	public void setCodeRedCleanupCounter(int codeRedCleanupCounter) {
		this.codeRedCleanupCounter = codeRedCleanupCounter;
	}
	public int getCodeRedCleanupThreshold() {
		return codeRedCleanupThreshold;
	}
	public void setCodeRedCleanupThreshold(int codeRedCleanupThreshold) {
		this.codeRedCleanupThreshold = codeRedCleanupThreshold;
	}
	public String getCodeRedActivityMaxTime() {
		return codeRedActivityMaxTime;
	}
	public void setCodeRedActivityMaxTime(String codeRedActivityMaxTime) {
		this.codeRedActivityMaxTime = codeRedActivityMaxTime;
	}

	
	
}
