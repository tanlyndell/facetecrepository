package tera.base.bean;

import com.tera.facetec.zoom.util.blowfish.BlowfishEasy;

public class DBDetailsEncryptBean extends DataBean
{
	private static BlowfishEasy blowFish = new BlowfishEasy("tera");
	
	private String username;
	private String password;
	private String encUsername;
	private String encPassword;
	private String decUsername;
	private String decPassword;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEncUsername() {
		return encUsername;
	}
	public void setEncUsername(String encUsername) {
		this.encUsername = encUsername;
	}
	public String getEncPassword() {
		return encPassword;
	}
	public void setEncPassword(String encPassword) {
		this.encPassword = encPassword;
	}
	public String getDecUsername() {
		return decUsername;
	}
	public void setDecUsername(String decUsername) {
		this.decUsername = decUsername;
	}
	public String getDecPassword() {
		return decPassword;
	}
	public void setDecPassword(String decPassword) {
		this.decPassword = decPassword;
	}
	
	public static synchronized String decrypt(String str) {
		//System.out.println("synchronized decrypt: " + str);
		return blowFish.decryptString(str);
	}
	
	public static synchronized String encrypt(String str) {
		//System.out.println("synchronized encrypt: " + str);
		return blowFish.encryptString(str);
	}
}
