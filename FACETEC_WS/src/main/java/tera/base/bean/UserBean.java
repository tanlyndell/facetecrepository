/**
 * CLASS: 			UserBean.java
 * AUTHOR: 			Louie John Leona
 * DATE CREATED:	2016/08/17
 * 														--==MODIFICATION HISTORY==--
 * +: Added, -: Deleted, *: Modified
 * 
 * Date				Programmer		Fields/Methods						Action		Description
 * -----------------------------------------------------------------------------------------------------------------------------------------------------------------
 *		
 */

package tera.base.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

public class UserBean extends DataBean {

	private String usrID;
	private String usrName;
	private String usrFirstName;
	private String usrMiddleName;
	private String usrLastName;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	private String usrPassword;
	
	private String usrNickname;
	private String usrMotherName;
	private String usrRC;
	private String usrRCName;
	private String usrAddress;
	private String usrTelNo;
	private String usrMobileNo;
	private String usrFaxNo;
	private String usrEmail;
	private String emailNotif;
	private java.sql.Timestamp usrPwExp;
	private String usrGroupInd;
	private String usrGroup;
	private String usrSuperInd;
	private String usrClassification;
	private String usrStatusInd;
	private java.sql.Timestamp usrExpDate;
	private String usrAllowLock;
	private String usrLockedInd;
	private String usrInvalidCount;
	private String usrInvCntCon;
	private Integer usrPwDays = new Integer(0);
	private String usrPwFreq;
	private java.sql.Timestamp loginDate;
	private java.sql.Timestamp logoutDate;
	private String usrAllowDeactivate;
	private String hasSecQstn;
	private String hasSigninSeal;
	private String usrDeleted;
	private String otpIndicator;
	private String usrInputBy;
	private java.sql.Timestamp usrInputDate;
	private String usrUpdatedBy;
	private java.sql.Timestamp usrUpdatedDate;
	private String sessionID;
	
	//for Transaction logging
	private String deviceType;
	
	//for Login
	private String expiredLoginDate;
	private String expiredUsrExpDateDays;
	private String loginDateString;
	private String usrPwRemainDaysString;
	private String lastPasswordChanged;

	//for Password Reset
	private String newUsrPassword;
	private String requiredNewPassword;
	private String confirmNewUsrPassword;
	private java.sql.Timestamp usrPwUnlockDate;
	private String usrPwTmpInd;
	
	//for User Definition
	private String roleName;
	private String roleNameString;
	private List<String> roles;
	
	//for User Definition indicators
	private String usrForceInd;
	
	//for User Management
	private String usrIP;
	private String usrPasswordTemp;
	protected Float usrPwRemainDays = new Float(0);
	private String warnNewPassword;
	private List<Object> selectedRolesID;
	
	//For Role Management
	private List<Object> usrList;
	private String usrListID;
	private String users;
	private String users2;
	private String ugrGroupID;
	
	// For Role Management Pending/Approval
	private String change;
	private String status;
	private String dateChanged;
	private String changedBy;
	private String remarks;
	
	//For User Management
	private String usrGrpRoles;
	private String usrPwExp2;
	private Integer usrExpDays = new Integer(0);
	private String usrExpFreq;
	private String usrExpDate2;
	private String usrUpdatedDate2;
	private String usrInputDate2;
	private String usrInactivity;
	
	private String usrGroupID;
	
	//Appraisal Field
	private String rcRegion;
	private String appraisalCompany;
	private String appraisalRc;
	
	private String usrLevel;
	private String usrLevelDesc;
	
	private String usrGroupDesc;
	private String usrClassificationDesc;
	
	private String ciCompany;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	private String jwtSignature;
	
	public String getUsrID() {
		return usrID;
	}
	public void setUsrID(String usrID) {
		this.usrID = usrID;
	}
	public String getUsrName() {
		if(getUsrFirstName() != null && getUsrLastName() != null)
			setUsrName(getUsrFirstName() + " " + (getUsrMiddleName() != null && getUsrMiddleName().length() > 0 ? getUsrMiddleName() + " " : "") + getUsrLastName());
		return usrName;
	}
	public void setUsrName(String usrName) {
		this.usrName = usrName;
	}
	public String getUsrFirstName() {
		return usrFirstName;
	}
	public void setUsrFirstName(String usrFirstName) {
		this.usrFirstName = usrFirstName;
	}
	public String getUsrMiddleName() {
		return usrMiddleName;
	}
	public void setUsrMiddleName(String usrMiddleName) {
		this.usrMiddleName = usrMiddleName;
	}
	public String getUsrLastName() {
		return usrLastName;
	}
	public void setUsrLastName(String usrLastName) {
		this.usrLastName = usrLastName;
	}
	public String getUsrPassword() {
		return usrPassword;
	}
	public void setUsrPassword(String usrPassword) {
		this.usrPassword = usrPassword;
	}
	public String getUsrNickname() {
		return usrNickname;
	}
	public void setUsrNickname(String usrNickname) {
		this.usrNickname = usrNickname;
	}
	public String getUsrMotherName() {
		return usrMotherName;
	}
	public void setUsrMotherName(String usrMotherName) {
		this.usrMotherName = usrMotherName;
	}
	public String getUsrRC() {
		return usrRC;
	}
	public void setUsrRC(String usrRC) {
		this.usrRC = usrRC;
	}
	public String getUsrAddress() {
		return usrAddress;
	}
	public void setUsrAddress(String usrAddress) {
		this.usrAddress = usrAddress;
	}
	public String getUsrTelNo() {
		return usrTelNo;
	}
	public void setUsrTelNo(String usrTelNo) {
		this.usrTelNo = usrTelNo;
	}
	public String getUsrMobileNo() {
		return usrMobileNo;
	}
	public void setUsrMobileNo(String usrMobileNo) {
		this.usrMobileNo = usrMobileNo;
	}
	public String getUsrFaxNo() {
		return usrFaxNo;
	}
	public void setUsrFaxNo(String usrFaxNo) {
		this.usrFaxNo = usrFaxNo;
	}
	public String getUsrEmail() {
		return usrEmail;
	}
	public void setUsrEmail(String usrEmail) {
		this.usrEmail = usrEmail;
	}
	public String getEmailNotif() {
		return emailNotif;
	}
	public void setEmailNotif(String emailNotif) {
		this.emailNotif = emailNotif;
	}
	public java.sql.Timestamp getUsrPwExp() {
		return usrPwExp;
	}
	public void setUsrPwExp(java.sql.Timestamp usrPwExp) {
		this.usrPwExp = usrPwExp;
	}
	public String getUsrGroupInd() {
		return usrGroupInd;
	}
	public void setUsrGroupInd(String usrGroupInd) {
		this.usrGroupInd = usrGroupInd;
	}
	public String getUsrGroup() {
		return usrGroup;
	}
	public void setUsrGroup(String usrGroup) {
		this.usrGroup = usrGroup;
	}
	public String getUsrSuperInd() {
		return usrSuperInd;
	}
	public void setUsrSuperInd(String usrSuperInd) {
		this.usrSuperInd = usrSuperInd;
	}
	public String getUsrClassification() {
		return usrClassification;
	}
	public void setUsrClassification(String usrClassification) {
		this.usrClassification = usrClassification;
	}
	public String getUsrStatusInd() {
		return usrStatusInd;
	}
	public void setUsrStatusInd(String usrStatusInd) {
		this.usrStatusInd = usrStatusInd;
	}
	public java.sql.Timestamp getUsrExpDate() {
		return usrExpDate;
	}
	public void setUsrExpDate(java.sql.Timestamp usrExpDate) {
		this.usrExpDate = usrExpDate;
	}
	public String getUsrAllowLock() {
		return usrAllowLock;
	}
	public void setUsrAllowLock(String usrAllowLock) {
		this.usrAllowLock = usrAllowLock;
	}
	public String getUsrLockedInd() {
		return usrLockedInd;
	}
	public void setUsrLockedInd(String usrLockedInd) {
		this.usrLockedInd = usrLockedInd;
	}
	public String getUsrInvalidCount() {
		return usrInvalidCount;
	}
	public void setUsrInvalidCount(String usrInvalidCount) {
		this.usrInvalidCount = usrInvalidCount;
	}
	public String getUsrInvCntCon() {
		return usrInvCntCon;
	}
	public void setUsrInvCntCon(String usrInvCntCon) {
		this.usrInvCntCon = usrInvCntCon;
	}
	public Integer getUsrPwDays() {
		return usrPwDays;
	}
	public void setUsrPwDays(Integer usrPwDays) {
		this.usrPwDays = usrPwDays;
	}
	public String getUsrPwFreq() {
		return usrPwFreq;
	}
	public void setUsrPwFreq(String usrPwFreq) {
		this.usrPwFreq = usrPwFreq;
	}
	public java.sql.Timestamp getLoginDate() {
		return loginDate;
	}
	public void setLoginDate(java.sql.Timestamp loginDate) {
		this.loginDate = loginDate;
	}
	public java.sql.Timestamp getLogoutDate() {
		return logoutDate;
	}
	public void setLogoutDate(java.sql.Timestamp logoutDate) {
		this.logoutDate = logoutDate;
	}
	public String getUsrAllowDeactivate() {
		return usrAllowDeactivate;
	}
	public void setUsrAllowDeactivate(String usrAllowDeactivate) {
		this.usrAllowDeactivate = usrAllowDeactivate;
	}
	public String getHasSecQstn() {
		return hasSecQstn;
	}
	public void setHasSecQstn(String hasSecQstn) {
		this.hasSecQstn = hasSecQstn;
	}
	public String getHasSigninSeal() {
		return hasSigninSeal;
	}
	public void setHasSigninSeal(String hasSigninSeal) {
		this.hasSigninSeal = hasSigninSeal;
	}
	public String getUsrDeleted() {
		return usrDeleted;
	}
	public void setUsrDeleted(String usrDeleted) {
		this.usrDeleted = usrDeleted;
	}
	public String getOtpIndicator() {
		return otpIndicator;
	}
	public void setOtpIndicator(String otpIndicator) {
		this.otpIndicator = otpIndicator;
	}
	public String getUsrInputBy() {
		return usrInputBy;
	}
	public void setUsrInputBy(String usrInputBy) {
		this.usrInputBy = usrInputBy;
	}
	public java.sql.Timestamp getUsrInputDate() {
		return usrInputDate;
	}
	public void setUsrInputDate(java.sql.Timestamp usrInputDate) {
		this.usrInputDate = usrInputDate;
	}
	public String getUsrUpdatedBy() {
		return usrUpdatedBy;
	}
	public void setUsrUpdatedBy(String usrUpdatedBy) {
		this.usrUpdatedBy = usrUpdatedBy;
	}
	public java.sql.Timestamp getUsrUpdatedDate() {
		return usrUpdatedDate;
	}
	public void setUsrUpdatedDate(java.sql.Timestamp usrUpdatedDate) {
		this.usrUpdatedDate = usrUpdatedDate;
	}
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getExpiredLoginDate() {
		return expiredLoginDate;
	}
	public void setExpiredLoginDate(String expiredLoginDate) {
		this.expiredLoginDate = expiredLoginDate;
	}
	public String getExpiredUsrExpDateDays() {
		return expiredUsrExpDateDays;
	}
	public void setExpiredUsrExpDateDays(String expiredUsrExpDateDays) {
		this.expiredUsrExpDateDays = expiredUsrExpDateDays;
	}
	public String getNewUsrPassword() {
		return newUsrPassword;
	}
	public void setNewUsrPassword(String newUsrPassword) {
		this.newUsrPassword = newUsrPassword;
	}
	public String getRequiredNewPassword() {
		return requiredNewPassword;
	}
	public void setRequiredNewPassword(String requiredNewPassword) {
		this.requiredNewPassword = requiredNewPassword;
	}
	public String getConfirmNewUsrPassword() {
		return confirmNewUsrPassword;
	}
	public void setConfirmNewUsrPassword(String confirmNewUsrPassword) {
		this.confirmNewUsrPassword = confirmNewUsrPassword;
	}
	public java.sql.Timestamp getUsrPwUnlockDate() {
		return usrPwUnlockDate;
	}
	public String getUsrPwTmpInd() {
		return usrPwTmpInd;
	}
	public void setUsrPwUnlockDate(java.sql.Timestamp usrPwUnlockDate) {
		this.usrPwUnlockDate = usrPwUnlockDate;
	}
	public void setUsrPwTmpInd(String usrPwTmpInd) {
		this.usrPwTmpInd = usrPwTmpInd;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getUsrForceInd() {
		return usrForceInd;
	}
	public void setUsrForceInd(String usrForceInd) {
		this.usrForceInd = usrForceInd;
	}
	public String getUsrIP() {
		return usrIP;
	}
	public void setUsrIP(String usrIP) {
		this.usrIP = usrIP;
	}
	public String getUsrPasswordTemp() {
		return usrPasswordTemp;
	}
	public void setUsrPasswordTemp(String usrPasswordTemp) {
		this.usrPasswordTemp = usrPasswordTemp;
	}
	public Float getUsrPwRemainDays() {
		return usrPwRemainDays;
	}
	public void setUsrPwRemainDays(Float usrPwRemainDays) {
		this.usrPwRemainDays = usrPwRemainDays;
	}
	public String getWarnNewPassword() {
		return warnNewPassword;
	}
	public void setWarnNewPassword(String warnNewPassword) {
		this.warnNewPassword = warnNewPassword;
	}
	public List<Object> getSelectedRolesID() {
		return selectedRolesID;
	}
	public void setSelectedRolesID(List<Object> selectedRolesID) {
		this.selectedRolesID = selectedRolesID;
	}
	public List<Object> getUsrList() {
		return usrList;
	}
	public void setUsrList(List<Object> usrList) {
		this.usrList = usrList;
	}
	public String getUsrListID() {
		return usrListID;
	}
	public void setUsrListID(String usrListID) {
		this.usrListID = usrListID;
	}
	public String getUsers() {
		return users;
	}
	public void setUsers(String users) {
		this.users = users;
	}
	public String getUsers2() {
		return users2;
	}
	public void setUsers2(String users2) {
		this.users2 = users2;
	}
	public String getUgrGroupID() {
		return ugrGroupID;
	}
	public void setUgrGroupID(String ugrGroupID) {
		this.ugrGroupID = ugrGroupID;
	}
	public String getChange() {
		return change;
	}
	public void setChange(String change) {
		this.change = change;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDateChanged() {
		return dateChanged;
	}
	public void setDateChanged(String dateChanged) {
		this.dateChanged = dateChanged;
	}
	public String getChangedBy() {
		return changedBy;
	}
	public void setChangedBy(String changedBy) {
		this.changedBy = changedBy;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getUsrGrpRoles() {
		return usrGrpRoles;
	}
	public void setUsrGrpRoles(String usrGrpRoles) {
		this.usrGrpRoles = usrGrpRoles;
	}
	public String getUsrPwExp2() {
		return usrPwExp2;
	}
	public void setUsrPwExp2(String usrPwExp2) {
		this.usrPwExp2 = usrPwExp2;
	}

	public Integer getUsrExpDays() {
		return usrExpDays;
	}
	public void setUsrExpDays(Integer usrExpDays) {
		this.usrExpDays = usrExpDays;
	}
	public String getUsrExpFreq() {
		return usrExpFreq;
	}
	public void setUsrExpFreq(String usrExpFreq) {
		this.usrExpFreq = usrExpFreq;
	}
	public String getUsrExpDate2() {
		return usrExpDate2;
	}
	public void setUsrExpDate2(String usrExpDate2) {
		this.usrExpDate2 = usrExpDate2;
	}
	public String getUsrUpdatedDate2() {
		return usrUpdatedDate2;
	}
	public void setUsrUpdatedDate2(String usrUpdatedDate2) {
		this.usrUpdatedDate2 = usrUpdatedDate2;
	}
	public String getUsrInputDate2() {
		return usrInputDate2;
	}
	public void setUsrInputDate2(String usrInputDate2) {
		this.usrInputDate2 = usrInputDate2;
	}
	public String getUsrInactivity() {
		return usrInactivity;
	}
	public void setUsrInactivity(String usrInactivity) {
		this.usrInactivity = usrInactivity;
	}
	public String getUsrGroupID() {
		return usrGroupID;
	}
	public void setUsrGroupID(String usrGroupID) {
		this.usrGroupID = usrGroupID;
	}
	public List<String> getRoles() {
		return roles;
	}
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public String getRcRegion() {
		return rcRegion;
	}
	public void setRcRegion(String rcRegion) {
		this.rcRegion = rcRegion;
	}
	public String getAppraisalCompany() {
		return appraisalCompany;
	}
	public void setAppraisalCompany(String appraisalCompany) {
		this.appraisalCompany = appraisalCompany;
	}

	public String getUsrRCName() {
		return usrRCName;
	}
	public void setUsrRCName(String usrRCName) {
		this.usrRCName = usrRCName;
	}
	public String getAppraisalRc() {
		return appraisalRc;
	}
	public void setAppraisalRc(String appraisalRc) {
		this.appraisalRc = appraisalRc;
	}
	public String getUsrLevel() {
		return usrLevel;
	}
	public void setUsrLevel(String usrLevel) {
		this.usrLevel = usrLevel;
	}
	public String getUsrLevelDesc() {
		return usrLevelDesc;
	}
	public void setUsrLevelDesc(String usrLevelDesc) {
		this.usrLevelDesc = usrLevelDesc;
	}
	
	public String getUsrGroupDesc() {
		return usrGroupDesc;
	}
	public void setUsrGroupDesc(String usrGroupDesc) {
		this.usrGroupDesc = usrGroupDesc;
	}
	public String getJwtSignature() {
		return jwtSignature;
	}
	public void setJwtSignature(String jwtSignature) {
		this.jwtSignature = jwtSignature;
	}
	public String getUsrClassificationDesc() {
		return usrClassificationDesc;
	}
	public void setUsrClassificationDesc(String usrClassificationDesc) {
		this.usrClassificationDesc = usrClassificationDesc;
	}
	public String getCiCompany() {
		return ciCompany;
	}
	public void setCiCompany(String ciCompany) {
		this.ciCompany = ciCompany;
	}
	public String getLoginDateString() {
		return loginDateString;
	}
	public void setLoginDateString(String loginDateString) {
		this.loginDateString = loginDateString;
	}
	public String getUsrPwRemainDaysString() {
		return usrPwRemainDaysString;
	}
	public void setUsrPwRemainDaysString(String usrPwRemainDaysString) {
		this.usrPwRemainDaysString = usrPwRemainDaysString;
	}
	public String getLastPasswordChanged() {
		return lastPasswordChanged;
	}
	public void setLastPasswordChanged(String lastPasswordChanged) {
		this.lastPasswordChanged = lastPasswordChanged;
	}
	public String getRoleNameString() {
		return roleNameString;
	}
	public void setRoleNameString(String roleNameString) {
		this.roleNameString = roleNameString;
	}
	


}