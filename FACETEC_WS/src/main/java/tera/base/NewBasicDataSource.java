package tera.base;

import org.apache.commons.dbcp.BasicDataSource;

import tera.base.bean.DBDetailsEncryptBean;

public class NewBasicDataSource extends BasicDataSource{

/*	@Override
	protected synchronized DataSource createDataSource() throws SQLException{
		System.out.println("username: " + super.getUsername());
		System.out.println("password: " + super.getPassword());
		String decryptedUsername = DBDetailsEncryptBean.decrypt(super.getUsername());
		String decryptedPassword = DBDetailsEncryptBean.decrypt(super.getPassword());
		super.setUsername(decryptedUsername);
		super.setPassword(decryptedPassword);
		return super.createDataSource();
	}*/
	
   @Override
    public synchronized void setPassword(String password) {    
	    System.out.println("encrypted password: " + password);
        super.setPassword(DBDetailsEncryptBean.decrypt(password));
    }
	 
   @Override
   public synchronized void setUsername(String username) {    
	   System.out.println("ecnrypted username: " + username);
       super.setUsername(DBDetailsEncryptBean.decrypt(username));
   }
   
}
