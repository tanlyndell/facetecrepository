<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
	<style type="text/css">
		.mandatory{ 
		color:#FF0000; 
		float:left; 
		margin-left:5px; 
		text-align:left
	}
	</style>
<script type="text/javascript">
function doClearForm() {
	document.getElementById('usr_id').value='';
	document.getElementById('usr_password').value='';
	document.getElementById('errTD').innerHTML = '';
}
</script>

</head>
<body scroll="no" onload="init();">

<div id="loginForm" style="width:50%;margin:0 auto">
<fieldset>
	<legend>Login</legend>
	
		<form method="post" name="txnBean" id="txnBean" autocomplete="off">
			<table align="center">
				<tr>
					<td class="label">User ID:</td>
					<td align="center"></td>
					<td>
						<input type="text" name="usr_id" id="usr_id"/><span class="mandatory">*</span>
					</td>
				</tr>
		
				<tr>
					<td class="label">Password:</td>
					<td align="center"></td>
					<td>
						<input type="password" name="usr_password" id="usr_password"/><span class="mandatory">*</span>
					</td>
				</tr>
				
				<tr>
					<td colspan="2"></td>
					<td align="right">
							<input class="btn" name="submit" id="sign_in" type="submit" value="Login" value="Sign In" >
							<input name="clear" type="button" class="btn" value="Refresh" onClick="doClearForm('txnBean', false);usr_id.focus();" />
					</td>
					
				</tr>
				<tr>
					<td colspan="3" id="errTD">
						<c:if test="${not empty errMessage }">
							<span style="color:red"><c:out value="${errMessage }"/></span>
						</c:if>
					</td>
				</tr>
			</table>
		</form>
		
</fieldset>

<div id="footer">
<div>
<%--a href="http://www.terasystem.com">Go to Main Site</a> | <a href="#">Contact Us</a><br--%>
Copyright � 2015 Terasystem Inc. All Rights Reserved.
</div>
</div>
</div>

</body>
</html>
