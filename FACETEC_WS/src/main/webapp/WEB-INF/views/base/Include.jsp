<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>

<!-- workaround to stop clearing browser cache whenever .js files are updated -->
<meta http-equiv="Cache-control" content="no-cache">

<link rel="stylesheet" href="resources/css/bootstrap.min.css">
<link rel="stylesheet" href="resources/css/datetimepicker_css/bootstrap-datetimepicker2.css">
<link rel="stylesheet" href="resources/css/normalize.css">
<link rel="stylesheet" href="resources/css/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="resources/css/web.css">
<link rel="stylesheet" href="resources/css/mobile.css">
<link rel="stylesheet" href="resources/css/jquery-ui.min.css">
<link rel="stylesheet" href="resources/css/pw-meter.css">


<script type="text/javascript" src="resources/js/jquery-1.12.1.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/generic.js"></script>
<script type="text/javascript" src="resources/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="resources/js/date.js"></script>
<script type="text/javascript" src="resources/js/dateformat.js"></script>
<script type="text/javascript" src="resources/js/dateField.js"></script>
<script type="text/javascript" src="resources/js/colResizable-1.6.min.js"></script>
<script type="text/javascript" src="resources/js/maskedInput.js"></script>
<script type="text/javascript" src="resources/js/moment-2.9.0.js"></script>
<script type="text/javascript" src="resources/js/bootstrap-datetimepicker-4.17.37.js"></script>
<script type="text/javascript" src="resources/js/jquery.caret.js"></script>
<script type="text/javascript" src="resources/notif/sockjs.min.js"></script>
<script type="text/javascript" src="resources/notif/stomp.min.js"></script>

<script>

	var t;
	var sl;
	var tl;
	var lockAlertShown = false;		
	var sessWarnShown = false;
	var showLockAlert = false;
	var screenLockInd = '${SCREEN_LOCK_IND}';
	var sessWarnInd = '${SESS_WARN_IND}';
	var sessExpInd = '${SESS_EXP_IND}';
	var passWarn = '${passWarn}'
	var hasChanges = false;
	var unlock = true;
	var url = window.location.pathname;
	function getHasChanges(){
		return hasChanges;
	}
	function setHasChanges(changes){
		hasChanges = changes;
	}
	function getUnlock(){
		return unlock;
	}
	function setUnlock(unlock){
		unlock = unlock;
	}

	window.onbeforeunload = function(){
		//Unlock record		 
		if(getUnlock() && $('#lockid').length > -1){
			$.ajax({
			      type: "POST", 
			      url: "RecordLockServlet", 
			      data: {
			    	  lockid: $('#lockid').val(),
			    	  locktask: 'unlock'
			   		}
			      });
		}
		
		
		if(hasChanges)
			return 'There are changes made. Are you sure you want to continue?';
	}
	
	function logout(){
		window.onbeforeunload = null
		window.location.href = 'Logout.htm?timeout=y';
	}
	
	function forceLogout(){
		
		var timeAfterAlert = new Date();
		var timeNow = timeShowed;
		if( ( timeAfterAlert.getTime() - timeNow.getTime() ) > sessWarn )
		{
			$( 'a' ).attr( 'href' , '');
			$( 'a' ).bind( 'click' , '');
	
			/* var params = new Object();
        	params["confMessage"] = "Session Timeout"
        	params["confBody"] = "You have not used the site for a while. For your security, your connection has been closed.";
			params["rButtonFunction"] = logout;
			params["data-backdrop"] = "static"
			showAlertBox( params ); */
			updateSessAttr("SESS_EXP_IND","ADD",true);
			logout();
	    }else{
	    	sessCheck();
	    	modalClose('modalAlert');
	    	sessWarnShown = false;
	    	updateSessAttr("SESS_WARN_IND","REMOVE",null);
	    	updateSessAttr("SESS_REFRESH","SESS_REFRESH",null);
	    	if(showLockAlert && url.indexOf('Logout.htm') < 0){
	    		lockAlertShown =false;
	    		lockScreen();
	    	}	
	    }
	}

	function warn()
	{
		logTime = sessTimeout;
		timer = (sessTimeout-sessWarn)/60000;
		
		msg1 = (logTime/60000) > 1 ? "minutes" : "minute";
		msg2 = timer > 1 ? timer+" minutes" : "one minute";		
		
		$.ajax( {
		      type: "POST", url: "SessionMaintenanceServlet", data: "type=CHECK",success: 
		      function(data)
		      {
		        if(data == "-1")
		        {
		        	timeShowed = new Date();
	
		        	var params = new Object();
		        	params["confMessage"] = "Session Warning"
		        	params["confBody"] = "You have not used the site for a while. You can extend your connection for "+(logTime/60000)+" more "+msg1+".<br>"+
					  "(For your security, your connection will close if there is no activity within "+msg2+".)<br>"+
					  "Would you like to extend your connection?";
					params["rButtonFunction"] = forceLogout;
					params["data-backdrop"] = "static"
					$('.modal').modal('hide');
					showAlertBox( params );
					sessWarnShown = true;
					updateSessAttr("SESS_WARN_IND","ADD",true);
		        }
		        else {
					splitStr = data.split(",");
					sessTimeout = splitStr[0];
					sessWarn = splitStr[1];
		        	sessCheck();
		        }			        	
		      }
		    } );
	}

	function checkPass(){
		
		if($.trim($('#lockPass').val()) == ''){
			showErrorOnField($('#lockPass'),"Mandatory");
		}else{
			$.ajax({
			      type: "POST", 
			      url: "SessionMaintenanceServlet", 
			      data: {
			    	  type:"CHECKPASS",
			    	  pass:$.trim($('#lockPass').val())
			   		},
			      success: function(data){
			    	  removeErrorOnField($('#lockPass'));
			    		if(data=='valid'){
			    			modalClose('modalConfirmSession');
			    			lockAlertShown = false;
			    			showLockAlert = false;
			    			lockCheck();
			    			updateSessAttr("SCREEN_LOCK_IND","REMOVE",null);
			    			updateSessAttr("SESS_REFRESH","SESS_REFRESH",null);//Reset session timeout
			    		}else if(data=='logout'){
			    			logout();
			    		}else{
			    			showErrorOnField($('#lockPass'),"Invalid Password");
			    		}
			      	}
			      });
		}
	}
	
	//add or remove session attribute
	function updateSessAttr(attr,attrAction,attrVal){
		$.ajax({
		      type: "POST", 
		      url: "SessionMaintenanceServlet", 
		      data: {
		    	  type: "SESSUPDATE",
		    	  attr:attr,
		    	  attrAction:attrAction,
		    	  attrVal:attrVal
		   		}
		      });
	}
	
	function cancelCheckPass(){};
	function lockScreen(){};
	
	cancelCheckPass = function(){
		
		var params = new Object();
    	params["confMessage"] = "Log-on"
    	params["confBody"] = "This will close the whole session. Do you still want to continue?";
		params["lButtonFunction"] = lockScreen;
		params["rButtonFunction"] = logout;
		params["rButton"] = "Yes";
		params["lButton"] = "No";
		params["preventClose"] = true;
		params["disableLinks"] = false;
		params["modalID"] = "modalConfirmSession";
		lockAlertShown = false;
		showConfirmationBox( params );
	}
	
	lockScreen = function(){	
		if(!lockAlertShown && !sessWarnShown){
			var params = new Object();
			var usrID = '${userBean.usrID}';
	    	params["confMessage"] = "Log-on"
	    	params["confBody"] = "<div class='form-group'><div class='col-sm-12' style='padding-bottom:5px;'><input type='text' readonly='readonly' id='lockUserID' name='lockUserID' class='form-control' value='"+usrID+"' onkeydown='preventESC(event);' /></div></div>"+
	    		"<div class='form-group'><div class='col-sm-12'><input type='password' id='lockPass' name='lockPass' class='form-control' placeholder='Enter Password' onkeydown='preventESC(event);' /></div></div>";
			params["rButtonFunction"] = checkPass;
			params["lButtonFunction"] = cancelCheckPass;
			params["disableLinks"] = false;
			params["preventClose"] = true;
			params["modalID"] = "modalConfirmSession";
			showConfirmationBox( params );
			lockAlertShown = true;
			showLockAlert = true;
			updateSessAttr("SCREEN_LOCK_IND","ADD",true);
		}else
			lockCheck();
	}
	
	function sessCheck()
	{
		clearTimeout(t);
		t = setTimeout(warn,sessWarn);
		clearTimeout(tl);
		tl = setTimeout(logout,sessTimeout);
		
	}
	
	function lockCheck(){
		clearTimeout(sl);
		sl = setTimeout(lockScreen,lock);
	}
	$(function(){
		
		if(sessExpInd == 'true'){
			disableLinks();
			logout();
		}else{	
			enableLinks();
			if('${preLogin}' != 'true'){
				
				if(sessWarnInd == 'true'){
					sessWarnShown = true;
					warn();
				}else
					sessCheck();
				
				if(lock != 0){
					if(screenLockInd  == 'true' && url.indexOf('Logout.htm') < 0)
						lockScreen();
					else
						lockCheck();	
				}
			}	
		}
		
		if(passWarn != ''){
			var params = new Object();
        	params["confMessage"] = 'Password Warning';
        	params["confBody"] = 'Your password will expire in '+'${passWarn}'+' day(s)';
			params["data-backdrop"] = "static"
			showAlertBox( params );
		}
		
		//Include x button to return to dashboard/home page
		$('div#app-body').find('h1').append('<button type="button" id="dashboardBtn" class="btn btn-default" data-toggle="tooltip"' 
			+'title="Close" onclick="window.location=\'Home.htm?dashboard=true\'"><i class="fa fa-times"></i></button>')
	});
</script>