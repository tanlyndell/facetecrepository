<%-- <%@ page isELIgnored ="false" %>  --%>
<!-- <!doctype html> -->
<%@ page contentType="text/html; charset=iso-8859-1" pageEncoding="ISO-8859-1" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%-- <%@ taglib prefix="form" uri="/spring-form" %> --%>
<%-- <%@ taglib prefix="form" uri="/META-INF/spring-form.tld" %> --%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
<!-- <meta http-equiv="content-type" content="text/html; charset=utf-8" /> -->
	<title>Code Red - Search</title>
	<link rel="stylesheet" href="resources/css/bootstrap.min.css">
	<link rel="stylesheet" href="resources/css/datetimepicker_css/bootstrap-datetimepicker2.css">
	<style>
		.tdLabel
		{
			text-align:right;
			width:120px;
		}
		
		/*Error --------------------------------------*/
		.errMsg{ 
			text-align:center; color:#FF0000; 
			background:#FFFFCC; 
			padding:10px 5px; 
			border-bottom:1px solid #996600; 
			clear:both
		}
		.errMsgPerField{ 
			font-style:oblique; font-size:0.9em; 
			color:#FF0000; 
			clear:both; 
		}
		.errHighlight{border:1px solid #FF0000}
		
		.ui-datepicker-trigger {
		    width: 19px;
		    height: 21px;
		    vertical-align: middle;
		}
		
		span.input-group-btn button{
			padding-top: 4px;
	    	padding-bottom: 4px;
	    }
	    
	    #filter td, th{
	    	padding:2px;
	    }
	</style>
	
	<!-- <script type="text/javascript" src="js/SpringWeb.js"></script>
	<script type="text/javascript" src="js/generic_front.js"></script> -->
	<script type="text/javascript" src="resources/js/jquery-1.12.1.min.js"></script>
	<script type="text/javascript" src="resources/js/moment-2.9.0.js"></script>
	<script type="text/javascript" src="resources/js/bootstrap-datetimepicker-4.17.37.js"></script>	
	<script type="text/javascript" src="resources/js/timepicker/jquery.timeentry.min.js"></script>
	<script type="text/javascript">
		var defaultValues;
		var maxRow = "<c:out value = '${MAX_NUM_ROWS}'/>";
		var currDate = new Date();

		$(document).ready( function(){
			/* $( '#dateFromDP' ).datepicker({
				altFormat: "yy-mm-dd",
				altField: '#DATE_FROM',
		        buttonImage: 'resources/datepicker/Calendar.png',
		        buttonImageOnly: true,
		        changeMonth: true,
		        changeYear: true,
		        showOn: 'button',
		        maxDate:  'today'
			});
			 */
			 $( '.dateFrom' ).datetimepicker({format: 'YYYY-MM-DD',sideBySide:true, maxDate:  '${todayDate}'});
			
			$("#submitButton, .submit").click( function(){
				clearErrorMessages();
				$("#jsEnabled").val('true');
				$("#tbAction").val($(this).val());
				if($.trim($('#TIME_FROM').val()) == '' && $.trim($('#DATE_FROM').val()) != '')
					$('#TIME_FROM').val('00:00:00');
				
				if($(this).attr('id') == 'submitButton')
					$('#TIME_FROM_MS').val($('#DATE_FROM').val()+' '+$('#TIME_FROM').val());
				
				document.forms['transaxnBean'].submit();
				
			});
			
			$("#clearButton").click( function(){
				doReset();
			});
			

			$('#TIME_FROM').timeEntry({
				/*spinnerImage:'',*/
				ampmPrefix:' ',
				showSeconds: true,
				show24Hours: true
			});
			
			$('#TIME_FROM').keydown(function(e){
				return checkKey(e);
			});	
			
			$(document).keydown(function(e){
				var key = e.keyCode;
				
				if(key == '13')
					$('#submitButton').click();
				if(key == '33' || key == '118')
					$('#btnPrev').click();
				if(key == '34' || key == '119')
					$('#btnNext').click();
			});
			
			$("#submitButton").focus();
			
			$("#contents").show();
			$('#btnPrev').blur();
			$('#btnPrev').focusout();
			$('#DATE_FROM').focus();
			
			
		});
		
		function doSubmit()
		{			
			
			return true;
		}
		function checkMandatoryEOL(formName,checkInvalidChars){
			var prevLabel = '';
			var labels = '';
			var values = '';
			var noError = true;
			
			
			$('#'+formName+ ' [mandatory="true"]').each(function(index, value){
				
				if($(this).is(":visible")){
					removeErrorOnField($(this));				
					var mandatory = 'Mandatory';
			
					var type = $(this).prop('tagName'); 
					var ok = true;
					if(type == 'INPUT'){
						if(!($(this).is(':radio') || $(this).is(':checkbox'))){
							if($.trim($(this).val()) == ''){
								showErrorOnField($(this),mandatory);
								noError = false;
							}				
						}else if($(this).is(':radio') || $(this).is(':checkbox')){// for radio button
							var name = $(this).attr('name');
							if($('input[name="'+name+'"]:checked').length < 1){
								showErrorOnField($(this),mandatory);
								noError = false;
							}
						}
					}else if(type == 'SELECT'){//for select
						if($(this).find('option:selected').val()==''){
							showErrorOnField($(this),mandatory);
							noError = false;
						}
					}else if(type == 'TEXTAREA'){
						if($.trim($(this).val()) == ''){
							showErrorOnField($(this),mandatory);
							noError = false;
						}	
					}else /*if(type == 'TD')*/{
						var hasValue = true;
						
						$(this).find("input[exemptManda!='true'], select[exemptManda!='true']").each(function(index,value){
							
							//if(($(this).attr("exemptManda") != "true" || $(this).attr("exemptManda") != true)){
							if($(this).prop('tagName') == 'SELECT'){
								if($.trim($(this).find('option:selected').val())==''){
									hasValue = false;
								}
							}else if($(this).prop('tagName') == 'INPUT'){
								if(!($(this).is(':radio') || $(this).is(':checkbox')) && 
									($.trim($(this).val())=='') ){
									hasValue = false;
								}
								else if($(this).is(':radio') || $(this).is(':checkbox')){// for radio button and checkbox
									var name = $(this).attr('name');
									if($('input[name="'+name+'"]:checked').length < 1){
										hasValue = false;
									}
								}
							}
							
							if(!hasValue){
								$(this).addClass("errHighlight");
								$(this).focus();
							}
							//}
						});
						if(!hasValue){
							//showErrorOnField($(this).find("input[exemptManda!='true'], select[exemptManda!='true']").last(),mandatory,true);
							//$(this).find("[exemptManda!='true']").addClass('errHighlight');
							$(this).find("input[exemptManda!='true'], select[exemptManda!='true']").last().parent().append('<div class="errMsgPerField">'+mandatory+'</div>');
							noError = false;
						}
					}
				}
			});
			
			return noError;
		}
		
		function clearForm(formIdent) 
		{ 
		  var form, elements, i, elm; 
		  form = document.getElementById 
		    ? document.getElementById(formIdent) 
		    : document.forms[formIdent]; 

			if (document.getElementsByTagName)
			{
				elements = form.getElementsByTagName('input');
				for( i=0, elm; elm=elements.item(i++); )
				{
					if (elm.getAttribute('type') == "text")
					{
						elm.value = '';
					}
					
					if (elm.getAttribute('type') == "radio")
					{
						elm.checked = false;
					}
				}
				elements = form.getElementsByTagName('select');
				for( i=0, elm; elm=elements.item(i++); )
				{
						elm.value = '';
				}
			}

			
			else
			{
				elements = form.elements;
				for( i=0, elm; elm=elements[i++]; )
				{
					if (elm.type == "text")
					{
						elm.value ='';
					}
				}
			}
		}
		function showErrorOnField(id,msg,highlight){
			
			if(id.jquery){
				id.parent().append('<div class="errMsgPerField">'+msg+'</div>');
				if(highlight == undefined || highlight == true){
					id.addClass('errHighlight');
				}		
				id.focus();
			}else{
				$('#'+id).parent().append('<div class="errMsgPerField">'+msg+'</div>');
				if(highlight == undefined || highlight == true){
					$('#'+id).addClass('errHighlight');
				}
				$('#'+id).focus();
			}
			return false;
		}

		//remove error on field
		function removeErrorOnField(id){
			
			if(id.jquery){
				id.parent().find('.errMsgPerField').remove();
				id.removeClass('errHighlight');
			}else{
				$('#'+id).parent().find('.errMsgPerField').remove();
				$('#'+id).removeClass('errHighlight');
			}
			return true;
		}
		
		function clearErrorMessages( id ){
			var serverMsg = id != null && id != undefined ? $("#" + id + " .errMsg") : $(".errMsg");
			var fields = id != null && id != undefined ? $("#" + id + " .errMsgPerField") :  $(".errMsgPerField");
			var highlights = id != null && id != undefined ? $("#" + id + " .errHighlight") :  $(".errHighlight");
			
			if(serverMsg != null && serverMsg.length > 0)
				serverMsg.remove();
			if(fields != null && fields.length > 0)
				fields.remove();
			if(highlights != null && highlights.length > 0)
				highlights.removeClass("errHighlight");
		}
		
		function doReset()
		{
			clearErrorMessages();

			clearForm("transaxnBean");
			$("#DATE_FROM").val('');
			$("#TIME_FROM").val('');
			$('#dispLimit').val(maxRow);
			$("#submitButton").focus();
		}
	
		
		function formatDate(month, day, year){
			var m = "";
			var d = "";
			
			if(month < 10)
				m = "0" + month;
			else
				m = month + "";

			if(day < 10)
				d = "0" + day;
			else
				d = day + "";

			return m + "-" + d + "-" + year;
		}
		
		function checkKey(e) {
		   /*  e = e || event;
		    if (!e) {return true;}
		    var code = e.keyCode || e.which || null;
		    if (code) {
		    	if(code == 8 || code == 46){
		    		$(e.target).val(defaultTime);
		    		$(e.target).focus();
		    		return false;
		    	}
		    } */
		    return true;
		}
	</script>
</head>
<body>
<div class="col-sm-12">
<noscript>
	<div class="errMsg">
		This transaction works best with JavaScript enabled. Please enable your JavaScript and reload the page to proceed with your transaction.
	</div>        
</noscript>
	<form:form method="post" modelAttribute="transaxnBean" name = "transactionBean" id = "transaxnBean">
		<h3 class="pageTitle">Code Red - Search Last <c:if test="${not empty fromCodeRedLoginAll}">[<c:out value="${databaseName}"/>]</c:if></h3>
		
		<div id="links">
			<div class="header_link">
				
			</div>
		</div>
		
		<%-- <center><spring:message text="${err.errorMessages}" /></center> --%>
		
		<div class="formTable">
			<c:if test ="${hasError == true}">
				<div id = "serverError" class = "errMsg" >
					<form:errors path = "serverError" id = "errMsg" />
				</div>
			</c:if>
			
			<fieldset>
				<legend class="fieldset_title">Filter Option<span class="pull-right">
				<a href="#" id="submitButton" tabindex="40">Search</a> | <a tabindex="50" href="#" id="clearButton">Clear</a>
				<c:if test="${not empty fromCodeRedLoginAll}">
					<a href="CodeRedLoginAll.htm?b=CodeRedSearchLast.htm" style="float:right">Change Database</a>
				</c:if>
				</span></legend>
				<table cellspacing="0" id="filter">
					<tr>
						<td class="tdLabel">Date From: </td>
						<td style="width:25% !important;">
							<%-- <form:input tabindex="10" path="DATE_FROM" id = "DATE_FROM" class = "datetime dateFromClass"
							dataType="numdate" fieldName="Date From" cssStyle="float:left;text-align:right;width:auto;" readonly='true'/>
							<span style = "float:left;position:absolute;"><input type='hidden' id='dateFromDP' style = "float:left;"/></span>
							<br/> --%>
							<div class="input-group date dateFrom">
							 	<form:input path="DATE_FROM" cssClass="datetime" id="DATE_FROM" mandatory="true" fieldName="Date From" 
							 		datatype="numdatetime" placeholder="YYYY-MM-DD" maxLength="10"/>
								<span class="input-group-btn">
									<button type="button" class="btn btn-default">
										<span class="glyphicon glyphicon-calendar"></span>
									</button>
								</span>
							</div>
							<form:errors path = "DATE_FROM" class = "errMsgPerField"/>
						</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td class="tdLabel">Time From: </td>
						<td>
							<form:input path = "TIME_FROM" id = "TIME_FROM" dataType = "time24sec" class = "amount datetime dateFromClass" maxLength = "2"
										cssStyle="float:left;width:auto;" />
							<br/>
							<form:errors path = "TIME_FROM" class = "errMsgPerField" style = "float:left"/>
						</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td class="tdLabel">Display Limit:</td>
						<td colspan="3">
							<input type="text" name="dispLimit" id="dispLimit" style="width:50px;float:left" value="<c:out value='${MAX_NUM_ROWS }'/>"/><br/>
							<form:errors path="TRAN_CODE" class="errMsgPerField"/>
						</td>
					</tr>
				</table>
				
				<div style = "text-align:center;width:99%" id = "fieldErrMsg"></div>
			</fieldset>
			
			<fieldset>
				<table id="teraTableId" width = "100%"  align="center" border = "1" cellspacing="0" class = "teraTable aaTable" >
					<thead>
						<tr>	
							<th id="date" style="width: 25%;">Date & Time</th>
							<th id="usrIdMasked" style="width: 12.5%;">User ID</th>
							<th id="transactionName" style="width: 25%;">Transaction Name</th>
							<th id="url" style="width: 12.5%;">Service / URL</th>							
							<th id="requestType" style="width: 12.5%;">Request Type</th>
							<th id="responseTime" style="width: 12.5%;">Response Time (ms)</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${not empty beanList }">
								<c:forEach var="bean" items="${beanList }">
									<tr>
										<td class="fmt_date"><c:out value="${bean.dateTime}"/></td>
										<td class="fmt_text"><c:out value="${bean.usrIdMasked}"/></td>
										<td class="fmt_text"><c:out value="${bean.transactionName}"/></td>
										<td class="fmt_text"><c:out value="${bean.url}"/></td>										
										<td class="fmt_decimal"><c:out value="${bean.requestType}"/></td>
										<td class="fmt_decimal"><c:out value="${bean.responseTime}"/></td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr><td colspan="7" align="center">No Records found</td></tr>
							</c:otherwise>
						</c:choose>
						
							
					</tbody>
				</table>
				<table width="100%" class="tblNumRec">
				<tbody>
				<tr>
					<td align="center" style="width:30%">&nbsp;</td>
					<td align="center" style="width:40%">
						<input type="button" class="submit" id="btnPrev" title="Previous Page" value="Previous" style="float:none; width:75px;">
						<input type="button" class="submit" id="btnNext" title="Next Page" value="Next" style="float:none; width:75px;">
					</td>
					<td align="center" style="width:30%">&nbsp;</td>
				</tr>
				</tbody>
				</table>
			</fieldset>
		</div>
		
		<input type = "hidden" name = "jsEnabled" id = "jsEnabled" value = ""/>
		<input type = "hidden" name = "tbAction" id = "tbAction"/>
		<form:hidden path="TIME_FROM_MS" id="TIME_FROM_MS"/>
	</form:form>
</div>
</body>
</html>