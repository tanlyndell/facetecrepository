<%@ page contentType="text/html; charset=iso-8859-1" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%-- <%@ taglib prefix="tera" uri="/tera"%> --%>
<html>
<head>
	<title>Code Red - Search</title>
	<jsp:include page="Include.jsp"></jsp:include>

	<style type="text/css">
		.mandatory{ 
		color:#FF0000; 
		float:left; 
		margin-left:5px; 
		text-align:left
		}
		
		input[type="text"], 
		input[type="password"],
		select  {
			width: 90%;	
		}

		input[type="button"], 
		input[type="submit"]  {
			max-width: 80px;	
		}
	</style>

<script type="text/javascript">
function doClearForm() {

	document.getElementById('usr_id').value='';
	document.getElementById('usr_password').value='';

	document.getElementById('dbType').value = document.getElementById('dbType').defaultValue;
	document.getElementById('hostName').value = document.getElementById('hostName').defaultValue;
	document.getElementById('port').value = document.getElementById('port').defaultValue;
	document.getElementById('dbName').value = document.getElementById('dbName').defaultValue;
	document.getElementById('username').value = document.getElementById('username').defaultValue;
	document.getElementById('password').value = document.getElementById('password').defaultValue;
}
</script>	

</head>
<body>

<div id="loginForm" style="width:50%;margin:0 auto">
<form:form method="post" commandName="transaxnBean" name = "transaxnBean" id = "transaxnBean">
	<fieldset>
		<legend>Code Red Login:</legend>
				<table align="center">
					<tr>
						<td class="label">User ID:</td>
						<td align="center"></td>
						<td>
							<input type="text" name="usr_id" id="usr_id"/><span class="mandatory">*</span>
						</td>
					</tr>
			
					<tr>
						<td class="label">Password:</td>
						<td align="center"></td>
						<td>
							<input type="password" name="usr_password" id="usr_password"/><span class="mandatory">*</span>
						</td>
					</tr>
			</table>
	</fieldset>

	<fieldset>
		<legend>Database Details:</legend>
			<table align="center">
				<tr>
					<td class="label">Database Type:</td>
					<td align="center"></td>
					<td>
						<form:select path ="dbType">dbTypeList
							<form:options items="${transaxnBean.dbTypeList}"/>
						</form:select>
						<span class="mandatory">*</span>
					</td>
				</tr>

			 	<tr>
					<td class="label">Host Name:</td>
					<td align="center"></td>
					<td>
						<form:input path ="hostName" type="text" name="hostName" id="hostName"/><span class="mandatory">*</span>
					</td>
				</tr>
		
				<tr>
					<td class="label">Port:</td>
					<td align="center"></td>
					<td>
						<form:input path ="port" type="text" name="port" id="port"/><span class="mandatory">*</span>
					</td>
				</tr>

				<tr>
					<td class="label">Database Name:</td>
					<td align="center"></td>
					<td>
						<form:input path ="dbName" type="text" name="dbName" id="dbName"/><span class="mandatory">*</span>
					</td>
				</tr>

				<tr>
					<td class="label">Username:</td>
					<td align="center"></td>
					<td>
						<form:input path ="username" type="text" name="username" id="username"/><span class="mandatory">*</span>
					</td>
				</tr>

				<tr>
					<td class="label">Password:</td>
					<td align="center"></td>
					<td>
						<form:input path ="password" type="password" name="password" id="password"/><span class="mandatory">*</span>
					</td>
				</tr>
				
				<tr>
					<td colspan="2"></td>
					<td align="right">
							<input class="btn" name="submit" id="connect" type="submit" value="Connect" value="Connect" >
 							<input name="clear" type="button" class="btn" value="Refresh" onClick="doClearForm();" />
					</td>
					
				</tr>
				<tr>
					<td colspan="3" id="errTD">
						<c:if test="${not empty errMessage }">
							<span style="color:red"><c:out value="${errMessage }"/></span>
						</c:if>
					</td>
				</tr>
			</table>
	</fieldset>
</form:form>

</div>

</body>
</html>
