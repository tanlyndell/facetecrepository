<%-- <%@ page isELIgnored ="false" %> --%>
<!-- <!doctype html> -->
<%@ page contentType="text/html; charset=iso-8859-1" pageEncoding="ISO-8859-1" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%-- <%@ taglib prefix="tera" uri="/tera"%> --%>
<html>
<head>
<!-- <meta http-equiv="content-type" content="text/html; charset=utf-8" /> -->
	<title>Code Red - Search</title>
	
	<link rel="stylesheet" href="resources/css/bootstrap.min.css">
	<link rel="stylesheet" href="resources/css/datetimepicker_css/bootstrap-datetimepicker2.css">
	<style>
		.tdLabel
		{
			text-align:right;
			width:120px;
		}
		
		/*Error --------------------------------------*/
		.errMsg{ 
			text-align:center; color:#FF0000; 
			background:#FFFFCC; 
			padding:10px 5px; 
			border-bottom:1px solid #996600; 
			clear:both
		}
		.errMsgPerField{ 
			font-style:oblique; font-size:0.9em; 
			color:#FF0000; 
			clear:both; 
		}
		.errHighlight{border:1px solid #FF0000}
		
		#tTableArea td:nth-child(5), td:nth-child(6), td:nth-child(7){
			text-align:right;
		}
		
		span.input-group-btn button{
			padding-top: 4px;
	    	padding-bottom: 4px;
	    }
	    
	    #filter td, th{
	    	padding:2px;
	    }
	</style>
	
	<!-- <script type="text/javascript" src="js/SpringWeb.js"></script>
	<script type="text/javascript" src="js/generic_front.js"></script> -->
	<script type="text/javascript" src="resources/js/jquery-1.12.1.min.js"></script>
	<script type="text/javascript" src="resources/js/moment-2.9.0.js"></script>
	<script type="text/javascript" src="resources/js/bootstrap-datetimepicker-4.17.37.js"></script>	
	<script type="text/javascript" src="resources/js/timepicker/jquery.timeentry.min.js"></script>
	<script type="text/javascript">
		var defaultValues;
		var defaultDate = "<c:out value = '${defaultDate}'/>";
		var defaultTime = "<c:out value = '${defaultTime}'/>";
		var maxRow = "<c:out value = '${MAX_NUM_ROWS}'/>";
		var currDate = new Date();
		var fromLast10 = false;

		
		$(document).ready( function(){
			dateFrom = '<c:out value="${transDF}"/>';
			dateTo = '<c:out value="${transDT}"/>';
			/* 
			$('#DATE_FROM').change(function(){
				$('#dateFromDP').val( $('#DATE_FROM').val().replace(/-/g,'\/') );
			});

			$( '#dateFromDP' ).change( function(){
				$('#DATE_FROM').val( $('#dateFromDP').val().replace(/\//g,'-') );
			}); */
			
			/* $('#DATE_TO').change(function(){
				$('#dateFromDP').val( $('#DATE_TO').val().replace(/-/g,'\/') );
			});

			$( '#dateToDP' ).change( function(){
				$('#DATE_TO').val( $('#dateToDP').val().replace(/\//g,'-') );
			}); */

			$( '.dateFrom' ).datetimepicker({format: 'YYYY-MM-DD',sideBySide:true, maxDate:  '${todayDate}'});
			$( '.dateTo' ).datetimepicker({format: 'YYYY-MM-DD',sideBySide:true, maxDate: '${todayDate}'});
			/* 
			var to = $( '#dateToDP' ).datepicker({
				altFormat: "yy-mm-dd",
				altField: '#DATE_TO',
		        buttonImage: 'resources/datepicker/Calendar.png',
		        buttonImageOnly: true,
		        changeMonth: true,
		        changeYear: true,
		        showOn: 'button',
		        maxDate:  'today'
			});
			
			$( '#dateFromDP' ).datepicker({
				altFormat: "yy-mm-dd",
				altField: '#DATE_FROM',
		        buttonImage: 'resources/datepicker/Calendar.png',
		        buttonImageOnly: true,
		        changeMonth: true,
		        changeYear: true,
		        showOn: 'button',
		        maxDate : 'today'
			}).on('change',function(){
				to.datepicker('option','minDate',new Date(this.value));
			});		 */	
			
			$( '#<c:out value="${historyType}"/>' ).attr('checked', 'checked');
			
			
			$("#search").click( function(){
				clearErrorMessages();
				
				if( checkMandatory("transaxnBean") ){
					if($.trim($('#TIME_FROM').val()) == '')
						$('#TIME_FROM').val('00:00:00');
					if($.trim($('#TIME_TO').val()) == '')
						$('#TIME_TO').val('23:59:59');
					
					if( !doSubmit() )
						return false;
					
					$("#jsEnabled").val(true);
					$('#action').val('search');
					
					
					document.forms['transaxnBean'].submit();
				}
				else{
					$('.errMsgPerField:contains("Mandatory")').remove();
					return false;
				}
			});

			$("#clearButton").click( function(){
				doReset();
			});
			

			$('#TIME_FROM').timeEntry({
				/*spinnerImage:'',*/
				ampmPrefix:' ',
				showSeconds: true,
				show24Hours: true
			});
			
			$('#TIME_TO').timeEntry({
				ampmPrefix:' ',
				showSeconds: true,
				show24Hours: true
			});
			
			$(document).keydown(function(e){
				var key = e.keyCode;
				
				if((key == '33' || key == '118') && $('#prevPage').attr('disabled') != 'disabled')
					$('#prevPage').click();
				
				if((key == '34' || key == '119') && $('#nextPage').attr('disabled') != 'disabled')
					$('#nextPage').click();
				
				if(key == '35' && $('#lastPage').attr('disabled') != 'disabled')
					$('#lastPage').click();
				
				if(key == '36' && $('#firstPage').attr('disabled') != 'disabled')
					$('#firstPage').click();
			
			});
			
			$('.submitButton').click(function(){
				$('#action').val($(this).attr('id'));
				$("#jsEnabled").val(true);
				$('#transaxnBean').submit();
			});
			$("#contents").show();
			
			$('#teraTableId tbody tr:odd').addClass('odd')
			$('#teraTableId tbody tr:even').addClass('even')

			if('<c:out value="${currPage}"/>' <= 1 ){
				$('#firstPage').attr('disabled',true);
				$('#prevPage').attr('disabled',true);
			}
			
			if('<c:out value="${currPage}"/>' == '<c:out value="${pages}"/>' ){
				$('#nextPage').attr('disabled',true);
				$('#lastPage').attr('disabled',true);
			}
			
			if('<c:out value="${pages}"/>' <= 1 ){
				$('#firstPage').attr('disabled',true);
				$('#prevPage').attr('disabled',true);
				$('#gotoValue').attr('disabled',true);
				$('#goto').attr('disabled',true);
				$('#nextPage').attr('disabled',true);
				$('#lastPage').attr('disabled',true);
			}
			
			$('.timeEntry_control').remove();
		});
		
		function doSubmitForm(formObject) {
			if (typeof formObject.onsubmit == "function") {
				if (!formObject.onsubmit())
					return;
			}
			formObject.submit();
		}
		
		function doSubmit()
		{
			var d1 = "Date From";
			var d2 = "Date To";
			var d1v = $.trim( $("#DATE_FROM").val() );
			var d2v = $.trim( $("#DATE_TO").val() );
			var h1 = $.trim( $("#TIME_FROM").val() );
			var h2 = $.trim( $("#TIME_TO").val() );
			
			if( d2v.length > 0 && compareDateTime(d1v, h1, d2v, h2) > 0)
			{
				var errMsg = "<div class = 'errMsgPerField' >" + 
							d2 + " must be greater than "+d1+"." 
							 "</div>";
				$(".datetime").addClass("errHighlight");
				
				$("#fieldErrMsg").append(errMsg);
				return false;
			}
		
			if(h1 == '' && h2 == ''){
				$('#teraTableShowLast').val('true');	
			}else
				$('#teraTableShowLast').val('false');
			return true;
		}
		
		function clearForm(formIdent) 
		{ 
		  var form, elements, i, elm; 
		  form = document.getElementById 
		    ? document.getElementById(formIdent) 
		    : document.forms[formIdent]; 

			if (document.getElementsByTagName)
			{
				elements = form.getElementsByTagName('input');
				for( i=0, elm; elm=elements.item(i++); )
				{
					if (elm.getAttribute('type') == "text")
					{
						elm.value = '';
					}
					
					if (elm.getAttribute('type') == "radio")
					{
						elm.checked = false;
					}
				}
				elements = form.getElementsByTagName('select');
				for( i=0, elm; elm=elements.item(i++); )
				{
						elm.value = '';
				}
			}

			
			else
			{
				elements = form.elements;
				for( i=0, elm; elm=elements[i++]; )
				{
					if (elm.type == "text")
					{
						elm.value ='';
					}
				}
			}
		}
		
		function checkMandatory(formName,checkInvalidChars){
			var prevLabel = '';
			var labels = '';
			var values = '';
			var noError = true;
			
			
			$('#'+formName+ ' [mandatory="true"]').each(function(index, value){
				
				if($(this).is(":visible")){
					removeErrorOnField($(this));				
					var mandatory = 'Mandatory';
			
					var type = $(this).prop('tagName'); 
					var ok = true;
					if(type == 'INPUT'){
						if(!($(this).is(':radio') || $(this).is(':checkbox'))){
							if($.trim($(this).val()) == ''){
								showErrorOnField($(this),mandatory);
								noError = false;
							}				
						}else if($(this).is(':radio') || $(this).is(':checkbox')){// for radio button
							var name = $(this).attr('name');
							if($('input[name="'+name+'"]:checked').length < 1){
								showErrorOnField($(this),mandatory);
								noError = false;
							}
						}
					}else if(type == 'SELECT'){//for select
						if($(this).find('option:selected').val()==''){
							showErrorOnField($(this),mandatory);
							noError = false;
						}
					}else if(type == 'TEXTAREA'){
						if($.trim($(this).val()) == ''){
							showErrorOnField($(this),mandatory);
							noError = false;
						}	
					}else /*if(type == 'TD')*/{
						var hasValue = true;
						
						$(this).find("input[exemptManda!='true'], select[exemptManda!='true']").each(function(index,value){
							
							//if(($(this).attr("exemptManda") != "true" || $(this).attr("exemptManda") != true)){
							if($(this).prop('tagName') == 'SELECT'){
								if($.trim($(this).find('option:selected').val())==''){
									hasValue = false;
								}
							}else if($(this).prop('tagName') == 'INPUT'){
								if(!($(this).is(':radio') || $(this).is(':checkbox')) && 
									($.trim($(this).val())=='') ){
									hasValue = false;
								}
								else if($(this).is(':radio') || $(this).is(':checkbox')){// for radio button and checkbox
									var name = $(this).attr('name');
									if($('input[name="'+name+'"]:checked').length < 1){
										hasValue = false;
									}
								}
							}
							
							if(!hasValue){
								$(this).addClass("errHighlight");
								$(this).focus();
							}
							//}
						});
						if(!hasValue){
							//showErrorOnField($(this).find("input[exemptManda!='true'], select[exemptManda!='true']").last(),mandatory,true);
							//$(this).find("[exemptManda!='true']").addClass('errHighlight');
							$(this).find("input[exemptManda!='true'], select[exemptManda!='true']").last().parent().append('<div class="errMsgPerField">'+mandatory+'</div>');
							noError = false;
						}
					}
				}
			});
			
			return noError;
		}
		
		function showErrorOnField(id,msg,highlight){
			
			if(id.jquery){
				id.parent().append('<div class="errMsgPerField">'+msg+'</div>');
				if(highlight == undefined || highlight == true){
					id.addClass('errHighlight');
				}		
				id.focus();
			}else{
				$('#'+id).parent().append('<div class="errMsgPerField">'+msg+'</div>');
				if(highlight == undefined || highlight == true){
					$('#'+id).addClass('errHighlight');
				}
				$('#'+id).focus();
			}
			return false;
		}

		//remove error on field
		function removeErrorOnField(id){
			
			if(id.jquery){
				id.parent().find('.errMsgPerField').remove();
				id.removeClass('errHighlight');
			}else{
				$('#'+id).parent().find('.errMsgPerField').remove();
				$('#'+id).removeClass('errHighlight');
			}
			return true;
		}
		
		function clearErrorMessages( id ){
			var serverMsg = id != null && id != undefined ? $("#" + id + " .errMsg") : $(".errMsg");
			var fields = id != null && id != undefined ? $("#" + id + " .errMsgPerField") :  $(".errMsgPerField");
			var highlights = id != null && id != undefined ? $("#" + id + " .errHighlight") :  $(".errHighlight");
			
			if(serverMsg != null && serverMsg.length > 0)
				serverMsg.remove();
			if(fields != null && fields.length > 0)
				fields.remove();
			if(highlights != null && highlights.length > 0)
				highlights.removeClass("errHighlight");
		}
		function set24HourTime( sDate, sTime ){
			if(sTime.indexOf(" ") > -1){
				var arr = sTime.substring(0, sTime.indexOf(" ")).split(":");
				
				if( sTime.toUpperCase().indexOf("P") > -1 ){
					sDate.setHours(Number(arr[0]) + 12);
				}
				else{
					sDate.setHours(Number(arr[0]));
				}
				
				sDate.setMinutes(Number(arr[1]));
				
				if(arr.length > 2)
					sDate.setSeconds(Number(arr[2]));
			}
			else{
				var arr = sTime.split(":");
				sDate.setHours(Number(arr[0]));
				sDate.setMinutes(Number(arr[1]));
				
				if(arr.length > 2)
					sDate.setSeconds(Number(arr[2]));
			}
			
			return sDate;
		}
		function compareDateTime( sdate1, sTime1, sdate2, sTime2, dateFormat ){
			var dateArr1 = sdate1.split("-");
			var dateArr2 = sdate2.split("-");
			//mm-dd-yy
			//yy-mm-dd
			var date2 = new Date();
			date2.setFullYear(parseInt(dateArr2[0],10), parseInt(dateArr2[1],10) > 0 ? parseInt(dateArr2[1],10) - 1 : parseInt(dateArr2[1],10), parseInt(dateArr2[2],10));
			
			if(sTime2 != '')
				date2 = set24HourTime(date2, sTime2);
			
			var date1 = new Date();
			date1.setFullYear(parseInt(dateArr1[0],10), parseInt(dateArr1[1],10) > 0 ? parseInt(dateArr1[1],10) - 1 : parseInt(dateArr1[1],10), parseInt(dateArr1[2],10));
			
			if(sTime1 != '')
				date1 = set24HourTime(date1, sTime1);
			
			if(date1 > date2)
				return 1;
			else if(date1 < date2)
				return -1;
			else if(date1 == date2)
				return 0;
		}
		function doReset()
		{
			clearErrorMessages();

			clearForm("transaxnBean");
			setCurrentDate();
			$("#DATE_FROM").val(dateFrom);
			$("#DATE_TO").val(dateTo);
			$("#TIME_FROM").val('');
			$("#TIME_TO").val('');
			$('#dispLimit').val(maxRow);
			//$("#TIME_TO").focus();
			//$("#TIME_FROM").focus();
			$("#USR_ID").focus();
			$('#tTableArea').remove();
		}
	
		function setCurrentDate(){
			if((dateFrom == null || dateFrom == "")/* && fromLast10*/)
				dateFrom = formatDate(currDate.getMonth()+1, currDate.getDate(), currDate.getFullYear());
			if((dateTo == null || dateTo == "")/* && fromLast10*/)
				dateTo = formatDate(currDate.getMonth()+1, currDate.getDate(), currDate.getFullYear());

			transaxnBean.DATE_FROM.value = dateFrom;
			transaxnBean.DATE_TO.value = dateTo;
		}

		function formatDate(month, day, year){
			var m = "";
			var d = "";
			
			if(month < 10)
				m = "0" + month;
			else
				m = month + "";

			if(day < 10)
				d = "0" + day;
			else
				d = day + "";

			return m + "-" + d + "-" + year;
		}
		
		function checkKey(e) {
		   /*  e = e || event;
		    if (!e) {return true;}
		    var code = e.keyCode || e.which || null;
		    if (code) {
		    	if(code == 8 || code == 46){
		    		$(e.target).val(defaultTime);
		    		$(e.target).focus();
		    		return false;
		    	}
		    } */
		    return true;
		}
	</script>
</head>
<body>
<div class="col-sm-12">
<noscript>
	<div class="errMsg">
		This transaction works best with JavaScript enabled. Please enable your JavaScript and reload the page to proceed with your transaction.
	</div>        
</noscript>
	<form:form method="post" modelAttribute="transaxnBean" name = "transactionBean" id = "transaxnBean">
		<h3>Code Red - Search <c:if test="${not empty fromCodeRedLoginAll}">[<c:out value="${databaseName}"/>]</c:if></h3>
		
		<div id="links">
			<div class="header_link">
				
			</div>
		</div>
		
		<%-- <center><spring:message text="${err.errorMessages}" /></center> --%>
		
		<div class="formTable">
			<c:if test ="${hasError == true}">
				<div id = "serverError" class = "errMsg" >
					<form:errors path = "serverError" id = "errMsg" />
				</div>
			</c:if>
			
			<fieldset>
				<legend class="fieldset_title">Filter Options <span class="pull-right">
				<a href="#" id="search" tabindex="40">Search</a> | <a tabindex="50" href="#" id="clearButton">Clear</a>
				<c:if test="${not empty fromCodeRedLoginAll}">
					<a href="CodeRedLoginAll.htm?b=CodeRedSearch.htm" style="float:right">Change Database</a>
				</c:if>
				</span></legend>
				<table cellspacing="0" id="filter">
					<tr>
						<td class="tdLabel">Transaction Code: </td>
						<td colspan="2">
							<form:input path="TRAN_CODE" id="TRAN_CODE" cssStyle="float:left;text-align:right;width:auto;" dataType="userid" maxLength="80" minLength="6"/><br/>
							<form:errors path="TRAN_CODE" class="errMsgPerField"/>
						</td>
						
						<%-- <td class="tdLabel">User ID: </td>
						<td colspan="2">
							<form:input path="USR_ID" id="USR_ID" cssStyle="float:left;text-align:right;width:auto;" dataType="userid" maxLength="80" minLength="6"/><br/>
							<form:errors path="USR_ID" class="errMsgPerField"/>
						</td> --%>
						<%-- <td class="tdLabel">Response Time:</td>
						<td>
							<form:input path = "responseTimeFrom" id = "responseTimeFrom" cssStyle="width:50px;" dataType = "numeric"/>-
							<form:input path = "responseTimeTo" id = "responseTimeTo" cssStyle="width:50px;" dataType = "numeric"/>
							<form:errors path="responseTimeFrom" class="errMsgPerField"/>
						</td> --%>
						<td style="width:15% !important;" class="tdLabel">Transacion Name: </td>
						<td style="width:18% !important;">
							<form:input path = "TRAN_NAME" id = "TRAN_NAME" />
							<form:errors path="TRAN_NAME" class="errMsgPerField"/>							
						</td>
						<td>&nbsp;</td>	
					</tr>
					<tr>
						<td style="width:15% !important;" class="tdLabel">Date From: </td>
						<td style="width:18% !important;vertical-align: top;">
							<div class="input-group date dateFrom">
							 	<form:input path="DATE_FROM" cssClass="datetime" id="DATE_FROM" mandatory="true" fieldName="Date From" 
							 		datatype="numdatetime" placeholder="YYYY-MM-DD" maxLength="10"/>
								<span class="input-group-btn">
									<button type="button" class="btn btn-default">
										<span class="glyphicon glyphicon-calendar"></span>
									</button>
								</span><span style="float:right;padding-right:8px">-</span>
							</div>
							<%-- <form:input tabindex="10" path="DATE_FROM" id = "DATE_FROM" mandatory = "true" class = "datetime dateFromClass"
							dataType="numdate" fieldName="Date From" cssStyle="text-align:right;width:auto;" readonly='true'/>
							<span style = "float:left;position:absolute;"><input type='hidden' id='dateFromDP' style = "float:left;"/></span><span style="float:right;padding-right:8px">-</span> --%>
							
							<form:errors path = "DATE_FROM" class = "errMsgPerField"/>
						</td>
						<td style="width:17% !important;vertical-align: top;">
							<div class="input-group date dateTo">
							 	<form:input path="DATE_TO" cssClass="datetime" id="DATE_TO" mandatory="true" fieldName="Date To" 
							 		datatype="numdatetime" placeholder="YYYY-MM-DD" maxLength="10"/>
								<span class="input-group-btn">
									<button type="button" class="btn btn-default">
										<span class="glyphicon glyphicon-calendar"></span>
									</button>
								</span>
							</div>
							<%-- <form:input tabindex="20" path="DATE_TO" id = "DATE_TO" class = "datetime dateToClass" mandatory = "true"
							dataType="numdate" fieldName="Date To" cssStyle="text-align:right;width:auto;" readonly='true'/>
							<span style = "float:left;position:absolute;"><input type='hidden' id='dateToDP' style = "float:left"/></span> --%>
								
							<form:errors path = "DATE_TO" class = "errMsgPerField"/>
						</td>
						<%-- <td class="tdLabel">Transaction Code: </td>
						<td colspan="2">
							<form:input path="TRAN_CODE" id="TRAN_CODE" cssStyle="float:left;text-align:right;width:auto;" dataType="userid" maxLength="80" minLength="6"/><br/>
							<form:errors path="TRAN_CODE" class="errMsgPerField"/>
						</td> --%>
						<%-- <td style="width:15% !important;" class="tdLabel">Response Time MF 1: </td>
						<td style="width:18% !important;">
							<form:input path = "mfResponseTimeFrom" id = "mfResponseTimeFrom" cssStyle="width:50px;" dataType = "numeric"/>-
							<form:input path = "mfResponseTimeTo" id = "mfResponseTimeTo" cssStyle="width:50px;" dataType = "numeric"/>
							<form:errors path="responseTimeFrom" class="errMsgPerField"/>							
						</td> --%>
						<td style="width:15% !important;" class="tdLabel">Service / URL: </td>
						<td style="width:18% !important;">
							<form:input path = "URL" id = "URL" />
							<form:errors path="URL" class="errMsgPerField"/>							
						</td>
						<td style="width:17% !important;">&nbsp;</td>
					</tr>
					
					<tr>
						<td class="tdLabel">Time From: </td>
						<td style="vertical-align: top;">
							<form:input path = "TIME_FROM" id = "TIME_FROM" cssStyle="float:left;width:auto;" dataType = "time24sec" class = "amount datetime dateFromClass"/><span style="float:right;padding-right:8px">-</span>
							<!-- <br/> -->
							<form:errors path = "TIME_FROM" class = "errMsgPerField" style = "float:left"/>
						</td>
						<td style="vertical-align: top;">
							<form:input path = "TIME_TO" id = "TIME_TO" cssStyle="float:left;width:auto;" dataType = "time24sec" class = "amount datetime dateToClass"/>
							<!-- <br/> -->
							<form:errors path = "TIME_TO" class = "errMsgPerField" style = "float:left"/>
						</td>
						<%-- <td class="tdLabel">IP Address: </td>
						<td colspan="2">
							<form:input path="IP_ADDRESS" id="IP_ADDRESS" cssStyle="float:left;text-align:right;width:auto;" dataType="valid" maxLength="80" minLength="6"/><br/>
							<form:errors path="IP_ADDRESS" class="errMsgPerField"/>
						</td> --%>
						<%-- <td class="tdLabel">Response Time MF 2: </td>
						<td>
							<form:input path = "mfResponseTimeFrom2" id = "mfResponseTimeFrom2" cssStyle="width:50px;" dataType = "numeric"/>-
							<form:input path = "mfResponseTimeTo2" id = "mfResponseTimeTo2" cssStyle="width:50px;" dataType = "numeric"/>
							<form:errors path="responseTimeFrom" class="errMsgPerField"/>							
						</td> --%>
						<td class="tdLabel">Response Time:</td>
						<td>
							<form:input path = "responseTimeFrom" id = "responseTimeFrom" cssStyle="width:50px;" dataType = "numeric"/>-
							<form:input path = "responseTimeTo" id = "responseTimeTo" cssStyle="width:50px;" dataType = "numeric"/>
							<form:errors path="responseTimeFrom" class="errMsgPerField"/>
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
										
						<td style="width:15% !important;" class="tdLabel">Display Limit: </td>
						<td style="width:18% !important;">
							<input type="text" name="dispLimit" id="dispLimit" style="width:50px;float:left" value="<c:out value='${MAX_NUM_ROWS }'/>"/><br/>
							<form:errors path="TRAN_CODE" class="errMsgPerField"/>
						</td>
						<td colspan="4">&nbsp;</td>	
					</tr>
				</table>
				
				<div style = "text-align:center;width:99%" id = "fieldErrMsg"></div>
			</fieldset>
			<c:if test="${empty fromGet}">
			<fieldset id="tTableArea">
				
				<table id="teraTableId" width = "100%"  align="center" border = "1" cellspacing="0" class = "teraTable aaTable" >
					<thead>
						<tr>	
							<th id="date" style="width: 25%;">Date & Time</th>
							<th id="usrIdMasked" style="width: 12.5%;">User ID</th>
							<th id="transactionName" style="width: 25%;">Transaction Name</th>
							<th id="url" style="width: 12.5%;">Service / URL</th>							
							<th id="requestType" style="width: 12.5%;">Request Type</th>
							<th id="responseTime" style="width: 12.5%;">Response Time (ms)</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${not empty searchList }">
								<c:forEach var="bean" items="${searchList }">
									<tr>
										<td class="fmt_date"><c:out value="${bean.dateTime}"/></td>
										<td class="fmt_text"><c:out value="${bean.usrIdMasked}"/></td>
										<td class="fmt_text"><c:out value="${bean.transactionName}"/></td>
										<td class="fmt_text"><c:out value="${bean.url}"/></td>										
										<td class="fmt_decimal"><c:out value="${bean.requestType}"/></td>
										<td class="fmt_decimal"><c:out value="${bean.responseTime}"/></td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr><td colspan="7" align="center">No Records found</td></tr>
							</c:otherwise>
						</c:choose>
						
							
					</tbody>
				</table>
				<table width = "100%" class="tblNumRec">
					<tbody>
						<tr>
							<td align = "center" style = 'width:30%'>No. of Records: <b id="numrecords"><c:out value="${totalRecordNum }"/></b> </td>
							<td align = "center" style = 'width:40%'>
								<input type="hidden" name="pagenum" id="pagenum" value=""/>
								<input type = "button" id="firstPage" class="submitButton" title="First Page" value = "&laquo;" style = "float:none" />
								<input type = "button" id="prevPage" class="submitButton"  title="Previous Page" value = "&lsaquo;" style = "float:none" />
								<input type = "text" style = "width:50px; float:none;text-align:right;" name="gotoValue" id="gotoValue" onkeypress = "return keyRestrict(event, '0123456789')" style = "float:none" >
								<input type = "button" id="goto" class="submitButton" name = "buttongoto" value = "Go To Page" style = "width:100px; float:none; " style = "float:none" >
								<input type = "button" id="nextPage" class="submitButton" title="Next Page" value = "&rsaquo;"  style = "float:none" />
								<input type = "button" id="lastPage" class="submitButton" title="Last Page" value = "&raquo;"  style = "float:none" />
							</td>
							<td align = "center" style = 'width:30%'>Page <c:out value="${currPage }"/> of <c:out value="${pages }"/></td>
						</tr>	
				</table>
			</fieldset>
			</c:if>
		</div>
		
		<input type = "hidden" name = "jsEnabled" id = "jsEnabled" value = ""/>
		<input type = "hidden" name = "teraTableShowLast" id = "teraTableShowLast" value = "true"/>
		<input type="hidden" name="action" id="action"/>
	</form:form>
	</div>
</body>
</html>