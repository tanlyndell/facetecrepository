/*NLM20170126 filterTable plugin*/
/*
	This function is attached on to a textfield: 
ex: $("#tableFilter").filterTable("teraTableId", "3");
<div class="section-header">
	<h6>...</h6>
	<div class="input-group">
		<input type="text" class="form-control" id="tableFilter">
	</div>
</div>
<div class="section-content table-responsive">
	<table class="table table-condensed table-striped table-hover">
		<thead>...</thead>
		<tbody>...</tbody>
	</table>
</div>

	This function has two parameters table name and table columns.
table name is the name of the table that you want to filter 
table columns can be indicated as ex: "all"/"ALL" if you want all columns to
be checked if it contains the filter keyword entered or specify the columns
you want to filter ex: "1, 2"/"1, 3, 5", the column numbers should 
be separated by a comma

	The function triggers on key up meaning as you type it filters the table 
automatically. it hides the row in the table that doesnt contain the 
filter keyword entered. Take note that it only filters visible table rows
meaning it only filters row on the current page of the table. If no results are 
found, "Keyword not found" row will appear.
*/

(function($) {
	$.fn.filterTable = function(tableName, tableColumns, dropdownColumn) {
		var numOfVisibleRows;
		var dropdownValue = "All";
		
		if(!(dropdownColumn.toUpperCase() === "NONE")) {			
			var arr = [];
			var options = [];
			$("#" + tableName + " tbody tr td:nth-child(" + dropdownColumn + ")").each(function() {
				if ($.inArray($(this).text(), arr) == -1) {
		            arr.push($(this).text());
					options.push('<li class="list-group-item" data-value=' + $(this).text() + '>' + $(this).text() + '</li>');
				}
			});

			$(this).before( '<div class="input-group-addon input-group-addon-c">' +
								'<div class="select" class="form-control">' +
									'<input id="dropdown-filter" class="form-control" type="hidden">' +
									'<input id="dropdown-filter_view" class="form-control" type="text">' +
									'<i class="glyphicon glyphicon-menu-down"></i>' + 
									'<ul class="list-group">' +
										'<li class="list-group-item" style="display:none" data-value>Select</li>' +
										'<li class="list-group-item" data-value="All">All</li>' +
										options.join('') +
									'</ul>' + 
								'</div>' +
							'</div>');
			$('#dropdown-filter').val('All');
			$('#dropdown-filter_view').val('All');
		}
		
		$('#dropdown-filter').change(function(e){
			dropdownValue = $(this).val();
			$("#" + tableName + " tbody tr td:nth-child(" + dropdownColumn + ")").each(function() {
				if(dropdownValue === $(this).text() || dropdownValue === "All") {
					$(this).closest("tr").css("display", "");
					$(this).closest("tr").attr("filter", dropdownValue);
					if(dropdownValue === "All") 
						$(this).closest("tr").removeAttr("filter");
				} else {
					$(this).closest("tr").css("display", "none");
					$(this).closest("tr").removeAttr("filter");
				}
			});
			$("#tableFilter").val("");
			
			numOfVisibleRows = $("#" + tableName + " tbody tr:visible").length;
			$("#numrecords").text(numOfVisibleRows);
		});
		
		var columns = [];
		var numCol = $("#" + tableName).find('tr')[0].cells.length;
		if (tableColumns.toUpperCase() === "ALL") {
			for(var i = 1; i <= numCol; i++) {
				columns.push(i);
			}
		} else {
			columns = tableColumns.split(',').map(Number);
		}
		
		$(this).attr('placeholder', 'Keyword Search');
		$(this).after('<div class="input-group-addon" id="basic-addon1">' +
				'<span class="glyphicon glyphicon-search" aria-hidden="true" style="font-family: Glyphicons Halflings"></span>' +
				'</div>');
		
		$(this).keypress(function(e) {
			if(e.keyCode == 13) {
				e.preventDefault();
			}
		});
		
		$(this).keyup(function() {
			var keyword = $(this).val().toUpperCase().trim();
			
			var found = false;
			$( ".warning" ).remove();
			
			$("#" + tableName + " tbody tr").each(function() {
				if(dropdownValue === "All" || $(this).attr("filter") === dropdownValue) {
				    $(this).find('td').each(function(index, value) {
				        if($.inArray(index + 1, columns) > -1) {
				        	if($(this).text().toUpperCase().trim().indexOf(keyword) > -1) {
				        		$(this).closest("tr").css("display", "");
				        		found = true;
				        		return false;
				        	} else {
				        		$(this).closest("tr").css("display", "none");
				        	}
				        }
				    })
				}
			})
			
			if(!found) {
				$("#" + tableName + " tbody").after(
					"<tr class='warning'>" +
						"<td align='center' colspan='" + numCol + "' style='padding: 5px'>" +
							"Keyword not found" +
						"</td>" +
					"</tr>"
				);
			}
			
			numOfVisibleRows = $("#" + tableName + " tbody tr:visible").length;
			$("#numrecords").text(numOfVisibleRows);
		});
		
		
		
		return this;
	};

	/**START: LGT 20170223: Added function to refresh table results**/
	$.fn.refreshTable = function(tableName, tableColumns){
		var columns = [];
		var numCol = $("#" + tableName).find('tr')[0].cells.length;
		if (tableColumns.toUpperCase() === "ALL") {
			for(var i = 1; i <= numCol; i++) {
				columns.push(i);
			}
		} else {
			columns = tableColumns.split(',').map(Number);
		}		
		
		$( ".warning" ).remove();
		
		$("#" + tableName + " tbody tr").each(function() {
		    $(this).find('td').each(function(index, value) {
		        if($.inArray(index + 1, columns) > -1) {
	        		$(this).closest("tr").css("display", "");
	        		return false;
		        }
		    })
		})
	};
	/**END: LGT 20170223: Added function to refresh table results**/
	
}(jQuery));