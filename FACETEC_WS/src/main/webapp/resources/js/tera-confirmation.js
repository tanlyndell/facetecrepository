/**
 * tera-confirmation.js
 * Created by: Louie John E. Leona
 * Date Created: 20140224
 */
function replaceEscapeChar(str){
	return str.replace(/\\/g, "&#92").replace(/\'/g, "&#39").replace(/\"/g, "&#34")
}
 //formats label and value to JSON format
function formatJSON(label, value){
	value = replaceEscapeChar(value)
	return '{^'+label+'^:^'+value+'^}';
}

//get field value from parameter map
function getFieldValue(params){
	var formIns = params['formIns'];
	var fieldIns = params['fieldIns'];
	var fieldValue = undefined;
	
	if(formIns != undefined && fieldIns != undefined){
		var fieldType = fieldIns.prop('tagName');
		var searchOldValue = params['searchOldValue'];
		var fieldValue2 = undefined;
		if(fieldType == 'INPUT'){//for text field	
			if(!(fieldIns.is(':radio') || fieldIns.is(':checkbox'))){
				fieldIns.val($.trim(fieldIns.val()));
				fieldValue = (searchOldValue) ? fieldIns.attr('oldValue') : (fieldIns.attr("confValue") != null ? $.trim(fieldIns.attr("confValue")) : $.trim(fieldIns.val()));
				fieldValue2 = fieldValue; 
			}else if(fieldIns.is(':radio') && fieldIns.is(":checked")){// for radio button
				 var radioText = $.trim(fieldIns.parent().text());
				 if(fieldIns.attr('altField') != undefined ){
					 radioText = $.trim($('#'+fieldIns.attr('altField')).val());
				 }
				 fieldValue = (searchOldValue) ? fieldIns.attr('oldValue') : (fieldIns.attr("confValue") != null ? $.trim(fieldIns.attr("confValue")) : radioText);
				 fieldValue2 = fieldIns.is(':checked');
			}else if(fieldIns.is(':checkbox')){//for checkbox
				var getVal = true;
				
				if(fieldIns.attr('checklist')=='true' && !fieldIns.is(':checked'))
					getVal = false;
				
				if(getVal){
					var checkBoxVal = (searchOldValue) ? fieldIns.attr('oldValue') : (fieldIns.attr("confValue") != null ? $.trim(fieldIns.attr("confValue")) :$.trim(fieldIns.parent().text()));
					if(checkBoxVal == fieldIns.attr('fieldName')){
						checkBoxVal = fieldIns.is(':checked') ? 'Yes' : 'No'	 
					}
					fieldValue = checkBoxVal;
					fieldValue2 = fieldIns.is(':checked');
				}
			}
		}else if(fieldType == 'SELECT'){//for select			
			var value = (searchOldValue) ? fieldIns.attr('oldValue') : $.trim(fieldIns.find('option:selected').attr('confValue'));			
			if(value == undefined || value == '')
				value = $.trim(fieldIns.find('option:selected').text());
				
			fieldValue = value;
			fieldValue2 = fieldIns.val();
		}else if(fieldType == 'TEXTAREA'){
			fieldIns.val($.trim(fieldIns.val()));
			fieldValue = (searchOldValue) ? fieldIns.attr('oldValue') : $.trim(fieldIns.val());
			fieldValue2 = fieldValue;
		}else if(fieldType == 'LABEL'){
			fieldValue = (searchOldValue) ? fieldIns.attr('oldValue') : $.trim(fieldIns.text());
			fieldValue2 = fieldValue;
		}else if(fieldType == 'TR'){
			var tdIndex = [];
			var value = '';
			if(fieldIns.attr('tdIndex') != undefined){
				tdIndex = fieldIns.attr('tdIndex').split(",");
			}else{
				fieldIns.find('td').each(function(index,value){
					tdIndex[index] = index;
				});
			}
			
			for(var i = 0; i < tdIndex.length ; i ++){
				if((fieldIns.find(':checkbox').length > 0 && fieldIns.find(':checkbox').is(':checked')) || 
						(fieldIns.find(':radio').length > 0 && fieldIns.find(':radio').is(':checked')) ||
						(fieldIns.find(':checkbox').length < 1 && fieldIns.find(':radio').length < 1)){
					value += ' ' + fieldIns.find('td:eq('+tdIndex[i]+')').text();
				}else
					value = undefined;
			}
			
			fieldValue = value;
			
		}else{
			
			if(fieldIns.children().length < 1){
				fieldValue = fieldIns.text();
			}else{
				var childValue = '';
				var defValue = '';
				var validValue = '';
				fieldIns.children().each(function(index,value){


					if($(this).prop('tagName') == 'SELECT'){
						hasChild = true;
			
						var value = (searchOldValue) ? $(this).attr('oldValue') : $(this).find('option:selected').attr('confValue') ? $(this).find('option:selected').attr('confValue') : $(this).find('option:selected').text();
						if(value != undefined){
							if($(this).hasClass('default'))
								defValue += value;
							else
								validValue += value;
						}
					}else if($(this).prop('tagName') == 'INPUT'){
		
						var value = (searchOldValue) ? $(this).attr('oldValue') : $(this).attr('confValue') ? $.trim($(this).attr('confValue')) : $.trim($(this).val());
						if(value != undefined){
							if($(this).hasClass('default'))
								defValue += value;
							else
								validValue += value;
						}
					}else{
						
					}
				});
				
				if($.trim(validValue) == '')
					fieldValue = undefined;
				else
					fieldValue = defValue+validValue;
			}
		}
		
		if(fieldIns.attr('fieldNameAddon') != undefined){
			fieldValue = fieldValue + " " + fieldIns.attr('fieldNameAddon');
		}
		
		var oldVal = fieldIns.attr('oldValue') != undefined ? fieldIns.attr('oldValue') : '';
		var keyField = params['keyField'] != undefined ? params['keyField'] : '';

		if(params['checkNoChange'] == true && keyField != '' && keyField == fieldIns.attr('id'))
			return fieldValue;
		else if(params['checkNoChange'] == true && (''+fieldValue2 ) == oldVal)
			return '_NOCHANGE';
		else if(params['checkNoChange'] == true && fieldValue2 != oldVal && $.trim(fieldValue) == '')
			return oldVal + ' (Removed)';
		else
			return fieldValue;
	}else{
		return null;
	}
}

//format confirmation label value parameters to a format ready for logging 
function formatConfLabelValue(params){
	
	var formIns = params['formIns'];
	
	if(formIns != undefined && formIns != ''){
		var searchHidden = params['hasHidden'];
		var oldValueLen = formIns.find('[oldValue]').length;
		
		var label;
				
		var jsonConfValArr = new Array();
		var jsonConfNewValArr = new Array();
		var jsonConfOldValArr = new Array();
		
		var params2 = new Object();
		
		if(params['checkNoChange'] != undefined)
			params2['checkNoChange'] = params['checkNoChange'];

		if(params['keyField'] != undefined)
			params2['keyField'] = params['keyField'];
		
		if(formIns.find('#confLabels').length > 0 && formIns.find('#confLabels').val().length > 0){
			
			var confLabels = formIns.find('#confLabels').val();
			confLabels = confLabels.replace(/[\^]/g,"\"");
			var confLabelsObj = eval ("(" +confLabels+")" );
			$.each(confLabelsObj, function(key1, val1){
				$.each(val1, function(index, val){
					var label2 = "";
					var value2 = "";
					
					$.each(val,function (key, value){
						label2 = key;
						value2 = value;
					});
					if(key1 == 'confVal')
						jsonConfValArr[jsonConfValArr.length] = formatJSON(label2, value2);
					else if(key1 == 'confOld')
						jsonConfOldValArr[jsonConfOldValArr.length] = formatJSON(label2, value2);
					else if(key1 == 'confNew')
						jsonConfNewValArr[jsonConfNewValArr.length] = formatJSON(label2, value2);
				});
			});
		}
		
		//iterate through all element with "fieldName" attribute
		formIns.find('[fieldName]').each(function(index, value){
			
			//check if element is visible
			if($(this).is(":visible") || (searchHidden != undefined && searchHidden)){
				
				//get fieldName
				label = $(this).attr('fieldName');
				
				params2['formIns'] =formIns;
				params2['fieldIns'] = $(this); 
				
				var arrIndex = jsonConfValArr.length;
				var confSeq = $(this).attr('confSeq');
				
				if(confSeq != undefined)
					arrIndex = confSeq;
				
				var newVal = getFieldValue(params2);
				if(typeof(newVal)!=='undefined')
					newVal = newVal.replace(/\n/g,'<br>');

				var OTbl = $(this).attr('OTbl');
				var NTbl = $(this).attr('NTbl');
				
				if(newVal != undefined){
					if(oldValueLen > 0 && params['searchOldValue'] == true){
						var oldValueAttr = $(this).attr('oldValue');
						var compareValAttr = $(this).attr('compareVal');
						
						params2['searchOldValue'] = true;
						var oldVal = getFieldValue(params2);
						if(typeof(oldVal)!=='undefined')
							oldVal = oldVal.replace(/\n/g,'<br>');
						
						if(oldValueAttr != undefined){
							if(compareValAttr == undefined
											|| (compareValAttr != undefined && newVal != '' && newVal != oldVal)){
								jsonConfNewValArr[jsonConfNewValArr.length] = formatJSON(label, newVal);
								jsonConfOldValArr[jsonConfOldValArr.length] = formatJSON(label, oldVal);
							}
						}else{
							jsonConfValArr[arrIndex] = formatJSON(label, newVal);
						}
						
					}else if(OTbl != undefined){
						jsonConfOldValArr[jsonConfOldValArr.length] = formatJSON(label, newVal);
					}else if(NTbl != undefined){
						jsonConfNewValArr[jsonConfNewValArr.length] = formatJSON(label, newVal);
					}else{
						/**
						 * 20170809LEL: Update - Optional feature to not display values that does not changed
						 * "checkNoChange" must be on parameter
						 * fields must have "oldValue" attribute containing old values to compare with new value
						 * **/
						if(params['checkNoChange'] == undefined || (params['checkNoChange'] == true && newVal != '_NOCHANGE'))
							jsonConfValArr[arrIndex] = formatJSON(label, newVal);
					}
				}
					
				
							
				params2['searchOldValue'] = false;
			}
		});
		
		function formatFromArr(array){
			var str = '';
			for(var v=0; v < array.length; v++){
				str += array[v];
				if((v + 1)< array.length){
					str += ',';
				}
			}
			
			return str;
		}
		
		var confDetails;
		
		confDetails = '{^confVal^:['+ formatFromArr(jsonConfValArr) +']';
		
		
		if(jsonConfNewValArr.length > 0){
			confDetails += ',^confNew^:['+ formatFromArr(jsonConfNewValArr)+']';
		}
		
		if(jsonConfOldValArr.length > 0){
			confDetails += ',^confOld^:['+ formatFromArr(jsonConfOldValArr)+']';
		}
		
		var oldLblDef = true;
		var newLblDef = true;
		if(params['OLHdr'] != undefined){
			confDetails += ',^OLHdr^:^'+params['OLHdr']+'^';
			oldLblDef = false;
		}	
			
		if(params['ORHdr'] != undefined){
			confDetails += ',^ORHdr^:^'+params['ORHdr']+'^';
			oldLblDef = false;
		}	
		
		if(params['NLHdr'] != undefined){
			confDetails += ',^NLHdr^:^'+params['NLHdr']+'^';
			newLblDef = false;
		}	
		if(params['NRHdr'] != undefined){
			confDetails += ',^NRHdr^:^'+params['NRHdr']+'^';
			newLblDef = false;
		}	

		if(params['OLbl'] != undefined)
			confDetails += ',^OLbl^:^'+params['OLbl']+'^';	
/*		else if(oldLblDef)
			confDetails += ',^OLbl^:^&nbsp;^';*/
		
		if(params['NLbl'] != undefined)
			confDetails += ',^NLbl^:^'+params['NLbl']+'^';			
/*		else if(newLblDef)
			confDetails += ',^NLbl^:^&nbsp;^';*/
		
		if(params['HidOLCol'] != undefined)
			confDetails += ',^HidOLCol^:^'+params['HidOLCol']+'^';
		
		if(params['HidNLCol'] != undefined)
			confDetails += ',^HidNLCol^:^'+params['HidNLCol']+'^';
		
		if(params['ThreeColTbl'] != undefined)
			confDetails += ',^ThreeColTbl^:^'+params['ThreeColTbl']+'^';
		
		if(params['FourColTbl'] != undefined)
			confDetails += ',^ThreeColTbl^:^'+params['ThreeColTbl']+'^';
			
		if(params['ignoreDup'] != undefined)
			confDetails += ',^ignoreDup^:^'+params['ignoreDup']+'^';
		
		if(params['showDetails'] != undefined)
			confDetails += ',^showDetails^:^'+params['showDetails']+'^';
		
		confDetails += '}';
		
		formIns.find('#confValues').remove();
		formIns.append('<input type="text" id="confValues" name="tranDetails" value="'+confDetails+'" style="display:none"/>');
		return confDetails;
	}
	
}	

//format transaction details to a table display
function formatTxnDetails(params){
	
	var confValues = params['confValues'];
	confValues = confValues.replace(/[\^]/g,"\"");	
	//confValues = confValues.replace(/[\']/g,"U39").replace(/[\^]/g,"\"").replace(new RegExp("U39"),"\'");
	confValues = confValues.replace(/(\r\n|\n|\r)/gm, "");
	var confValObj = eval ("(" +confValues+")" );
	var confBody = "";
	
	if(confValObj.confVal != undefined){
		params['valList'] = confValObj.confVal;
		params['tblClass'] = 'confVal';
		
		confBody += formatConfTable(params);
	}
	
	if(!params["detailLimit"]){
		params["detailLimit"]=undefined;
		confBody += '<div>';

		//set table label
		if(confValObj.OLbl != undefined)
			confBody += '<h6>&nbsp;</h6><h6>'+confValObj.OLbl+'</h6>';
		
		if(confValObj.OLHdr == undefined && confValObj.ORHdr != undefined
				&& confValObj.NLHdr == undefined && confValObj.NRHdr != undefined
				|| confValObj.ThreeColTbl != undefined || confValObj.FourColTbl != undefined){
			
			if(confValObj.OLbl == undefined)
				confBody += '<h6>&nbsp;</h6>';
			
			confBody += '<div class="oldValues">';
		}
				
				
			if(confValObj.confOld != undefined){
				params['valList'] = confValObj.confOld;
				params['tblClass'] = 'confOld';
				
				
				//set table header
				if(confValObj.OLHdr != undefined)
					params['tblLHdr'] = confValObj.OLHdr;
				if(confValObj.ORHdr != undefined)
					params['tblRHdr'] = confValObj.ORHdr;
				
				if(confValObj.HidOLCol != undefined)
					params['tblClass'] += ' hideLeftCol';
				
				if(params['OHideHdr'] != undefined && params['OHideHdr'] == 'true')
					params['HideHdr'] = true;
				
				confBody += formatConfTable(params);
				params['tblLHdr'] = undefined;
				params['tblRHdr'] = undefined;
				params['tblClass'] = undefined;
				params['HideHdr'] = undefined;
			}

			//set table label
			if(confValObj.NLbl != undefined)
				confBody += '<h6>&nbsp;</h6><h6>'+confValObj.NLbl+'</h6>';
			
			if(confValObj.FourColTbl != undefined){
				
				if(confValObj.Nlbl == undefined)
					confBody += '<h6>&nbsp;</h6>';
				
				confBody += '</div><div class="oldValues">';
			}
			else if((confValObj.OLHdr == undefined && confValObj.ORHdr != undefined
					&& confValObj.NLHdr == undefined && confValObj.NRHdr != undefined)
					|| confValObj.ThreeColTbl != undefined){
				
				if(confValObj.Nlbl == undefined)
					confBody += '<h6>&nbsp;</h6>';
				
			
					confBody += '</div><div class="newValues">';
			}
			if(confValObj.confNew != undefined){
				params['valList'] = confValObj.confNew;
				params['tblClass'] = 'confNew';
				
				//set table header
				if(confValObj.NLHdr != undefined)
					params['tblLHdr'] = confValObj.NLHdr;
				if(confValObj.NRHdr != undefined)
					params['tblRHdr'] = confValObj.NRHdr;
				if(confValObj.HidNLCol != undefined)
					params['tblClass'] += ' hideLeftCol';
				
				if(params['NHideHdr'] != undefined && params['OHideHdr'] == 'true')
					params['HideHdr'] = true;
				
				confBody += formatConfTable(params);
				params['tblLHdr'] = undefined;
				params['tblRHdr'] = undefined;
				params['HideHdr'] = undefined;
			}
			
			if(confValObj.OLHdr == undefined && confValObj.ORHdr != undefined
				&& confValObj.NLHdr == undefined && confValObj.NRHdr != undefined
				|| confValObj.ThreeColTbl != undefined || confValObj.FourColTbl != undefined)
				confBody += '</div>';
			
			confBody += '</div>';
	}
	
	return confBody;
}

//formats the parameters to a confirmation table for display in the lightbox
function formatConfTable(params){
	
	var valList = params['valList'];
	
	if(valList != undefined){
		var tblClass = params['tblClass'] != undefined ? params['tblClass'] :'';
		
		var confTable = '';
		var prevLabel = '';
		
		var RColClass = params['valClass'] != undefined ? params['lblClass'] : '';
		var LColClass = params['lblClass'] != undefined ? params['lblClass'] : '';
		
		
		/*confTable += '<table class="table table-sm '+tblClass+'">';
		
		if(params['tblLHdr'] != undefined || params['tblRHdr'] != undefined){
			var lHeader = params['tblLHdr'] != undefined ? params['tblLHdr']:'&nbsp;';
			var rHeader = params['tblRHdr'] != undefined ? params['tblRHdr']:'&nbsp;';
			var headerClass = '';
			if(params['HideHdr'] != undefined)
				headerClass = 'class="HideHdr"';
				
			confTable += '<thead '+headerClass+'><th>'+ lHeader+ '</th><th>'+ rHeader+ '</th></thead>';
			
			if(lHeader != '&nbsp;')
				LColClass = '';
		}*/
		var detailLimit = params['detailLimit'] != undefined ?  params['detailLimit'] : valList.length;
		
		
		$.each(valList, function(index, value){
			if(index < detailLimit){
				$.each(value, function(key, value2){
					var label = key;
					
					if($.trim(value2) == '')
						value2= '&nbsp;';
					
					if(label == prevLabel && params["ignoreDup"] != "true")
						label = '';
					
					if($.trim(label) == '')
						label = '&nbsp;';
					
					prevLabel = key;	

					/*LGT 20141119 - for center alignment of dates*/
					var dateRegex = /^[0-9]{2}\/[0-9]{2}$/;
					var dateRegex2 = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;
					

					/*LEL 20161215 - change from table to div. 
					/*GGP 20170105 - added text-right class in label then set column size to 4. 
					 * Regardless of type, set align left*/
						confTable += '<div class="row"><div class="form-group"><label class="col-sm-4 control-label">'
						+label+'</label><div class="col-sm-8"><p class="form-control-static">'+value2+'</p></div></div></div>'
						/*if((dateRegex.test(label) || dateRegex2.test(label)) && (tblClass.indexOf('confOld')>-1 || tblClass.indexOf('confNew')>-1))
						confTable += '<tr><td class="col-sm-6 text-right'+LColClass+'"><label>'+label+'</label>';
					else
						confTable += '<tr><td class="col-sm-6 text-right'+LColClass+'"><label>'+label+'</label>';
					if((dateRegex.test(value2) || dateRegex2.test(value2)) && (tblClass.indexOf('confOld')>-1 || tblClass.indexOf('confNew')>-1))
						confTable += '</td><td class="col-sm-6 text-center'+RColClass+'">'+value2+'</td></tr>';
					else
						confTable += '</td><td class="col-sm-6 text-left'+RColClass+'">'+value2+'</td></tr>';
					*/	
					/*LGT 20141119 - for center alignment of dates*/
					
					//confTable += '<tr><td class="'+LColClass+'">'+label+'</td><td class="'+RColClass+'">'+value2+'</td></tr>';
					
				});
			}
		});
		return confTable += '</table>';
	}else{
		return null;
	}
}