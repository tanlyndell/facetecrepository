var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){  
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    return true;
}
function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}


function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}


function isDate(dtStr,dLbl){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	
	var strMonth=dtStr.substring(0,2)
	var strDay=dtStr.substring(3,5)
	var strYear=dtStr.substring(6,10)
	
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) 
		strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) 
		strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) 
			strYr=strYr.substring(1)
	}
	
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)

	if (pos1==-1 || pos2==-1){		
		manageMessage("autoMessage", dLbl+" date format should be : MM/DD/YYYY. Please specify a valid date.");
		return false
	}
	if(dtStr.length!=10){
		manageMessage("autoMessage", dLbl+" date format should be : MM/DD/YYYY. Please specify a valid date.");		
		return false;
	}
	if (strMonth.length<1 || month<1 || month>12){
		manageMessage("autoMessage", "Please enter a valid month on "+dLbl);		
		return false;
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		manageMessage("autoMessage", "Please enter a valid day on "+dLbl);		
		return false;
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		manageMessage("autoMessage", "Please enter a valid 4 digit year between "+minYear+" and "+maxYear+" "+dLbl);		
		return false;
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		manageMessage("autoMessage", "Please enter a valid date on "+dLbl);			
			return false;
		
	}
return true
}

function ValidateDate(sDate){
	if(trim(sDate.value)=='')
		return true;
	if( sDate.getAttribute("fieldName") == null ) {
		if (isDate(trim(sDate.value),sDate.getAttribute("fieldName"))==false){
			sDate.focus()
			sDate.select()
			return false
		}
	} else {
		if (isDate(trim(sDate.value),sDate.getAttribute("fieldName"))==false){
			sDate.focus()
			sDate.select()
			return false
		}
	}
	
    return true
 }

 function isTime(sTime,dLbl){
 	
 		time = sTime.match(/^((2[0-3])|([0-1][0-9])):([0-5]\d):([0-5]\d)$/);
		if(time==null){
			alert(dLbl+" time format should be : hh:mm:ss. Please specify a valid time.");
			return false;
		}
	return true;
 }
 
function isValidTime(sTime){
	var regexp = /^(([0-1][0-9])|([2][0-3]))([0-5][0-9])$/;
	return regexp.test(sTime);
}
 

function ValidateDatetime(sDate){

	if(sDate.value == "")
		return true;
	
	if( sDate.columnname == null ) {
		if( sDate.value.indexOf(" ") >= 0 ) {
			
			
			date = sDate.value.split(" ");
			if( date[0].length == 0 || date[1].length == 0 ) {
				alert(sDate.name+" datetime format should be : ddmmyyyy hh:mm:ss. Please specify a valid datetime.");
				sDate.focus();
				sDate.select();
				return false;
			}
			
			if( isDate(date[0],sDate.name) && isTime(date[1],sDate.name) ) {
				return true;
			}
			
			sDate.focus();
			sDate.select();
			return false;
		} else {
			
			if( isDate(sDate.value,sDate.name) ) {
				return true;
			} else {
				return false;
			}
		}
	} else {
		if( sDate.value.indexOf(" ") >= 0 ) {
			
			
			date = sDate.value.split(" ");
			if( date[0].length == 0 || date[1].length == 0 ) {
				alert(sDate.columnname+" datetime format should be : ddmmyyyy hh:mm:ss. Please specify a valid datetime");
				sDate.focus();
				sDate.select();
				return false;
			}
			
			if( isDate(date[0],sDate.columnname) && isTime(date[1],sDate.columnname) ) {
				return true;
			}
			
			sDate.focus();
			sDate.select();
			return false;
		} else {
			
			if( isDate(sDate.value,sDate.columnname) ) {
				return true;
			} else {
				return false;
			}
		}
	}
}


function isDate2(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strYear=dtStr.substring(0,pos1)
	var strMonth=dtStr.substring(pos1+1,pos2)
	var strDay=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		return false
	}
	return true
}

 function ValidateDateColName(sDate){
	if(sDate.value=='')
		return true;
	if (isDate(sDate.value,sDate.columnname)==false){
		sDate.focus()
		sDate.select()
		return false
	}
    return true
 }
 

 function ValidateDatetimeColName(sDate)
 {

 	if(sDate.value!=null&&sDate.value!="")
 	{
 		if(sDate.value.length!=8&&sDate.value.length!=17)
 		{
 			alert(sDate.columnname + " datetime format must be ddmmyyyy hh:mm:ss. Please specify a valid datetime.");
			sDate.focus();
			sDate.select() 			
 			return false;
 		}
	
	 	date=sDate.value.split(" ");
		if(date.length==0||date.length>2)
		{
 			alert(sDate.columnname + " datetime format must be ddmmyyyy hh:mm:ss. Please specify a valid datetime");
			sDate.focus();
			sDate.select() 			
 			return false;		
		}

		if(date[0]!=null&&date[0].length!=8)
		{
 			alert(sDate.columnname + " date format must be ddmmyyyy. Please specify a valid date");
			sDate.focus();
			sDate.select() 			
 			return false;			
		}

		if(!isDate(date[0],sDate.columnname))
		{
			alert(sDate.columnname + " date format must be ddmmyyyy. Please specify a valid date");
			sDate.focus();
			sDate.select();
			return false;
		}
		
		if(date.length==2)
		{
			if(date[1]!=null&&date[1].length!=8)
			{
	 			alert(sDate.columnname + " time format must be hh:mm:ss. Please specify a valid time.");
				sDate.focus();
				sDate.select() 			
	 			return false;			
			}		
			if(!isTime(date[1],sDate.columnname))
			{
				alert(sDate.columnname + " time format must be hh:mm:ss. Please specify a valid time.");
				sDate.focus();
				sDate.select();
				return false;
			}			
		}
		
		return true;

	}
	else
		return true;
	
 }


 function compareDate(str,str1){
 	try{
		str = trim(str);
		str1 = trim(str1);
		var temp1=str.substring(6,10)+str.substring(0,2)+str.substring(3,5);

	 	var temp2=str1.substring(6,10)+str1.substring(0,2)+str1.substring(3,5);
	 	if(temp1>temp2){
	 		return true
	 	}else if(temp1==temp2){
	 		return true
	 	}else
	 		return false
	 }catch(exp){
	 }
 }
 
 	 
 function isAfterCurrentDate(str){
 	
 	var currentTime = new Date();
	var month = currentTime.getMonth() + 1
	var day = currentTime.getDate()
	var year = currentTime.getFullYear()
 	
 	if(month < 10)
 		month = '0' + month;
 	if(day < 10)
 		day = '0' + day;
 
 	var temp1=str.substring(6,10)+str.substring(0,2)+str.substring(3,5);
 	var temp2=String(year)+String(month)+String(day);
 	
 	if(temp1>temp2){
 		return true
 	}else if(temp1==temp2){
 		return true
 	}else
 		return false
 }
 
 function isBeforeCurrentDate(str){
 	
 	var currentTime = new Date();
	var month = currentTime.getMonth() + 1
	var day = currentTime.getDate()
	var year = currentTime.getFullYear()
 	
 	if(month < 10)
 		month = '0' + month;
 	if(day < 10)
 		day = '0' + day;
 
 	var temp1=str.substring(6,10)+str.substring(0,2)+str.substring(3,5);
 	var temp2=String(year)+String(month)+String(day);
 	
 	if(temp1<temp2){
 		return true
 	}else if(temp1==temp2){
 		return true
 	}else
 		return false
 }
 
 function trim(s){
	return s.replace(/^\s+|\s+$/g,"");
}