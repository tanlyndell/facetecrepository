/** LEL dateField plugin v1.0 *
* 	This plugin prevents invalid input of dates and validate date format
*/
(function($){
	$.fn.dateField = function(dateFormat,dpSelector){
		var c = $(this).selector;
		/*var dateFormat = 'mm/dd/yyyy';*/
		var dt = dateFormat.replace(/[mdyMDY]/g,'1');
		var vd1 = new RegExp("[\\d]{6,8}");
		var vd2 = new RegExp("[\\d]{2}[-/]{1}[\\d]{2}[-/]{1}[\\d]{4}");
		var vd3 = new RegExp("[\\d]{2}[-/]{1}[\\d]{2}[-/]{1}[\\d]{2}");
		var vd4 = new RegExp("[\\d]{4}[-/]{1}[\\d]{2}[-/]{1}[\\d]{2}");
		var vd5 = new RegExp("[\\d]{4}[-/]{1}[\\d]{2}");
		var vd6 = new RegExp("[\\d]{2}[-/]{1}[\\d]{4}");
		var vd7 = new RegExp("[\\d]{2}[-/]{1}[\\d]{2}");
		var vd8 = new RegExp("[\\d]{4}"); //year
		var dF = dateFormat;
		var regExd = new RegExp("[\\d]");
		var dateFormatL = dateFormat.length;
		if(dateFormat != undefined && $.trim(dateFormat) != "" &&
			(vd1.test(dt) || vd2.test(dt) || 
				vd3.test(dt) || vd4.test(dt)) || vd5.test(dt) || vd6.test(dt) || vd7.test(dt) || vd8.test(dt)){
				
				var d1	= vd1.test(dt);
				var d8 = vd8.test(dt);
				
				if(!d1 && (!d8 || dateFormatL > 4)){
					
					var delim = new RegExp("[^mdyMDY]");
					var delim2 = new RegExp("[^mdyMDY]", "g");
					
					var d1i = dF.match(delim).index;
					var d1d = dF.substring(d1i,d1i+1);
					var d1f = dF.substring(0,d1i);
					dF = dF.substring(d1i+1, dateFormatL);
					
					var dcnt = dateFormat.match(delim2) ? dateFormat.match(delim2).length : 0;
					var d2i = 0;
					var d2d = 0;	
					var d2f = 0;
					if(dcnt > 1){
						d2i = dF.match(delim).index;
						d2d = dF.substring(d2i,d2i+1);	
						d2f = dF.substring(0,d2i);
					}
				}
				
				/***Commented by LGT 20170207***/
/*				if($(this).val().length < 1){
					$(this).val(dateFormat);
					$(this).css('color','#999999');
				}
				else
					$(this).css('color','#000000');*/
				/***Commented by LGT 20170207***/
				
				$(this).attr('maxlength',dateFormatL);
				$(this).attr("dfInit", "true");
				$(this).attr("dformat", dateFormat);
				$(this).attr("dpSelector", dpSelector);
				
				return this.each(function(){
				
					$(document).on("blur",c,function(){
						var v = $(this).val();
						if(v.length > dateFormatL){
							$(this).val(v.substring(0,dateFormatL));
						}
						
						/***Commented by LGT 20170207***/
/*						if(v.length == 0){
							$(this).val(dateFormat);
							$(this).css('color','#999999');
						}
						else if(v.length > 0 && v != dateFormat)
							$(this).css('color','#000000');*/
						/***Commented by LGT 20170207***/
						
					});
					
					$(document).on("focus",c,function(){
						
						/***Commented by LGT 20170207***/
/*						if($(this).val() == dateFormat){
							$(this).val('');
							$(this).css('color','#000000');
						}*/
						/***Commented by LGT 20170207***/
						
					});
					$(document).on("keypress",c,function(event){
						var elem  = $(this);
						
						/***Commented by LGT 20170207***/
/*						if(elem.val() == dateFormat){
							elem.val('');
							elem.css('color','#000000');
						}*/
						/***Commented by LGT 20170207***/
						
						var cc = event.which;
						var a = String.fromCharCode(cc);
						
						if ( (navigator.userAgent.search("MSIE") > -1 && cc == 0)/*For delete*/ 
								|| (navigator.userAgent.search("Firefox") > -1 && cc == 0)/*For delete, left arrow, right arrow*/ 
								|| (navigator.userAgent.search("Firefox") > -1 && cc == 8)/*For backspace*/ ){
							return;
						}
						else if(!(cc >= 48 && cc <=57) && !(a == d1d || a == d2d)){
							event.preventDefault();
							return;
						}
						else{
							if(!d1){
								/*formatDate(event, elem, a);
								
								function formatDate(event, elem, a){
								*/	
								var v = elem.val();
								var l = v.length + 1;
								
								if(l <= d1i+1){
									var r1 = regExd.test(a);
									
									if(r1 && l == (d1i + 1)){
										elem.val(v+d1d);
									}else if(!r1 && l < (d1i + 1)){
										event.preventDefault();
									}
								}else if(l <= d1i+d2i+2){
									var r2 = regExd.test(a);
									
									if(r2 && l == (d1i+d2i+2)){
										elem.val(v+d2d);
									}else if(!r2 && l < (d1i+d2i+2)){
										event.preventDefault();
									}
								}else if(l <= dateFormatL){
									var r3 = regExd.test(a);
									
									if(!r3){
										event.preventDefault();
									}
								}
								//}
							}/*else{
								var v = elem.val();
								var l = v.length + 1;
								if(l > dateFormatL){
									elem.val(elem.val().substring(0,l-1));
								}
							}*/
						}	
					});
					
					$(document).on("keyup",c,function(){
						var v = $(this).val();
						if(v.length > dateFormatL){
							$(this).val(v.substring(0,dateFormatL));
						}
					});
					
					
					$(document).on("change",dpSelector,function(event){
						if(dpSelector != null && dpSelector != undefined){
							var dpVal = $(event.target).val();
							
							/***Commented by LGT 20170207***/
/*							if(dpVal != dateFormat)
								$(c).css('color','#000000');*/
							/***Commented by LGT 20170207***/
						}
					});
					
					/*
					$(document).on("change",c,function(){
						var v = $(this).val();
						if(v != dateFormat)
							$(this).css('color','#000000');
					});*/
				});
		}
		
	};
	
	$.fn.validate = function(vMin, vMax){
		vMin = vMin != null && vMin!= undefined ? vMin : true;
		vMax = vMax != null && vMax!= undefined ? vMax : true;
		
		var isInit = $(this).attr("dfInit") == "true";
		
		if($(this).attr("dformat") == $.trim($(this).val()) || $.trim($(this).val()).length < 1)
			return true;
		
		if(isInit){
			var df = $(this).attr("dformat").toLowerCase().split(/[\/-]{1}/);
			var text = $(this).val();
			var comp = text.split(/[\/-]{1}/g);
			if(comp.length < 0 || df.length < 0)
				return false;
			
			//removed setting to zero
			var mi = -1;
			var di = -1;
			var yi = -1;
			
			for(var x = 0; x < df.length; x++){
				if(df[x] == "m" || df[x] == "mm")
					mi = x;
				else if(df[x] == "d" || df[x] == "dd")
					di = x;
				else if(df[x] == "yy" || df[x] == "yyyy")
					yi = x;
			}
			
			//default value for m,d,y
			var m = 1;
			var d = 1;
			var y = 1900;
			
			var m = mi != -1 ? Number(comp[mi]) : m;
			var d = di != -1 ? Number(comp[di]) : d;
			var y = yi != -1 ? Number(comp[yi]) : y;
			
			var date = new Date(y,m-1,d, 0, 0, 0, 0);
			if( !(date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d) )
				return false;
			
			//check if there is minDate and maxDate
			var dpSelector = $(this).attr("dpSelector");
			var midao = null;
			var madao = null;
			var mida = $(dpSelector).datepicker("option", "minDate");
			var mada = $(dpSelector).datepicker("option", "maxDate");
			
			if(mida != null && mida != undefined && mida.toString().length > 0){
				var midai = $(dpSelector).data("datepicker");
				midao = $.datepicker._determineDate(midai, mida, new Date());
			}
			else{
				midao = new Date(1900, 0, 1, 0, 0, 0, 0);
			}
			
			if(mada != null && mada != undefined && mada.toString().length > 0){
				var madai = $(dpSelector).data("datepicker");
				madao = $.datepicker._determineDate(madai, mada, new Date());
			}
			else{
				madao = new Date(3000, 11, 31, 0, 0, 0, 0);
			}
			
			//check base date and maxDate
			if( (vMin && date.compareTo(midao) < 0) || (vMax && date.compareTo(madao) > 0) )
				return false;
			
			return true;
		}
		else return false;
	};
	
	$.fn.setToBlack = function(){
		$(this).css("color", "#000000");
		$(this).blur();
	};
	$.fn.setToGray = function(){
		$(this).css("color", "#999999");
		$(this).blur();
	};
	$.fn.setToDefault = function(){
		$(this).val($(this).attr("dformat"));
		$(this).css("color", "#999999");
		$(this).blur();
	};
	
}(jQuery));