function setFormFocus(){
	return;
}

function buildTable(a,b) {
	var div = document.getElementById(b);
	document.getElementById(b).innerHTML='';
	
	var table_content = document.createElement('table');
	table_content.className = 'teraTable';
	table_content.id = 'teraTableId';
	table_content.id = 'myTable'
	table_content.cellPadding = "0";
	table_content.cellSpacing = "0";

	var tbody = document.createElement('tbody');
	var form = document.createElement('form');
	
	var th = document.createElement('tr');
	
	for(var k=0; k<arrayHeader.length; k++){
		
		var tdh = document.createElement('th');
		tdh.width='300px';
		
		var t1 = document.createTextNode(arrayHeader[k]);
		
		tdh.appendChild(t1);
		th.appendChild(tdh);
		tbody.appendChild(th);
	}

	for(var x=0;x<arrayNames.length;x++){
		var tr = document.createElement('tr');
		var td = document.createElement('td');
		td.width = '300px';
		var s1 = arrayNames[x];
		var splitStr = s1.toString().split("-");
		var t = document.createTextNode(splitStr[0]);

		td.appendChild(t);
		tr.appendChild(td);
		tbody.appendChild(tr);
		
		var td = document.createElement('td');
		td.width = '300px';
		splitStr = splitStr[1].split("_");
		t = document.createTextNode(splitStr[0]);

		td.appendChild(t);
		tr.appendChild(td);
		tr.id = s1;
		tbody.appendChild(tr);
		
		var td = document.createElement('td');
		var link = document.createElement('a');
		link.setAttribute('href','#');
		link.onclick= new Function("return doRemove(this.parentNode.parentNode)");
		var linkText = document.createTextNode('Remove');
  		link.appendChild(linkText);
		
		td.appendChild(link);
		tr.appendChild(td);
		tbody.appendChild(tr);
	}
	table_content.appendChild(tbody);
	div.appendChild(table_content);
	setFormFocus();
}



function buildTable2(a,b) {
	var div = document.getElementById(b);
	
	var tbody = div.getElementsByTagName("tbody")[0];
	
	var tr = document.createElement('tr');
	var td = document.createElement('td');
	td.width = '300px';
	var splitStr = a.split("-");
	var t = document.createTextNode(splitStr[0]);

	td.appendChild(t);
	tr.appendChild(td);
	tbody.appendChild(tr);
	
	var td = document.createElement('td');
	td.width = '300px';
	splitStr = splitStr[1].split("_");
	t = document.createTextNode(splitStr[0]);

	td.appendChild(t);
	tr.appendChild(td);
	tr.id = a;
	tbody.appendChild(tr);
	
	var td = document.createElement('td');
	var link = document.createElement('a');
	link.setAttribute('href','#');
	link.onclick= new Function("return doRemove(this.parentNode.parentNode)");
	var linkText = document.createTextNode('Remove');
 		link.appendChild(linkText);
	
	td.appendChild(link);
	tr.appendChild(td);
	tbody.appendChild(tr);
	setFormFocus();
	
}

function buildTable3(arrayData, id, arrayDataType) {
	var div = document.getElementById(id);
	
	var tbody = div.getElementsByTagName("tbody")[0];
	var tr = document.createElement('tr');
	
	for(ctr=0; ctr<arrayData.length; ctr++){
		var td = document.createElement('td');		
		if(arrayDataType[ctr]=='amount')	
			td.align = 'right';		
		td.width = '300px';		
		var t = document.createTextNode(arrayData[ctr]);
		
		td.appendChild(t);
		tr.appendChild(td);	
		
	}
	tr.id = arrayData[0];
	tbody.appendChild(tr);
	
	var td = document.createElement('td');
	var link = document.createElement('a');
	link.setAttribute('href','#');
	link.onclick= new Function("return doRemove(this.parentNode.parentNode)");
	var linkText = document.createTextNode('Remove');
 		link.appendChild(linkText);
	
	td.appendChild(link);
	tr.appendChild(td);
	tbody.appendChild(tr);
}




function index(){
	[].indexOf || (Array.prototype.indexOf = function(v,n){
	  n = (n==null)?0:n; var m = this.length;
	  for(var i = n; i < m; i++)
	    if(this[i] == v)
	       return i;
	  return -1;
	});
}

function setDestinationList(arrayNames){
	var toList = "";
	for(var p=0; p<arrayNames.length; p++){
		toList += arrayNames[p];
		if(p<arrayNames.length-1)
			toList+="~tera136~";
	}
	return toList;
}

function setAmountsList(arrayNames){
	var toList = "";
	for(var p=0; p<arrayNames.length; p++){
		toList += arrayNames[p];
		if(p<arrayNames.length-1)
			toList+="_";
	}
	return toList;
}

function setReasonsList(arrayNames){
	var toList = "";
	for(var p=0; p<arrayNames.length; p++){
		toList += arrayNames[p];
		if(p<arrayNames.length-1)
			toList+="#@%";
	}
	return toList;
}


function setTableHeaders(){
	var arrayHeader = new Array();
	
	for(var c=0; c<3; c++){
		arrayHeader[c] = new Array;
	}
	arrayHeader[0] = 'Destination A/C No.';
	arrayHeader[1] = 'Destination A/C Name';
	arrayHeader[2] = ' ';
	
	return arrayHeader;
}


function buildTableGen(a, b){

	var div = document.getElementById(b);
	var tbody = div.getElementsByTagName("tbody")[0];
	var x=0;
	var tr = document.createElement('tr');
	
	for(x=0; x<a.length; x++){
		var td = document.createElement('td');
		var t = document.createTextNode(a[x]);
		if(x == 1){
		  td.style.textAlign = 'right';
		 }
		td.appendChild(t);
		tr.appendChild(td);
		tbody.appendChild(tr);
	}
	
	var td = document.createElement('td');
	var link = document.createElement('a');
	link.setAttribute('href','#');
	link.onclick= new Function("return doRemove(this.parentNode.parentNode)");
	var linkText = document.createTextNode('Remove');
 	link.appendChild(linkText);
	td.align="center"
	td.appendChild(link);
	tr.appendChild(td);
	tr.id = a[0];
	tbody.appendChild(tr);
}

function buildTableGen2(a, b, c){
	var div = document.getElementById(b);
	var tbody = div.getElementsByTagName("tbody")[0];
	var x=0;
	var tr = document.createElement('tr');
	
	for(x=0; x<a.length; x++){
		var td = document.createElement('td');
		td.setAttribute("fieldName", c);
		var t = document.createTextNode(a[x]);
	
		td.appendChild(t);
		tr.appendChild(td);
		tbody.appendChild(tr);
	}
	
	var td = document.createElement('td');
	var link = document.createElement('a');
	link.setAttribute('href','#');
	link.onclick= new Function("return doRemove(this.parentNode.parentNode)");
	var linkText = document.createTextNode('Remove');
 	link.appendChild(linkText);
	
	td.appendChild(link);
	tr.appendChild(td);
	tr.id = a[0]+' - '+a[1];
	tbody.appendChild(tr);
}

function buildTableNoRemove(a, b, id){
	var div = document.getElementById(b);
	var tbody = div.getElementsByTagName("tbody")[0];
	var x=0;
	var tr = document.createElement('tr');
	
	for(x=0; x<a.length; x++){
		var td = document.createElement('td');
		var t = document.createTextNode(a[x]);
	
		td.appendChild(t);
		tr.id = id;
		tr.appendChild(td);
		tbody.appendChild(tr);
	}
}
/**
Added: NLA 112111
multiple values corresponding to a single data in a row
*/
var rowIndicator = 0;
function buildDynamicTable(values, tableId){
	var div = document.getElementById(tableId);
	var tbody = div.getElementsByTagName("tbody")[0];
	var hasArray =false;
	var rowLen = 1;
	var multiVal;
	var arrayPos =0;
	rowIndicator++;	// indicator for odd and even row
	
	//check if array contains array
	for(var i=0; i<values.length; i++){
		if(isArray(values[i])){
			hasArray = true;
			multiVal = values[i];
			rowLen = multiVal.length;
			arrayPos = i;
		}
	}
	var columnLen = values.length;
	
	//construct the table
	for(var i=0; i<rowLen; i++){
		var tr = document.createElement('tr');
		for(var j=0; j<columnLen; j++){
			var td = document.createElement('td');
			var nodeVal = "";
			if(hasArray && i>0){
				if(j===arrayPos){
					nodeVal = multiVal[i];
				}else{
					nodeVal = "";
				}
			}else{
				if(isArray(values[j])){
					nodeVal = multiVal[i];
				}else{
					nodeVal = values[j];
				}
			}
			
			var value = document.createTextNode(nodeVal);
			td.appendChild(value);
			tr.appendChild(td);
			
		}
		if(i===0){
			var td_r = document.createElement('td');
			var link = document.createElement('a');
			link.setAttribute('href','#');
			link.onclick= new Function("return doRemove(this.parentNode.parentNode)");
			var linkText = document.createTextNode('Remove');
			link.appendChild(linkText);
			
			td_r.appendChild(link);
			tr.appendChild(td_r);
		}else{
			var td_r = document.createElement('td');	
			tr.appendChild(td_r);
		}
		tr.name = rowLen;
		tr.onmouseover=new Function("return do_onMouseOver(this)");
		tr.onmouseout=new Function("return do_onMouseOut(this)");
		tr.onclick=new Function("return do_onClick(this)");
		
		if((rowIndicator%2) === 0){
			tr.bgColor="#FFFFFF";
		}else{
			tr.bgColor="#F9DDDD";
		}
		tbody.appendChild(tr);
		
	}
}

//checks if element is an array
function isArray(obj) {
	//returns true is it is an array
	if (obj.constructor.toString().indexOf("Array") == -1)
		return false;
	else
		return true;
}

var setTblPrp = false;
function setTeraTable() {
	var tables = document.getElementsByTagName("table"), trs, checkBoxes, checkBoxTr;
	var appendClass;
	for (x=0; x<tables.length; x++){
		
		if (tables[x].className && tables[x].className.indexOf("teraTable") > -1) {
			trs = tables[x].getElementsByTagName("tr");
			for (var y = 0; y < trs.length; y++) {
				if (y % 2 == 0 && y > 0)
					trs[y].className = "even";
				else if (y > 0)
					trs[y].className = "odd";
				trs[y].onmouseover = function() {
					this.className = this.className.replace(this.className, this.className + "_hovered");
				}
				trs[y].onmouseout = function() {
					this.className = this.className.replace("_hovered", "");
				}
			}
		}
	}
	setTblPrp = true;
}