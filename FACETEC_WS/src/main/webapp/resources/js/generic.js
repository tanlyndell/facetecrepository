/**
 *	AUTHOR: Louie John E. Leona
 *	DATE CREATED: 2016/08/17
 *	DESCRIPTION: Generic scripts
 */
document.onmouseover=hidestatus
document.onmouseout=hidestatus
document.onmousedown=hidestatus
document.onclick=hidestatus

$(function(){
	
	//Restrict field to numbers and 1 period
	$(document).on('keypress','.amount',function(event){
		if(!$(this).attr("readonly")){	
			var charCode = event.which;
										
			if($(this).val().indexOf('.') > -1	&& charCode == 46 )
				return false;
			
			if($(this).val() == '' && charCode == 46){
				$(this).val('0');
				return true;
			}
			
			if ((navigator.userAgent.search("MSIE") > -1 && charCode == 0)/*For delete*/ 
				|| (navigator.userAgent.search("Firefox") > -1 && charCode == 0)/*For delete, left arrow, right arrow*/ 
				|| (navigator.userAgent.search("Firefox") > -1  && charCode == 8)/*For backspace*/ 
				|| charCode == 46)
					return true;
					
			else if(charCode < 48 || charCode > 57){
				event.preventDefault();
				return false;
			}
			return true;
		}
	});
	
	
	$(document).on('blur','.amount',function(){
		
		if(!$(this).attr("readonly")){
			var amount = $.trim($(this).val());
			$(this).val(convertToAmount(amount));
		}
	});
	
	$(document).on('focus','.amount',function(){
		
		if(!$(this).attr("readonly")){
			var amount = $.trim($(this).val());
			var len = amount.length;
			
			if(len > 0)
				$(this).val(amount.replace(/[^\d.]/g,''));
		}
	});

	/**LGT 20170329: Added row click function**/ 
   /* $('table > tbody > tr').click(function(event) {
    	if($(this).find(":checkbox").length > 0){
    		if(event.target.type !== 'checkbox')
    			$(':checkbox', this).trigger('click');
    	}
    	else if($(this).find(":radio").length > 0){
    		if(event.target.type !== 'radio')
    			$(':radio', this).trigger('click');
    	}
    	else if($(this).find("a").length > 0 && $(this).find("a").attr("class") && $(this).find("a").attr("class") == 'linkColumns'){
	    	window.location = $(this).find("a").attr("href");
    	}
    	
    	
    });*/
    /**LGT 20170329: Added row click function**/	
	
    $(document).on('click', 'table tbody tr:not(".selected")', function(event){
    	if (event.target.type !== 'checkbox' && event.target.type !== 'text') {
  	      	$(':checkbox', this).trigger('click');
  	    }
    	if(event.target.type !== 'radio' && event.target.type !== 'text'){
    		$(':radio', this).trigger('click');
    	}
    	//if there is checkbox column, allow multiple select
    	if($(this).find('.cb').length > 0) {
            $(this).addClass('selected');
    	} else {
    		$(this).addClass('selected').siblings().removeClass('selected');
    	}
    }).on('click', 'table tbody tr.selected', function(event){
    	if (event.target.type !== 'checkbox' && event.target.type !== 'text') {
  	      	$(':checkbox', this).trigger('click');
  	    }
    	if(event.target.type !== 'radio' && event.target.type !== 'text'){
    		$(':radio', this).trigger('click');
    	}
        $(this).removeClass('selected');
    });
    
    $('textarea').each(function(){
    	addTextareaEvent($(this));
	});
	
	$("input[dataType='numeric']").blur(function() { 
		var value = $(this).val().replace(/^0+/, '') 
		$(this).val(value); 
	});
	
	Number.prototype.padLeft = function(base,chr){
	    var  len = (String(base || 10).length - String(this).length)+1;
	    return len > 0? new Array(len).join(chr || '0')+this : this;
	}
	
	//START: redirect to Home when back button is clicked (works in IE, FF, CH)
	history.pushState(null, null, location.href);
    window.onpopstate = function () {
    	window.location.href = 'Home.htm?dashboard=true';
    }
	//END: redirect to Home when back button is clicked (works in IE, FF, CH)

	//START: disable back button (works in IE, FF, CH)
/*	history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };*/
	//END: disable back button (works in IE, FF, CH)
    
});

function viewMode(){
	$('form[id=txnBean] input, form[id=txnBean] textarea, form[id=txnBean] button').attr('disabled','disabled');
	$('.header-button button').removeAttr('disabled');
	$('.modal-footer button').removeAttr('disabled');
	$('.header-button button').show();
	$('.modal-footer button').show();
	$('.buttonsDiv button').hide();
	$('.buttonsDiv span.btn').hide();
	$('div#progress').hide();
	$('#buttonsDivBack').removeAttr('disabled');
	$('#dashboardBtn').removeAttr('disabled');
	$('#buttonsDivBack').show();
	$('.mandatory').removeClass('mandatory');
	$('.counter').hide();
	setHasChanges(false);
}

function addTextareaEvent(textarea) {
	var maxLength = parseInt(textarea.attr('maxlength'));
	var maxLengthOrig = maxLength;
	var ctr = textarea.val().split('\n').length - 1;
	maxLength = parseInt(maxLengthOrig) ;
	
	currLength = textarea.val().replace(/(\r\n|\n|\r)/g, '  ').length;
	textarea.attr('maxlength', maxLengthOrig-ctr);
	
	remLength =  maxLength - currLength;
	var counter = $('<span class="counter">remaining characters: ' + remLength + '</span>');
	
	if(textarea.is(':disabled') || isNaN(remLength)) {
		//added condition, remove the counter element after the current textarea
		if(textarea.next().hasClass('counter'))
			textarea.next().remove();
	}
	else{
		if(textarea.next().hasClass('counter')) {
			textarea.next().remove();
		}
		textarea.after(counter);		
	}
	
	textarea.on('keyup', function(event) {
		if(!isNaN(maxLengthOrig)) {
			var ctr = $(this).val().split('\n').length - 1;
			maxLength = parseInt(maxLengthOrig) ;
			
			currLength = $(this).val().replace(/(\r\n|\n|\r)/g, '  ').length;
			$(this).attr('maxlength', maxLengthOrig-ctr);
			
			remLength =  maxLength - currLength;
			counter.text('remaining characters: '+remLength.toString());
			
			if(remLength < 0){
				var cursor = $(this).getCursorPosition();
				
				//split value start and end
				var startStr = $(this).val().substr(0, cursor);
				var endStr = $(this).val().substr(cursor);
				console.log("cursor position: " + cursor);
				
				//remove 1 character in startStr then append endStr
				var newStr = startStr.substr(0, startStr.length-1) + endStr;
				$(this).val(newStr);
				
				//update caret position
				$(this).caretTo(cursor-1);
				$(this).trigger('keyup');
			}
		}
	});
}

function convertToAmount(amount) {
	var len = amount.length;
	
	if(len > 0){
		amount = convertToDecimal(amount);
		amount = addCommas(amount);
	}
	return amount;
}

function convertToDecimal(amount) {
	var len = amount.length;
	
	if(len > 0){
	
		var decimalplace = amount.indexOf('.');
		var decimal = 2;
		
		//drop excess decimal point (2)
		if((decimalplace > -1) && (len - decimalplace) - 1 > decimal)
			amount = amount.substring(0,decimal + decimalplace + 1);
		
		amount = parseFloat(amount).toFixed(decimal);
		//for 10,2; maxlength: 13
		if(amount > 999999999.99)
			amount = 999999999.99;
		
		//for 18,2; maxlength: 21
		/*if(amount > 99999999999999999.99)
			amount = 99999999999999999.99; */
	}
	return amount;
}

function removeCommas(nStr){
	return nStr.replace(/\,/g, '');
}

function addCommas(nStr){
	nStr += '';
	x = $.trim(nStr).split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3}),?/;
	while (rgx.test(x1)) {
	  x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1+x2;
}

function hidestatus(){
	window.status=''
	return true
}


function manageMessage(elemId, msg) { 
	if(elemId == "autoMessage"){
		$('body').find('div.alert').remove();
		$('body').find('h1:last').after("<div class='alert alert-danger'>"+msg+"</div>").show();	
	}
	else{
	// ADDED // Highlights fields with error  
	showErrorOnField(elemId,'',true);
	//END ADDED
	$('body').find('div.alert').remove();
	$('body').find('h1:last').last().after('<div class="alert alert-danger">'+msg+'</div>').show();
	}

	$('body').scrollTop(0);
	return false;
}

function customMessage(elemId, msg, alertClass) { 
	if(elemId == "autoMessage"){
		$('body').find('div.alert').remove();
		$('body').find('h1:last').after("<div class='alert "+alertClass+"'>"+msg+"</div>").show();	
	}
	else{
		// ADDED // Highlights fields with error  
		showErrorOnField(elemId,'',true);
		//END ADDED
		$('body').find('div.alert').remove();
		$('body').find('h1:last').last().after('<div class="alert '+alertClass+'">'+msg+'</div>').show();
	}

	$('body').scrollTop(0);
	return false;
}

//LEL20130402
//set on page validation error
function clearAllErrors(form){
	
	if(!form.jquery)
		form = $('#'+form);
	form.find('div.alert').remove();
	form.find('.has-error').removeClass('has-error');
	
	//GGP20170213 Added
	form.find('.form-field').find('div[class*="col-"]').unwrap();
	form.find('.form-label').find('label.control-label').unwrap();
	
	form.find('.has-error').find('span.form-control-feedback').remove();
	form.find('div.err-inline').remove();
	
	form.find('.alert-danger').removeClass('alert-danger');
	form.find('#navtabs').find('.badge').remove();

	$('textarea').keyup();
}

function showErrorOnField(id,msg,highlight,focus){
	
	if(!id.jquery)
		id = $('#'+id);
		
	if(id.attr('altField') != undefined)
		id = $('#'+id.attr('altField'));
	
	if(id.closest('div.form-group').length != 0) {
		if(id.closest('div.form-group').find('label.control-label').parent().is( "div.form-label" )){
			id.closest('div.form-group').find('label.control-label').unwrap();
		}
		if(id.closest('div.form-sameline').find('label').parent().is( "div.form-sameline-label" )){
			id.closest('div.form-sameline').find('label').unwrap();
		}
		if(id.closest('div[class*="col-"]').parent().is( "div.form-field" )){
			id.closest('div[class*="col-"]').unwrap();
		}
		
		id.closest('div.form-group').find('label.control-label').wrap(('<div class="form-label"></div>'));
		
		id.closest('div[class*="col-"]').wrap(('<div class="form-field"></div>'));
		id.closest('div.form-field').addClass('has-error');

		id.closest('div.form-group').find('div.form-label').addClass('has-error');
		
		//LGT 20171212: For alerts on tab headers
		if(id.closest('div.tabHeader').length != 0 && id.closest('div.form-group').parent().attr('class').indexOf('modal-body')<0){
			var tabHeaderId = id.closest('div.tabHeader').attr('id');
			var tabLink = $('a[href="#' + tabHeaderId + '"]');
			var numErrors = $('#'+tabHeaderId).find('div.form-field.has-error').length;
			$(tabLink).parent().append('<span class="badge">'+ numErrors +'</span>');
			//$(tabLink).parent().addClass('alert-danger');
		}
	}
	
	//for table input
	if(id.closest('td.input-cell').length != 0){
		id.closest('td.input-cell').addClass('has-error');

		//LGT 20171212: For alerts on tab headers
		if(id.closest('div.tabHeader').length != 0){
			var tabHeaderId = id.closest('div.tabHeader').attr('id');
			var tabLink = $('a[href="#' + tabHeaderId + '"]');
			var numErrors = $('#'+tabHeaderId).find('table .has-error').length;
			$(tabLink).parent().append('<span class="badge">'+ numErrors +'</span>');
		}
		
		if(msg != ''){
			
			id.parent().append('<div class="err-inline">'+msg+'</div>');
		}

	}

	if(msg != ''){
		
		if(id.closest('div.form-field').find('div.err-inline').length < 1)
			id.closest('div.form-field > div').append('<div class="err-inline">'+msg+'</div>');
	}
	if(highlight == undefined || highlight == true)
		id.addClass('errHighlight');
	
	try{
		
		if(focus == undefined || focus == true){
			
			if($('#'+id.attr('id').length > -1))
				id = $('#'+id.attr('id')+"_view");
			
			$('html, body').scrollTop(id.offset().top - 200);
			
			id.focus();
		}
		
	}catch(er){}
		
	return false;
}

//GGP20170111 added this function which is used in updatePasswordValidity in userdefinitionvalidator
//remove error on field
function removeErrorOnFieldCustom(id){

	if(!id.jquery)
		id = $('#'+id);

	if(id.attr('altField') != undefined)
		id = $('#'+id.attr('altField'));
	
	id.closest('div.form-group').removeClass('has-error');
	id.closest('div.form-group').find('div.err-inline').remove();
	id.removeClass('errHighlight');
	return true;

}
//remove error on field
function removeErrorOnField(id){

	if(!id.jquery)
		id = $('#'+id);

	if(id.attr('altField') != undefined)
		id = $('#'+id.attr('altField'));
	
	id.closest('div.form-group').removeClass('has-error');
	id.parent().find('div.err-inline').remove();
	id.removeClass('errHighlight');
	return true;

}

function capsLockOn(e){
	var capsOnElem = $('#capsOn');
	var kc = e.keyCode?e.keyCode:e.which;
	var sk = e.shiftKey?e.shiftKey:kc == 16;
	capsOnElem.remove();
	if(((kc >= 65 && kc <= 90) && !sk) || ((kc >= 97 && kc <= 122) && sk)) {
		
		$('#warnDiv').after('<div id="capsOn" class="alert alert-warning">Caps Lock is On</div>');
		
	}
}

function inputError(id){
	if(!id.jquery)
		id=$('#'+id);
	id.parent('div.form-group').addClass('has-error');
	id.focus();
}

function inputErrorRemove(id){
	if(!id.jquery)
		id=$('#'+id);
	id.parent('div.form-group').removeClass('has-error');
	id.focus();
}


/*For Tera Table function*/
function doSubmitForm(formObject) {
	if (typeof formObject.onsubmit == "function") {
		if (!formObject.onsubmit())
			return;
	}
	formObject.submit();
}

/*************************
get the value of a radio button group.
parameter: radio button array (document.getElementsByName)
return: value of the selected radio button. null if nothing is selected
***************************/
function getSelectedRadioValue(radioArray){
	for(var i=0; i<radioArray.length; i++){
		if(radioArray[i].checked){
			return radioArray[i].value;
		}
	}	
	return null;
}

/**
* tableObj - the table that holds the checkbox element.
* returns boolean
*/
function hasSelectedValue(tableObj, checkCol){
	
	var my_table2 = tableObj;
	var rows = my_table2.getElementsByTagName('tr');
	var rowCount = rows.length -1;
	var nodeIndex = 0;
	if(checkCol == "" || checkCol == null ){
		checkCol=0;
	}
	
	for(var i =1;i<=rowCount;i++)
	{
		if(rows[i].cells[checkCol].childNodes[nodeIndex].checked == true){
			return true;	
		}
	}
	
	return false;
}

function checkMandatory(formID, checkHidden){
	
	clearAllErrors(formID); //Added //Clear Error Highlights
	
	var noError = true;
	var firstField = undefined;
		
	$('#'+formID+ ' [mandatory="true"]').each(function(index, value){

		if((checkHidden && $(this).attr("id").indexOf("modal")<0) || $(this).is(":visible:enabled") ){		
			var mandatory = 'Mandatory';

			if($(this).attr('mandatoryErrMessage')!=null){
				mandatory= $(this).attr('mandatoryErrMessage');
			}else{
				mandatory= 'Mandatory';
			}
	
			var type = $(this).prop('tagName'); 
			var ok = true;
			if(type == 'INPUT'){
				if(!($(this).is(':radio') || $(this).is(':checkbox'))){
					if($.trim($(this).val()) == '' || ($(this).attr('default') != undefined && $.trim($(this).val().toUpperCase()) == $.trim($(this).attr('default').toUpperCase()))){
						showErrorOnField($(this),mandatory);
						noError = false;
						if(firstField == undefined)
							firstField = $(this);
					}				
				}else if($(this).is(':radio') || $(this).is(':checkbox')){// for radio button
					var name = $(this).attr('name');
					if($('input[name="'+name+'"]:checked').length < 1){
						showErrorOnField($(this),mandatory,false);
						noError = false;
						if(firstField == undefined)
							firstField = $(this);
					}
				}
			}else if(type == 'SELECT'){//for select
						
				if($(this).find('option:selected').val()==''){				
					showErrorOnField($(this),mandatory,undefined);
					noError = false;
					if(firstField == undefined)
						firstField = $(this);
				}
			}else if(type == 'TEXTAREA'){
				if($.trim($(this).val()) == '' || ($(this).attr('default') != undefined && $(this).val().toUpperCase() == $(this).attr('default').toUpperCase())){
					showErrorOnField($(this),mandatory);
					noError = false;
					if(firstField == undefined)
						firstField = $(this);
				}	
			}else /*if(type == 'TD')*/{

				var hasValue = true;
				
				$(this).find("input[exemptManda!='true'], select[exemptManda!='true']").each(function(index,value){
					
					
					if($(this).prop('tagName') == 'SELECT'){
	
						if($.trim($(this).find('option:selected').val())==''){
							hasValue = false;
							$(this).addClass("errHighlight");
						}
						
					}else if($(this).prop('tagName') == 'INPUT'  || ($(this).attr('default') != undefined && $.trim($(this).val().toUpperCase()) == $.trim($(this).attr('default').toUpperCase()))){
						if(!($(this).is(':radio') || $(this).is(':checkbox')) && 
							($.trim($(this).val())=='') ){
							hasValue = false;
							$(this).addClass("errHighlight");
						}
						else if($(this).is(':radio') || $(this).is(':checkbox')){// for radio button and checkbox
							var name = $(this).attr('name');
							if($('input[name="'+name+'"]:checked').length < 1){
								hasValue = false;
								//$(this).addClass("errHighlight");
							}
						}
					}
					/*
					if(!hasValue){
						$(this).addClass("errHighlight");
						if(firstField == undefined)
							firstField = $(this);
					}*/
					//}
				});
				if(!hasValue){
					showErrorOnField($(this),mandatory,false);
					//$(this).find("[exemptManda!='true']").addClass('errHighlight');
					
					//$(this)./*find("input[exemptManda!='true'], select[exemptManda!='true']").last().parent().*/append('<div class="errMsgPerField">'+mandatory+'</div>');
					noError = false;
				}
			}
		}
	});
	
	if(firstField != undefined){
		
		if($('#'+firstField.attr('id')+"_view").length > 0)
			firstField = $('#'+firstField.attr('id')+"_view");
				
		$('html, body').scrollTop(firstField.offset().top - 200);
		
		firstField.focus();
	
	}	
	
	return noError;
}

/**
 * Check if input field contains invalid characters.
 * Checking is based on the dataType attribute of field.
 * Iterates through all input fields in the form
 * formID - ID of form
 */
function checkInvalidChar(formID){
	var noError = true;
	var firstField = undefined;
	
	//RegEx for valid characters
	var regexMap = new Object();
	
	//default valid characters
	//regexMap["valid"] = /^((?![<>^"\[\]\{\}\\])(?!(&lt;))(?!(&gt;))[\S\s])*$/;
	regexMap["valid"] = /^((?![<>^"\[\]\{\}])(?!(&lt;))(?!(&gt;))[\S\s])*$/; //removed backslash to accommodate directory values
	
	//custom RegEx		
	regexMap["alphanum"] = /^[Ñña-zA-Z0-9 \r\n]+$/;
	regexMap["numeric"] = /^[0-9]+$/;
	regexMap["numeric2"] = /^[0-9]+$/;
	regexMap["address"] = /^[Ñña-zA-Z0-9 \#\-\,\.\r\n]+$/;
	regexMap["userID"] = /^[a-zA-Z0-9\-_]+$/;
	regexMap["code"] = /^[a-zA-Z0-9_-]+$/;
	regexMap["name"] = /^[0-9Ñña-zA-Z \-\,\.\r\n]+$/;
	regexMap["amount"] = /^\d{1,3}(?:(,?)\d{3}(?:\1\d{3})*)?(?:\.\d{1,2})?$/;
	
	regexMap["faxNumber"] = /^[0-9\+]+$/;
	regexMap["telephone"] = /^[0-9\-]+$/;
	regexMap["email"] = /^[_A-Za-z0-9\-\.@]+$/;
	regexMap["version"] = /^[0-9\.]+$/;

	regexMap["branchName"] = /^[ñÑ.,a-zA-Z0-9\(\)\- ]+$/;

	regexMap["alphanum_2"] = /^[a-zA-Z0-9 \r\n]+$/;
	regexMap["javaclass"] = /^[Ñña-zA-Z0-9\.\_]+$/;
	regexMap["money"] = /^[0-9\.]+$/;
	regexMap["money2"] = /^[0-9\.\,]+$/;
	regexMap["mobile"] = /^\+{0,1}[0-9]+$/;
/*	regexMap["faxNumber"] = /^[\+]{0,1}[\d]+$/;
	regexMap["email"] = /^[_A-Za-z0-9\-]+(\.[_A-Za-z0-9\-]+)*@[A-Za-z0-9]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$/;*/

	regexMap["dateformat"] = /^[0-9/]+$/;
	
	/**LGT 20170322: Parameter Maintenance**/
	regexMap["int"] = /^[0-9]+$/;
	regexMap["tinyint"] = /^[0-9]+$/;
	regexMap["smallint"] = /^[0-9]+$/;
	regexMap["bigint"] = /^[0-9]+$/;
	regexMap["numeric"] = /^[0-9]+$/;
	regexMap["float"] = /^[0-9\.]+$/;
	regexMap["decimal"] = /^[0-9\.]+$/;
	regexMap["char"] = /^[a-zA-Z0-9 \-\.]+$/;
	regexMap["varchar"] = /^[_Ñña-zA-Z0-9 \-\.\r\n]+$/;
	regexMap["text"] = /^[_Ñña-zA-Z0-9 \-\.\r\n]+$/;
	regexMap["nvarchar"] = /^[_Ñña-zA-Z0-9 \-\.\r\n]+$/;
	regexMap["ntext"] = /^[_Ñña-zA-Z0-9 \-\.\r\n]+$/;
	regexMap["datetime"] = /(^(0[1-9]|1[012])[/](0[1-9]|[12][\d]|3[01])[/](19|20)\d\d$)/;
	//image
	/**LGT 20170322: Parameter Maintenance**/
	
	$('#'+formID+ ' input, ' + '#'+formID + ' textarea').each(function(index, value){
		
		if($(this).attr('id') != 'confValues'){
			
			if($.trim($(this).val()) != '' && $(this).attr("dataType") != 'exempt' && !regexMap['valid'].test($.trim($(this).val()))){
				showErrorOnField($(this),'Contains invalid character(s).');
				noError = false;
				if(firstField == undefined)
					console.log($(this))
					firstField = $(this);
			}
		}
	});
	

	/*if(noError){*/
		$('#'+formID+ ' [dataType]').each(function(index, value){
			if($(this).attr('id') != 'confValues'){
				if($.trim($(this).val()) != '' && regexMap[$(this).attr("dataType")] != undefined && !regexMap[$(this).attr("dataType")].test($.trim($(this).val()))){
					showErrorOnField($(this),'Contains invalid character(s).');
					noError = false;
					if(firstField == undefined)
						firstField = $(this);
				}
			}	
		});
	/*}*/
	
		if(firstField != undefined){
			
			if($('#'+firstField.attr('id')+"_view").length > 0)
				firstField$('#'+firstField.attr('id')+"_view").focus();
			
			$('html, body').scrollTop(firstField.offset().top - 200);
			
			firstField.focus();
		}	
	return noError;
}

function checkInvalidDate(formID){
	
	var noError = true;
	var firstField = undefined;
	
	$('#'+formID+ ' [dataType=dateformat]').each(function(index, value){
		if($(this).attr('id') != 'confValues'){
			if($.trim($(this).val()) != '' && !$(this).validate()){
				showErrorOnField($(this), "Invalid Date.");
				noError = false;
				if(firstField == undefined)
					firstField = $(this);
			}
		}	
	});
	
	$('#'+formID+ ' [dataType=dateyear]').each(function(index, value){
		if($(this).attr('id') != 'confValues'){
		    var d = new Date();
		    var now = d.getFullYear();
		    d.setFullYear(now-100);
		    var before = d.getFullYear();
			if($.trim($(this).val()) != '' && !($(this).val()>=before && $(this).val()<=now)){
				showErrorOnField($(this), "Invalid Date.");
				noError = false;
				if(firstField == undefined)
					firstField = $(this);
			}
		}	
	});

	$('#'+formID+ ' [dataType=datemonthyear]').each(function(index, value){
		if($(this).attr('id') != 'confValues'){
			if($.trim($(this).val()) != ''){
				var monthYear = new RegExp("(0[123456789]|10|11|12)([/])([1-2][0-9][0-9][0-9])");
				var dS = $.trim($(this).val()).split("/");
			    var d = new Date();
			    var now = d.getFullYear();
			    d.setFullYear(now-100);
			    var before = d.getFullYear();
				if(!monthYear.test($.trim($(this).val())) || !(dS[1]>=before && dS[1]<=now)){
					showErrorOnField($(this), "Invalid Date.");
					noError = false;
					if(firstField == undefined)
						firstField = $(this);
				}

			}
		}	
	});
	
	return noError;
}


function checkInvalidFormatValue(formID){
	
	var noError = true;
	var firstField = undefined;
	
	//RegEx for valid characters
	var regexMap = new Object();

	regexMap["valid"] = /^((?![<>=])(?!(&lt;))(?!(&gt;))[\S\s])*$/
	regexMap["faxNumber"] =/^[\+]{0,1}[\d]+$/;
	regexMap["telephone"] = /(^\d{3}\-\d{4})$|(^\d{7,11})$/;	
	regexMap["email"] = /^[_A-Za-z0-9\-]+(\.[_A-Za-z0-9\-]+)*@[A-Za-z0-9]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$/;
	regexMap["amount"] = /^\d{1,3}(?:(,?)\d{3}(?:\1\d{3})*)?(?:\.\d{1,2})?$/;
	regexMap["version"] = /^[0-9]+(\.[0-9]{1,2}){0,3}?$/;
	regexMap["dateformat"] = /(^(0[1-9]|1[012])[/](0[1-9]|[12][\d]|3[01])[/](19|20)\d\d$)/;
	
	$('#'+formID+ ' input').each(function(index, value){
		
		if($.trim($(this).val()) != '' && $(this).attr("dataType") != 'exempt' && !regexMap['valid'].test($.trim($(this).val()))){
			showErrorOnField($(this),'Contains invalid character(s).');
			noError = false;
			if(firstField == undefined)
				firstField = $(this);
		}
	});
	
	if(noError){
		$('#'+formID+ ' [dataType]').each(function(index, value){
			
			if($.trim($(this).val()) != '' && $(this).attr("dataType") != 'exempt' && regexMap[$(this).attr("dataType")] != undefined && !regexMap[$(this).attr("dataType")].test($.trim($(this).val()))){
				showErrorOnField($(this),'Contains invalid value(s)');
				noError = false;
				if(firstField == undefined)
					firstField = $(this);
			}	
		});
	}
	
	return noError;
}

function trim(s){
	return s.replace(/^\s+|\s+$/g,"");
}

//GGP20161206
function doClearForm(formId, clearHidden) {
	var formElem = document.getElementById(formId);
	var formControl = formElem.getElementsByClassName('form-control'),
		formSelect = formElem.getElementsByTagName('select'),
		formTextArea = formElem.getElementsByTagName('textarea'),
		formRadio = formElem.getElementsByClassName('radioItem');

	var x = 0;
	while(x < formControl.length || x < formSelect.length || x < formTextArea.length || x < formRadio.length){
		if (x < formControl.length && !formControl[x].readOnly  && !formControl[x].disabled && (formControl[x].type == "text" || formControl[x].type == "password" ||
				(clearHidden && formControl[x].type == "hidden")))
		{	
			formControl[x].value = '';
			$('#'+formControl[x].id).keyup();
		}
		if (x < formRadio.length && !formRadio[x].readOnly  && !formRadio[x].disabled && formRadio[x].type == "radio")
			formRadio[x].checked = false;
		if (x < formSelect.length){
			if(formSelect[x].getAttribute("exceptClear")=="true"){
				x++;
				continue;
			}
			
			if((formSelect[x].id).indexOf("listbox__")!= -1){
				pId=(formSelect[x].id).substring(9);
				document.getElementById(pId).value="";
			}
			formSelect[x].selectedIndex = 0;
			// formSelect[x].value = "";
		}
		if (x < formTextArea.length) {
			formTextArea[x].value = '';
			$('textarea').keyup();
		}
		x++;
	}
	if (document.getElementById("autoMessage") != null &&
			trim(document.getElementById("autoMessage").innerHTML) != "")
			document.getElementById("autoMessage").innerHTML = "";
	hasFocus = false;
	clearedForm = false;
    
    $('.select').each(function() {
    	if($(this).find("input[type=text]").is(":enabled")) {
    		var id = $(this).find("input[type=hidden]").attr("id");
    		doClearDropdownValue(id);
    	}
    });
}

function doClearAllDropdownValue(){
     $('.select input[type=text]').val("").trigger('change');
     $('.select input[type=hidden]').val("").trigger('change');
     $('.select').find('.list-group').show().scrollTop(0).hide();
     $('.select').find('.list-group').find('.list-group-item.liHover').removeClass("liHover");
}

function doClearDropdownValue(id){
     $("#"+id).val("").trigger('change');
     $("#"+id+"_view").val("").trigger('change');
     $("#"+id).siblings('.list-group').show().scrollTop(0).hide();
     $("#"+id).siblings('.list-group').find('.list-group-item.liHover').removeClass("liHover");
}

//GGP20161206
function redirectWindow(url){
	clearAllErrors("txnBean");
	
	//if add
	if(url.indexOf('add')>0) { 
		window.location=url;
	}
	else {
		if(!go && hasSelectedValue(document.getElementById("teraTableId"))){
			var selVal = getSelectedRadioValue(document.getElementsByName("checkBox"));
			go=true;
			$(".imageButton").addClass("disabled");
			window.location=url+selVal;
		}else{
			manageMessage('autoMessage','<spring:message code="specialMessage.validation.select" />');
		}
	}
}

function isNumber(num){
	num = trim(num);
	var regEx = new RegExp('[0-9]*');
	var match = regEx.exec(num);		
	if(match == num)				
		return true;				
	else			
		return false;				
}

//GGP20161208
function setDefaultValues2(frm){
	var arrDefValues = new Array();
	var count = 0;
	for(var i=0; i<frm.length; i++){
		if(frm[i].tagName.toLowerCase() == "select"){
			arrDefValues[count] = trim(frm[i].value);
			count++;
		}
		else if(frm[i].tagName.toLowerCase() == "input" && frm[i].type == "radio"){	
			if(frm[i].checked){
				arrDefValues[count] = trim(frm[i].defaultValue);
				count++;
			}
		}
		
		//Added for checkbox
		else if(frm[i].tagName.toLowerCase() == "input" && frm[i].type == "checkbox"){	
			if(frm[i].checked){
				arrDefValues[count] = trim(frm[i].defaultValue);
				count++;
			}
		}
		//Added another condition to not include datepickers
		else if(frm[i].tagName.toLowerCase() == "input" && frm[i].type == "text" && (frm[i].id).substring((frm[i].id).length - 6).toLowerCase() != "hidden"){	
			arrDefValues[count] = trim(frm[i].defaultValue);
			count++;
		}
		else if(frm[i].tagName.toLowerCase() == "input" && frm[i].type == "password"){	
			arrDefValues[count] = trim(frm[i].defaultValue);
			count++;
		}
		
		//Added for text area
		else if(frm[i].tagName.toLowerCase() == "textarea"){	
			arrDefValues[count] = trim(frm[i].value);
			count++;
		}
	}
	return arrDefValues;
}

//GGP20161208
function setDefaultValues3(frm){
	var arrDefValues = new Array();
	var count = 0;
	for(var i=0; i<frm.length; i++){
		if(frm[i].tagName.toLowerCase() == "select"){
			arrDefValues[count] = trim(frm[i].value);
			count++;
		}
		else if(frm[i].tagName.toLowerCase() == "input" && frm[i].type == "radio"){	
			if(frm[i].checked){
				arrDefValues[count] = trim(frm[i].value);
				count++;
			}
		}
		
		//Added for checkbox
		else if(frm[i].tagName.toLowerCase() == "input" && frm[i].type == "checkbox"){	
			if(frm[i].checked){
				arrDefValues[count] = trim(frm[i].value);
			} else {//ADDED GGP20170329 for undefined frm[i].value. OR checking in trim method if 's' is undefined
				arrDefValues[count] = "";
			}
			count++;
			
		}
		//Added another condition to not include datepickers
		else if(frm[i].tagName.toLowerCase() == "input" && frm[i].type == "text" && (frm[i].id).substring((frm[i].id).length - 6).toLowerCase() != "hidden"){	
			arrDefValues[count] = trim(frm[i].value);
			count++;
		}
		else if(frm[i].tagName.toLowerCase() == "input" && frm[i].type == "password"){	
			arrDefValues[count] = trim(frm[i].value);
			count++;
		}
		
		//Added for text area
		else if(frm[i].tagName.toLowerCase() == "textarea"){	
			arrDefValues[count] = trim(frm[i].value);
			count++;
		}
		
	}
	return arrDefValues;
}

//GGP20161208
function haveChanges2(arrDef, arrNew){
	for(var i=0;i<arrDef.length;i++){
		if(trim(arrDef[i]) != trim(arrNew[i]))
			return true;
	}
	return false;
}

//NBM20161208
function checkForSpecialChars(frm){
	var regexpSen = /[a-zA-Z0-9\.\,\'\-\?\!\@\_]+$/;
	var regexpCode = /[a-zA-Z0-9]+$/;
	var regexpId = /[a-zA-Z0-9\_\-]+$/;
	var regexp;
	var msg = "";
	var fieldValue;
	var specialCharMessage = " contains invalid character(s).";
	for(i=0;i<frm.length;i++){
		
		if(frm[i].getAttribute("fieldName")!=null && frm[i].getAttribute("fieldName")!='' && frm[i].value!='')
		{
		if((frm[i].tagName.toLowerCase()=="input" && frm[i].type.toLowerCase()!="button")|| frm[i].tagName.toLowerCase()=="textarea" ){					
			
			fieldValue = frm[i].tagName.toLowerCase()=="input" ? frm[i].value : frm[i].value;
			if(frm[i].getAttribute("fieldName").includes("Code")){
				regexp=regexpCode;
			}else if(frm[i].getAttribute("fieldName").includes("Id")){
				regexp=regexpId;
			}else{
				regexp=regexpSen;
			}
			if(!trim(fieldValue).match(regexp)){
				if(frm[i].getAttribute("fieldName") != null && msg.indexOf(frm[i].getAttribute("fieldName")+specialCharMessage) == -1) {
					if (document.getElementById("txnBean") != null){ 
						msg += "<br/>" + frm[i].getAttribute("fieldName")+specialCharMessage;
					}
					if (manageMessage(frm[i].name, frm[i].getAttribute("fieldName")+specialCharMessage)){
						return false;
					}
				}else if (msg.indexOf(frm[i].getAttribute("fieldName")+specialCharMessage) == -1) {
					if (document.getElementById("txnBean") != null){
						msg += "<br/>" + frm[i].name+specialCharMessage;
					}
					if (manageMessage(frm[i].name, frm[i].name +specialCharMessage)){
						return false;
					}
				}
			}
		} 
	}// if
	}
	return true;
}

function isTooSimplePassword2(usr_id, password){
	var regexp = new RegExp('^\\w*(?=\\w*\\d)(?=\\w*[a-zA-Z])\\w*$');
	var regexp2 = new RegExp('^[\\d]*$');
	var regexp4 = new RegExp('.*' + trim(usr_id.toLowerCase()) + '.*');
	var regexp3 = new RegExp('.*' + trim(password.toLowerCase()) + '.*');
		
		if (usr_id.toLowerCase() == password.toLowerCase() || !checkConRep(password) ||
			usr_id.toLowerCase().match(regexp3) ||
			password.toLowerCase().match(regexp4) ||
			password.match(regexp2))
			return "true";
		if(!password.match(regexp))
			return "reg";
		return "false";
}

function checkConRep(password){
	var pwLen = password.length
	var pattern = null;
	var notConsecutive = false;
	var notReplicated = false;
	var ctr=0;
	for (var i=1; i<pwLen; i++){
		notReplicated = password.charAt(i) != password.charAt(i-1);
		if (!notReplicated) 
			break;
		if (ctr == 0) {
			if (password.charAt(i).charCodeAt(0) - password.charAt(i-1).charCodeAt(0) > 0)
				pattern = 'inc';
			else if (password.charAt(i).charCodeAt(0) - password.charAt(i-1).charCodeAt(0) < 0)
				pattern = 'dec';			
		}
		
		notConsecutive = (pattern == 'inc' && (password.charAt(i).charCodeAt(0) - password.charAt(i-1).charCodeAt(0)  != 1)) ||
					 	(pattern == 'dec' && (password.charAt(i).charCodeAt(0) - password.charAt(i-1).charCodeAt(0) != -1));
			
		if (notConsecutive)	
			ctr = 0;
		else {
			ctr++;
			if (ctr == 2) 
				break;
			else if (i == pwLen-1) 
				notConsecutive = true;
		}
	}
	return notConsecutive && notReplicated;
}

//NBM20161209
function submitTableFromTH(object) {
	var numrecords = document.getElementById("numrecords").innerHTML;
	if (numrecords > 0) {
	
	var parentObject = object.parentNode;
	while(parentObject.tagName && parentObject.tagName != "form" && parentObject.tagName != "FORM") {
	parentObject = parentObject.parentNode;
	}
	if (parentObject.tagName == "form" || parentObject.tagName == "FORM")
		parentObject.submit();
	}
}

$(document).ready(function() {
	
	$('input[type="password"]').bind('copy paste',function(e){
		e.preventDefault();
	});
	
	document.addEventListener('contextmenu', function(e){
		e.preventDefault();
	});
	
	if( document.getElementById('teraTableTd_NoRecordsRetrived') != null)
		$('[class*="table-links"]').find('button').hide();
	
	$('select').each(function(){
		var sel = $(this);
		
		$(this).after('<i class="glyphicon glyphicon-menu-down"></i>');
			
		var nextElement = $(this).next().next().attr('class');
		if(nextElement != undefined && nextElement.indexOf("input-group-btn") >= 0)
			$(this).next('.glyphicon-menu-down').css({ "position": "absolute","right": "50px"});
	
		$(this).next('.glyphicon-menu-down').on('click',function(){
			$(this).prev('select').click();
		});
	});
	
	// for custom dropdown
	$('.select').each(function(){
		//$(this).find('input[type="text"]').attr('readonly', true);
		$(this).find('input[type="text"]').attr('placeholder', $(this).find('.list-group-item:eq(0)').text());
		$(this).find('input[type="text"]').after('<i class="glyphicon glyphicon-menu-down"></i>');

		$(this).find('.list-group-item').wrapAll('<ul class="list-group" />');
		
		if($(this).find('input[type="hidden"]').val() != undefined){
			var value = $(this).find('input[type="hidden"]').val().replace(/'/g, "\\'");
			if(value != ""){
				var selected = $(this).find(".list-group-item[data-value='"+value+"']").addClass('liHover'); //changed to data-value for IE11 compatibility
//				var label = $(this).find(".list-group-item[value='"+value+"']").addClass('liHover').text();
				$(this).find('input[type="text"]').val(selected.text());
				scrollTo(selected, $(this).find('.list-group'), false);
			}
		}
		

	});
		
	$('.edit-select, .filter-select, .select-filter, .select-edit').each(function(){
		$(this).find('input[type="text"]').attr('readonly', false);
	});

	
	var indMouseDown = false;
	$(document).on('mouseover', '.select .list-group-item:not(.list-group-item-heading)', function(){
		$(this).siblings('.list-group-item').removeClass('liHover');
		$(this).addClass('liHover');
	}).on('mouseout', '.select .list-group-item:not(.list-group-item-heading)', function(){
		$(this).removeClass('liHover')
	});
	
	$(document).on('click', '.select .glyphicon, .select .fa, .select input[type="text"]', function(){
		
		if ($(this).parents('.modal-body').length > 0) {
			var liH = $(this).parent().find('.list-group').outerHeight();
			var contH = $('.form-group').outerHeight() * $('.modal').find('.form-group:visible').length
			
			if(liH >= contH){
				$('.modal-body').css('overflowY','visible');
			}
		}
		$(this).parent().find('.list-group, .list-group-item').show();
		//20171127LEL: hide filtered items
		$(this).parent().find('.filtered').hide();
		$(this).parent().find('input[type="text"]').focus();
		
		setDropdownValue($(this));
	});
	
	$(document).on('mousedown', '.select .list-group-item:not(.list-group-item-heading)', function(){
		indMouseDown = true
	}).on('click', '.select .list-group-item:not(.list-group-item-heading)', function(){
		$(this).parent().siblings('input[type=text]').val($(this).text());
		$(this).parent().siblings('input[type=hidden]').val($(this).attr('data-value')).trigger('change');//changed to data-value for IE11 compatibility
		$(this).addClass('liHover');
		$(this).parent().hide();
	});
	
	$(document).mouseup(function(e){
	    var container = $('.select .list-group');

	    // if the target of the click isn't the container nor a descendant of the container
	    if (!container.is(e.target) && container.has(e.target).length === 0) 
	    {
	        container.hide();
	        container.unbind( 'click', document );
	    }
	});

	$(document).on('keyup','.select input[type="text"]', function(e){
		var listGroup = $(this).siblings('.list-group');
		var list = $(this).siblings('.list-group').find('.list-group-item:visible').not('.filtered');
		
		listGroup.show();
		
		var $hlight = listGroup.find('.list-group-item.liHover');
		scrollTo($hlight, listGroup, false);

		//do not include filtered elements
		if (e.keyCode == 40) {
			if($hlight==null)
				list.eq(0).addClass('liHover');
			if ($hlight.nextAll(':visible').first().hasClass('list-group-item-heading'))
				$hlight.removeClass('liHover').next().next(':visible').addClass('liHover');	
			else
				$hlight.removeClass('liHover').nextAll(':visible').first().addClass('liHover');	
			
			if ($hlight.nextAll(':visible').not('.filtered').first().length <= 0)
				list.eq(0).addClass('liHover');
	    } else if (e.keyCode == 38) {
	    	console.log($hlight.prevAll(':visible').first().length);
			if($hlight==null)
				list.eq(0).addClass('liHover');
			if ($hlight.prevAll(':visible').first().hasClass('list-group-item-heading'))
				$hlight.removeClass('liHover').prev().prev(':visible').addClass('liHover');	
			else
				$hlight.removeClass('liHover').prevAll(':visible').first().addClass('liHover');
			
			if ($hlight.prevAll(':visible').first().length <= 0)
				list.eq(list.length-1).addClass('liHover');
	    }
//		var index = list.index($('.liHover'));
//		var top = index*list.outerHeight();
/*		if(listGroup.find('.list-group-item.liHover').length == 0){
			list.eq(0).addClass('liHover');
		}
		if(listGroup.find('.list-group-item.liHover').offset() == undefined){
			top = 0;
		}*/
		
		if (e.keyCode == 40 || e.keyCode == 38){
			var selected = listGroup.find('.list-group-item.liHover');
			scrollTo(selected, listGroup, true);
		}
		
		if(e.keyCode == 13){
		    if(list.index($('.liHover'))==0 || $hlight.hasClass('list-group-item-heading')){
				$(this).val('');
				$(this).siblings('input[type="hidden"]').val('').trigger('change');
			}
			else{
				$(this).val($($hlight).text());
				$(this).siblings('input[type="hidden"]').val($($hlight).attr('data-value')).trigger('change');
			}
			$(this).siblings('.list-group').hide();
//			$hlight.removeClass('liHover');
		}
		
		else if (e.keyCode == 27){
			$(this).siblings('.list-group').hide();
//			$hlight.removeClass('liHover');
		}	

		if(e.keyCode==9){
			//add liHover class and scroll to selected value, adjust dropdown position
			setDropdownValue($(this));
		}
	});
	
	$(document).on('keyup','.select input[type="text"]', function(e){
		var textVal = $(this).val().toLowerCase();
		var list = $(this).siblings('.list-group').find('.list-group-item').not('.filtered');
		$(this).next('.list-group').show();

		$(list).hide();
		$(list).each(function(){
			if($(this).text().toLowerCase().indexOf(textVal) != -1){
				$(this).show();	
			}
		})
		
		$(list).eq(0).show();
		ddOptionPos();
	});
	
	$(document).on('blur', '.select input[type="text"]', function(){
		var selectInput = $(this);
		if(indMouseDown){
			indMouseDown = false;
			}
		else{	
			//$(this).parent().find('.list-group').hide();
		}
		
		if(!$(this).parent().hasClass('edit-select')){
			if($(this).parent().find('li.list-group-item').filter(function(){
				return $(this).text() == selectInput.val();
				}).index() < 0){
				selectInput.val('');
			}
		}
	});
	
	$(document).on('click', '.select .list-group-item:first-child', function(){
		$(this).parent().siblings('input[type=text]').val('');
		$(this).parent().siblings('input[type=hidden]').val('').trigger('change');
		$(this).parent().hide();
	});	
	
	$(".input-group .form-control:disabled").siblings('.input-group-btn').find('.btn').attr('disabled',true);
	$(".checkbox input:disabled, .radio input:disabled").parent('label').css('cursor','not-allowed');
	
	$(".input-group .form-control:disabled").siblings('.input-group-btn').find('.btn').attr('disabled',true);
	$(".checkbox input:disabled, .radio input:disabled").parent('label').css('cursor','not-allowed');
	
	$(window).on('scroll', function(){
		ddOptionPos();
	});
	// end 

});

//add liHover class and scroll to selected value, adjust dropdown position
function setDropdownValue(element) {
	//added - remove liHover before setting new liHover
	element.siblings('.list-group').find('.liHover').removeClass('liHover');
	
	var value = element.parent().find('input[type="hidden"]').val().replace(/'/g, "\\'");
	if(value != ""){
		element.parent().find(".list-group-item[data-value='"+value+"']").addClass('liHover');//changed to data-value for IE11 compatibility
	} else {
		element.siblings('.list-group').find('.list-group-item').eq(0).addClass('liHover');
	}
	
	scrollTo(element.parent().find('.list-group').find('.list-group-item.liHover'), element.parent().find('.list-group'), false);
	ddOptionPos();
}

/* 20150609 MHL compare date with time. assumed datetime is valid and datetime format is MM-DD-YYYY hh:mm:ss*/
function compareDateTimeEntry(str,str1)
{
	try
	{
		str = trim(str);
		str1 = trim(str1);

		var temp1=str.substring(6,10)+str.substring(0,2)+str.substring(3,5)
					+ str.substring( 11, 13 ) + str.substring( 14 , 16 )  + str.substring( 17 , 19 );
		var temp2=str1.substring(6,10)+str1.substring(0,2)+str1.substring(3,5)
					+ str1.substring( 11, 13 ) + str1.substring( 14 , 16 )  + str1.substring( 17 , 19 );

	 	if(temp1>temp2)
	 		return true;
	 	else if(temp1==temp2)
	 		return true;
	 	else
	 		return false;
	}
	catch(exp){
	}
}

//added KVO 2013-07-17
//takes in 2 strings date1 and date2 and their proper java date format
//returns 1 if date1 is greater than date2
//returns -1 if date1 is less than date2
//returns 0 if date1 is equal to date2
//default date format is 'mm-dd-yyyy' if parameter 'dateFormat' is null
function compareDateTime( sdate1, sTime1, sdate2, sTime2, dateFormat ){
	var dateArr1 = sdate1.split("/");
	var dateArr2 = sdate2.split("/");
	
	var date2 = new Date();
	date2.setFullYear(parseInt(dateArr2[2],10), parseInt(dateArr2[0],10) > 0 ? parseInt(dateArr2[0],10) - 1 : parseInt(dateArr2[0],10), parseInt(dateArr2[1],10));
	
	if(sTime2 != '')
		date2 = set24HourTime(date2, sTime2);
	
	var date1 = new Date();
	date1.setFullYear(parseInt(dateArr1[2],10), parseInt(dateArr1[0],10) > 0 ? parseInt(dateArr1[0],10) - 1 : parseInt(dateArr1[0],10), parseInt(dateArr1[1],10));
	
	if(sTime1 != '')
		date1 = set24HourTime(date1, sTime1);
	
	return date1.compareTo(date2);
}

function set24HourTime( sDate, sTime ){
	if(sTime.indexOf(" ") > -1){
		var arr = sTime.substring(0, sTime.indexOf(" ")).split(":");
		
		if( sTime.toUpperCase().indexOf("P") > -1 ){
			sDate.setHours(Number(arr[0]) + 12);
		}
		else{
			sDate.setHours(Number(arr[0]));
		}
		
		sDate.setMinutes(Number(arr[1]));
		
		if(arr.length > 2)
			sDate.setSeconds(Number(arr[2]));
	}
	else{
		var arr = sTime.split(":");
		sDate.setHours(Number(arr[0]));
		sDate.setMinutes(Number(arr[1]));
		
		if(arr.length > 2)
			sDate.setSeconds(Number(arr[2]));
	}
	
	return sDate;
}

function showDatepicker(formId)
{
	var hiddenInput = "<input type='input' class='form-control' id='"+formId+"hidden' style='display:block' value='"+document.getElementById(formId).value+"'/>";
	if($('[id='+formId+'hidden]').length > 0)
		hiddenInput = "";
	var formInput = document.getElementById(formId).parentNode.innerHTML;
	document.getElementById(formId).parentNode.innerHTML = hiddenInput + formInput
	document.getElementById(formId).value = document.getElementById(formId+"hidden").value;
	
	var datefrom=$('input[id="'+formId+'hidden"]'); 
	var container=$('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
	var options={
		dateFormat: 'mm/dd/yy',
		container: container,
		todayHighlight: true,
		onClose: function() {
			//changed from "document.getElementById().remove()" to jQuery because IE11 does not support remove

			$('#' +formId + "hidden").remove();
		},
		onSelect: function() {
			document.getElementById(formId).value = document.getElementById(formId+"hidden").value;
		}
		//autoclose: true,
	};
	datefrom.datepicker(options);
	$('#'+formId+'hidden').show().focus().hide();
}

// formId indicates what input element the datepicker will appear
// this function accepts string date with format 'mm-dd-yyyy'
// example: showDatepickerWithDateRange(formId, '01-01-16', null) Jan 01, 2016 start date onwards
// example: showDatepickerWithDateRange(formId, null, '01-01-16') Jan 01, 2016 end date backwards
// example: showDatepickerWithDateRange(formId, document.getElementById(formId).value, document.getElementById(formId).value) uses the value of target form element
/*function showDatepickerWithDateRange(formId, dateFrom, dateTo) 
{

	var hiddenInput = "<input type='input' class='form-control' id='"+formId+"hidden' style='display:block' value='"+document.getElementById(formId).value+"'/>";
	if($('[id='+formId+'hidden]').length > 0)
		hiddenInput = "";
	var formInput = document.getElementById(formId).parentNode.innerHTML;
	document.getElementById(formId).parentNode.innerHTML = hiddenInput + formInput
	document.getElementById(formId).value = document.getElementById(formId+"hidden").value;
	
	var datefrom=$('input[id="'+formId+'hidden"]'); 
	var container=$('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
	var options={
		dateFormat: 'mm/dd/yy',
		container: container,
		todayHighlight: true,
		minDate: dateFrom,
		maxDate: dateTo,
		changeMonth: true,
		changeYear: true,
		yearRange: "-100:+100", // last hundred years
		onClose: function() {
			//changed from "document.getElementById().remove()" to jQuery because IE11 does not support remove

			$('#' +formId + "hidden").remove();
			
		},
		onSelect: function() {
			document.getElementById(formId).value = document.getElementById(formId+"hidden").value;
			$("#"+formId).val(document.getElementById(formId).value);
			$("#"+formId).trigger('change');
		}
		//autoclose: true,
	}
	
	datefrom.datepicker(options);
	$('#'+formId+'hidden').show().focus().hide();
}*/


//formId indicates what input element the datepicker will appear
//this function accepts string date with format 'mm-yyyy'
//example: showDatepickerWithDateRange(formId, '01-16', null) Jan 2016 start date onwards
//example: showDatepickerWithDateRange(formId, null, '01-16') Jan 2016 end date backwards
//example: showDatepickerWithDateRange(formId, document.getElementById(formId).value, document.getElementById(formId).value) uses the value of target form element
/*function showDatepickerWithDateRangeMonthYear(formId, dateFrom, dateTo, format) 
{
	if(format == '')
		format = 'mm/yy';
	
	var changeYear = true;
	var changeMonth = true;
		
	var hiddenInput = "<input type='input' class='form-control' id='"+formId+"hidden' style='display:block' value='"+document.getElementById(formId).value+"'/>";
	if($('[id='+formId+'hidden]').length > 0)
		hiddenInput = "";
	var formInput = document.getElementById(formId).parentNode.innerHTML;
	document.getElementById(formId).parentNode.innerHTML = hiddenInput + formInput
	document.getElementById(formId).value = document.getElementById(formId+"hidden").value;
	
	var datefrom=$('input[id="'+formId+'hidden"]'); 
	var container=$('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
	var options={
		dateFormat: format,
		container: container,
		todayHighlight: true,
		changeMonth: changeMonth,
		changeYear: changeYear,
		minDate: dateFrom,
		maxDate: dateTo,
		yearRange: "-100:+0", // last hundred years
		onClose: function() {
			//changed from "document.getElementById().remove()" to jQuery because IE11 does not support remove

			$('#' +formId + "hidden").remove();
		},
		onSelect: function() {
			document.getElementById(formId).value = document.getElementById(formId+"hidden").value;
			$("#"+formId).val(document.getElementById(formId).value);
			$("#"+formId).trigger('change');
		}
		//autoclose: true,
	}
	
	datefrom.datepicker(options);
	$('#'+formId+'hidden').show().focus().hide();
}*/


function filterDatepicker(form, event)
{
	var key = event.keyCode || event.charCode;
	if( key == 8 )// backspace 
		form.select();
	else if( key == 46)// delete 
		form.value = "";
	else
		event.preventDefault();
}

function setChangedListModal(oldList, newList, addedLabel, removedLabel){

	var added = [];
	var removed = [];
	
	// find added/remove elements
	for(var i=0;i<oldList.length;i++){
		if(newList.indexOf(oldList[i]) == -1)
			removed[removed.length] =  oldList[i];
	}
	for(var i=0;i<newList.length;i++){
		if(oldList.indexOf(newList[i]) == -1)
			added[added.length] =  newList[i];
	}

	
	// set list to modal
	$('body').find('div#changeListModal').remove();
		$('body').find('form:last').append('<div id ="changeListModal"> </div>');

	var content = "";
	for(i=0; i<added.length; i++){
	    content += '<input fieldName="'+addedLabel+'" style="display:none" value="'+  added[i] + '"/>';
	}
	for(i=0; i<removed.length; i++){
	    content += '<input fieldName="'+removedLabel+'" style="display:none" value="'+  removed[i] + '"/>';
	}
	
	$('div#changeListModal').append(content);
	
}

function setDropdownViewValue(){
	$('.select').each(function(){
		var value = $(this).find('input[type="hidden"]').val();
		if(value != ""){
			var selected = $(this).find(".list-group-item[data-value='"+value+"']").addClass('liHover');//changed to data-value for IE11 compatibility
			$(this).find('input[type="text"]').val(selected.text());
		}
	});
}

function scrollTo(item, listGroup, animate){
	if(item.length > 0){
		var top = item.offset().top - listGroup.offset().top + listGroup.scrollTop();
		if(top >= listGroup.innerHeight()-30){
			if(animate)
				listGroup.animate({
					scrollTop: top
				});
			else
				listGroup.scrollTop(top);
		}
		else if(top < listGroup.innerHeight()-30){
			if(animate)
				listGroup.animate({
					scrollTop: 0
	});
			else
				listGroup.scrollTop(0);
		}
	}
}

function ddOptionPos(){
	vportW = $(window).innerWidth();
	vportH = $(window).innerHeight();
	
	$('.select').each(function(){
		var listTop = $(this).offset().top - $(window).scrollTop(); 
		var lgH = $(this).find('.list-group').innerHeight();
		
		if(listTop >= vportH/2){	
			$(this).find('.list-group').css({
				'top': -lgH,
				'borderTop': '1px solid #CCC',
				'borderBottom': 'none',
				'borderRadius': '4px 4px 0 0',
				'paddingTop': 0
			});
			
			$(this).find('.list-group-item:eq(0)').css({
				'borderTop': 'none'
			})
		}
		else{
			$(this).find('.list-group').css({
				'top': '25px',
				'borderTop': 'none',
				'borderBottom': '1px solid #CCC',
				'borderRadius': '0 0 4px 4px',
				'paddingTop': '5px'
			});
			$(this).find('.list-group-item:eq(0)').css({
				'borderTop': '1px dashed #e4e4e4;'
			})
		}
		
		if(lgH >= vportH/2){
			$(this).find('.list-group').css({
				'height': vportH/3 +'px',
			})
		}
		
	});
}	
//For teratable function
function keyRestrict(e, validchars)
{
	var key = '';
	var keychar = '';
	key = getkey(e);
	if (key == null)
		return true;
	keychar = String.fromCharCode(key);
	keychar = keychar.toLowerCase();
	validchars = validchars.toLowerCase();
	if (validchars.indexOf(keychar) != -1)
		return true;
	if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
		return true;
	return false;
}

function getkey(e){
	if (window.event)
	   return window.event.keyCode;
	else if (e)
	   return e.which;
	else
	   return null;
}
function checkState(myWin)    {
    if(document.readyState=="complete") {
    	myWin.close(); 
    } else {           
        setTimeout("checkState(myWin)", 1000);
    }
}

function printPage(printAreaID, print){
	var printPage = $('#'+printAreaID).clone();
	//printPage.find('.alert').remove()
	printPage.find('.button-holder').remove();
	printPage.find('div.col-sm-10').removeClass('col-sm-10').addClass('col-sm-12');
	printPage = printPage.html();
	
	if(print){
		var myWin = window.open("Print","_blank","height=1000px,width=600px,scrollbars=1,resizable=0,titlebar=0");   
		printPage = '<!doctype html><meta name="viewport" content="width=device-width, initial-scale=1"><head>'+
		'<link rel="stylesheet" href="resources/css/bootstrap.min.css">'+
		'<link rel="stylesheet" href="resources/css/normalize.css">'+
		'<link rel="stylesheet" href="resources/css/web.css">'+
		//'<link rel="stylesheet" href="resources/css/mobile.css">'+  //mobile printing is not yet supported; pending for update
		'<link rel="stylesheet" href="resources/css/jquery-ui.min.css">'+
		'<link rel="stylesheet" href="resources/css/print.css">'+
		'<script type="text/javascript" src="resources/js/jquery-1.12.1.min.js"></script'+
		'><script type="text/javascript" src="resources/js/bootstrap.min.js"></script'+
		'></head><body><div id="app-header"><div id="app-brand"><img src="resources/css/images/app.png" />'+
		'</div></div><div id="app-body"><div class="col-sm-12">' + printPage + '</div></div></body></html>';
				
		myWin.document.write(printPage);
		window.setTimeout(function () {
			myWin.print();
			checkState(myWin);
	    }, 1000);
		myWin.document.close();
	}
	
	return printPage;
}

function savePage(){
	var css = []
	var browser = navigator.userAgent.search("MSIE") > -1  && navigator.appVersion.indexOf("MSIE 7.") != -1 ? "ie7" : "not ie7";
	
	$("html").parent().find('link').each(function(){
		var cssFile = $(this).attr('href');
		
		if(cssFile.indexOf("web.css") > -1){
			cssFile = cssFile.replace('web.css','web_print.css');
			//css.push(cssFile);
		}else if(cssFile.indexOf("bootstrap.min.css") > -1){
			cssFile = cssFile.replace('bootstrap.min.css','bootstrap_print.css');
			//css.push(cssFile);
		}
		
	});
	
	css.push('resources/css/print.css');
	$('#download1').val('');
	
	var saveDiv = printPage('printDiv',false);
	
	$(saveDiv).find('.form-group > label.control-label').each(function(){
		if($.trim($(this).text()) == '')
			$(this).text('|s|');
	});
	
	$('#download1').val(saveDiv/*.replace(/col-sm/g,"col-xs")*/);
	$('#browser1').val(browser);
 	$('#cssSrc1').val(css);
	$('#proceed').click();	
}

function htmlEscape(str) {
	return str.replace(/&#039;/g, "\'").replace(/&amp;/g, "&").replace(/&#034;/g, "\"").replace(/&lt;br \/&gt;/g, "\n");
}

function hasNewValue(field){
	var formInput = $('#'+field);
	
	if(formInput.parent().hasClass('select'))
		formInput = formInput.parent();
	
	formInput.wrap('<div class="input-group"></div>');
	formInput.before('<span class="input-group-addon old-val">Old</span>');
	formInput.parents('div.form-group').addClass('has-changes');
	$('.'+field).show();
}

function checkValueOnDropdown(id) {
	var list = $("#"+id).siblings('.list-group').find('.list-group-item');
	var same = false;
	for(var ctr=1;ctr<list.length;ctr++){
		var optionValue = list.eq(ctr).text();
		if(optionValue != '' && optionValue == $("#"+id+"_view").val()) {
			same = true;
		}
	}
	return same;
}

//extracts code from code | description
function extractCode(str) {
	var pipeIndex = str.indexOf('|');
	if(pipeIndex > 0) {
		str = $.trim(str.substring(0, pipeIndex));
	}
	return str;
}

//Table and Modal jsp binding
function modalBinder(table, modal, modalBean, tableArray, listBean, enabledColumnsInAppForm, filterParameters) {
	
	//if there is an editable appform
	var isAppFormRowEditable = (enabledColumnsInAppForm != undefined && enabledColumnsInAppForm.length > 0);
	
	//store onload value of modal in form-group(for refresh/reset modal function)
	$('#' + modal + ' .modal-body input, #' + modal + ' .modal-body textarea').each(function() {
		$(this).closest('div.form-group').data('refresh', $(this).closest('div.form-group').html());
	});
	
	//store number of rows in the table(for modal server error functions)
	$('#' + table + ' table').data('rowLength', $('#' + table + ' table tbody tr').length);
	
	//add loanRecord class for record in loan app forms
	$('#' + table + ' .requestInd').each(function() {
		if($(this).text().trim() == 'Y')
			$(this).closest('tr').addClass('loanRecord');
	});
	
	//Hide deleted rows when going back to page in case of error
	$('#' + table + ' .deleteInd').each(function() {
		if($(this).text() == 'Y') {
			$(this).parent().hide();
		}
	});
	
	$('#' + table + ' .errorInd').each(function() {
		if($(this).text() == 'Y') {
			$(this).parent().addClass('alert-danger');
		}
	});
	
	$('#' + table + ' .addTable').click(function() {
		clearAllErrors(modal);
		
		if(!$('#' + modal).hasClass('in'))
			$('#' + modal).modal('toggle');
			
		refreshModal(modal, filterParameters);
		
		$('#' + modal + ' .addModal').show();
		$('#' + modal).find('div.modal-body').animate({
	        scrollTop:0
	    }, 500);
		
		//refresh dropdown values
		if(filterParameters != undefined) {
			for(var i in filterParameters) {
				filterDropdown('', filterParameters[i].childID, filterParameters[i].isChildMandatory, modal);
			}
		}
		
		$('#' + modal + ' .modifyModal').hide();
	});
	var tableErrorTime;
	
	$('#' + table + ' .modifyTable').click(function() {
		clearAllErrors(modal);
		$(document).find('div#tableError').remove();
		clearTimeout(tableErrorTime);
		var selectedVisible = $('#' + table + ' .selected').filter(':visible');
		if(!selectedVisible.length || $('#' + table + ' .selected').attr('id') == 'noRecord') {
			
			$(this).closest('div.section-block').before('<div id="tableError" class="alert alert-danger">Please select an entry to modify</div>');
			tableErrorTime = setTimeout(function(){
				$(document).find('div#tableError').hide();
				$(document).find('div#tableError').remove();
			}, 4000); 
			return false;
		}

		if($('#' + table + ' .selected .requestInd').text().trim() == 'Y' && !isAppFormRowEditable) {
			$(this).closest('div.section-block').before('<div id="tableError" class="alert alert-danger">Current selected entry requires validation as indicated in the request. Modification of record is not allowed.</div>');
			tableErrorTime = setTimeout(function(){
				$(document).find('div#tableError').hide();
				$(document).find('div#tableError').remove();
			}, 4000);
			return false;
		}
		
		refreshModal(modal, filterParameters);
		
		$('#' + table + ' .selected').find('td').each(function() {
			var className = $(this).attr("class") + '';
			var classNameCopy = className;
			//Remove Extra class names, add as necessary.
			className = $.trim(className.replace("hidden", ""));
			className = $.trim(className.replace("amount", ""));
			className = $.trim(className.replace("radiobutton", ""));
			var value = $(this).text();

			var selector = classNameCopy.indexOf('radiobutton') < 0 ? 'id' : 'name';
			if(isAppFormRowEditable) {
				//get index of className
				var columnIndex = -1;
				for(var i in enabledColumnsInAppForm) {
					if(enabledColumnsInAppForm[i].column == className) {
						columnIndex = i;
					}
				} 
				
				//if selected is from app form
				if($('#' + table + ' .selected .requestInd').text().trim() == 'Y') {
					//if className is in enabledColumns
					if(columnIndex > -1) {
						if(enabledColumnsInAppForm[columnIndex].mandatory) {
							//add mandatory
							$('#' + modal + ' [' + selector + '$="' + className + '"]').closest("div.form-group").find('label').first().addClass('mandatory');
							$('#' + modal + ' [id$="' + className + '"]').attr("mandatory", "true");
						} else {
							//remove mandatory
							$('#' + modal + ' [' + selector + '$="' + className + '"]').closest("div.form-group").find('label').first().removeClass('mandatory');
							$('#' + modal + ' [id$="' + className + '"]').attr("mandatory", "false");
						}
					} else {
						//disable the field
						$('#' + modal + ' [' + selector + '$="' + className + '"]').closest("div.form-group").find('label').first().removeClass('mandatory');
						$('#' + modal + ' [' + selector + '$="' + className + '"]').attr("disabled", true);
						
						//remove counter class if textarea
						if($('#' + modal + ' [' + selector + '$="' + className + '"]').is('textarea') && $('#' + modal + ' [' + selector + '$="' + className + '"]').next().hasClass('counter')) {
							$('#' + modal + ' [' + selector + '$="' + className + '"]').next().remove();
						}
					}
				} 
			}
			
			if(classNameCopy.indexOf('radiobutton') < 0) {
				$('#' + modal + ' [id$="' + className + '"]').val(value);
			} else {
				//for radio fields
				$('#' + modal + ' [name$="' + className + '"]').each(function(){
					if(value == $(this).val()) {
						$(this).prop('checked', true);
					}
				});
			}
		});
		
		//Set Checkbox to checked if value is 'Y'
		$('#' + modal + ' .modal-body :checkbox').each(function() {
			if($(this).val() == 'Y') {
				$(this).prop('checked', true);
			} 
		});
		
		if(!$('#' + modal).hasClass('in'))
			$('#' + modal).modal('toggle');
		
		$('#' + modal + ' .addModal').hide();
		$('#' + modal + ' .modifyModal').show();

		$('#' + modal).find('div.modal-body').animate({
	        scrollTop:0
	    }, 500);
		
		if(tableArray == undefined)
			tableArray = [table];
		
		var index = getSelectedIndex(table, tableArray);
		var errorFields = getErrorFields(index, listBean);
		
		$('#' + modal).on('shown.bs.modal', function (e) {
			if($(this).find('.modifyModal').is(':visible')) {
				addServerFieldError(modalBean, errorFields);
				$(this).find('textarea').each(function(e){
					$(this).keyup();
				});
				
				$('#' + modal).off('shown.bs.modal');
			}
		});

		//refresh dropdown values
		if(filterParameters != undefined) {
			for(var i in filterParameters) {
				filterDropdown($('#' +modal).find('#' + filterParameters[i].parentID).val(), filterParameters[i].childID, filterParameters[i].isChildMandatory, modal);
			}
		}
	});
	
	$('#' + table + ' .deleteTable').click(function() {
		$(document).find('div#tableError').remove();
		clearTimeout(tableErrorTime);
		var selectedVisible = $('#' + table + ' .selected').filter(':visible');
		if(!selectedVisible.length || $('#' + table + ' .selected').attr('id') == 'noRecord') {
			
			$(this).closest('div.section-block').before('<div id="tableError" class="alert alert-danger">Please select an entry to delete</div>');
			tableErrorTime = setTimeout(function(){
				$(document).find('div#tableError').hide();
				$(document).find('div#tableError').remove();
			}, 4000);
			return false;
		}
		
		if($('#' + table + ' .selected .requestInd').text() != 'Y') {
			$('#' + table + ' .selected').hide();
			checkRecord(table);
		}else{
			$(this).closest('div.section-block').before('<div id="tableError" class="alert alert-danger">Current selected entry requires validation as indicated in the request. Deletion of record is not allowed.</div>');
			tableErrorTime = setTimeout(function(){
				$(document).find('div#tableError').hide();
				$(document).find('div#tableError').remove();
			}, 4000);
		}
	});
	
	$('#' + modal + ' .addModal').click(function() {
		if(checkMandatory(modal) && checkInvalidChar(modal) && checkInvalidDate(modal)) {
			var row = getRowFromModal(table, modal, modalBean);

			$('#' + table + ' table > tbody').append(row);
			$('#' + table + ' table tbody tr:last').data('row', 'added');
			$('#' + modal).modal('hide');
		}
		checkRecord(table);
	}); 
	
	$('#' + modal + ' .modifyModal').click(function() {
		if(checkMandatory(modal) && checkInvalidChar(modal) && checkInvalidDate(modal)) {
			var row = getRowFromModal(table, modal, modalBean);

			$('#' + table + ' table > tbody .selected').replaceWith(row);
			$('#' + modal).modal('hide');
		}		
	});

	//add dropdown event for province-city like dropdowns - updating dropdown values
	if(filterParameters != undefined) {
		for(var i in filterParameters) {
			addDropdownEvent(filterParameters[i].parentID, filterParameters[i].childID, filterParameters[i].isChildMandatory, modal);
		}
	}
	
	//checkRecord upon load
	checkRecord(table);
}

function refreshModal(modal, filterParameters) {
	/**reset fields to its onload equivalent**/
	$('#' + modal + ' .modal-body input, #' + modal + ' .modal-body textarea').each(function() {
		$(this).closest('div.form-group').html($(this).closest('div.form-group').data('refresh'));
	});
	//add textarea event
	$('#' + modal + ' .modal-body textarea').each(function() {
		addTextareaEvent($(this));
	});
	//add checkbox event
	$('#' + modal + ' .modal-body [type="checkbox"]').click(function() {
		if ($(this).is(':checked')) {
			$(this).attr("value", "Y");
		} else {
			$(this).attr("value", "N");
		}
	});
	//add dropdown event
	if(filterParameters != undefined) {
		for(var i in filterParameters) {
			addDropdownEvent(filterParameters[i].parentID, filterParameters[i].childID, filterParameters[i].isChildMandatory, modal);
		}
	}
	
	/**emptying all fields**/
	//empty all input text
	$('#' + modal + ' .modal-body input[type="text"]:enabled').each(function() {
		$(this).val("");
	});

	//update remaining characters
	$('#' + modal + ' .modal-body textarea:enabled').each(function(){
		$(this).val("");
		$(this).keyup();
	});
	
	//remove checked radiobutton
	$('#' + modal + ' .modal-body [type="radio"]').each(function() {
		$(this).prop('checked', false);
	});
	
	//uncheck all checkboxes
	$('#' + modal + ' .modal-body [type="checkbox"]').each(function() {
		$(this).prop('checked', false);
		$(this).val('N');
	});
}

function getRowFromModal(table, modal, modalBean) {
	var row;
	var isLoanRecord = false;
	
	$('#' + modal).find('input:visible, textarea:visible').each(function() {
		var style = '';
		var id = $(this).attr('id') + '';
		var type = $(this).attr('type') + '';
		if(id != '') {
			if(type != 'radio') {
				id = id.replace(modalBean + '.', '');
				//for amount
				if($(this).hasClass('amount'))
					id += ' amount';
				if($(this).hasClass('pre-wrap'))
					style = 'style="word-wrap:break-word; white-space: pre-wrap"';
				//check if record is from loan app form
				isLoanRecord = ((id == "requestInd" || id == "loanAppFormInd") && $(this).val() == "Y") ? true : isLoanRecord;
				
				row += '<td class="' + id + '"' + style + '>' + $(this).val() + '</td>';
			} else {
				//for radio fields
				var name = $(this).attr('name') + '';
				if(name != '') {
					var value = $('input[name="' + name + '"]:checked').val();
					value = (value == undefined) ? '' : value;
					name = name.replace(modalBean + '.', '');
					name += ' radiobutton';
					
					if(row.indexOf('<td class="' + name + '">') < 0) {
						row += '<td class="' + name + '">' + value + '</td>';
					}
				}
			}
		}
	});
	
	$('#' + modal).find('input:hidden').each(function() {
		var id = $(this).attr('id') + '';
		if(id != '') {
			id = id.replace(modalBean + '.', '');
			//check if record is from loan app form
			isLoanRecord = ((id == "requestInd" || id == "loanAppFormInd") && $(this).val() == "Y") ? true : isLoanRecord;
			
			row += '<td class="hidden ' + id + '">' + $(this).val() + '</td>';
		}
	});
	
	if(isLoanRecord)
		row = '<tr class="loanRecord">' + row + '</tr>';
	else
		row = '<tr>' + row + '</tr>';
		
	
	return row;
}

function beanBinder(table, listBean, columns) {
	var tableSelectors = '#' + table[0] + ' table > tbody > tr[id!=noRecord]';
	
	for(var i in table) {
		if(i != 0) {
			tableSelectors += ', #' + table[i++] + ' table > tbody > tr[id!=noRecord]';
		}
	}
	$(tableSelectors).each(function(i, e) {
		for(var c in columns) {
			$('#txnBean').append('<input type="hidden" name="' + listBean + '['+i+'].' + columns[c] + '" value="' + $(this).find('td.' + columns[c]).text() + '"/>');
		}
		
		if(!$(this).is(":visible") && $(this).css('display') == 'none') {
			$('#txnBean').append('<input type="hidden" name="' + listBean + '['+i+'].deleteInd" value="Y"/>');
		} else {
			$('#txnBean').append('<input type="hidden" name="' + listBean + '['+i+'].deleteInd" value="N"/>');
		}
	});
}

//add No Records Retrieved if table is empty else remove No Records Retrieved row
function checkRecord(tableId) {
	var row = $('#' + tableId + ' table > tbody > tr').filter(function(){
		if($(this).css('display') != 'none')
			return $(this);
	});
	
	if(row.filter('[id!=noRecord]').length == 0) {
		row.filter('[id=noRecord]').remove();
		var colSpan = $('#' + tableId + ' table > thead > tr > th').length;
		$('#'+tableId+' table > tbody').prepend('<tr id="noRecord"><td colspan="' + colSpan + '" align="center">No Records Retrieved</td></tr>');
	} else if(row.filter('[id!=noRecord]').length > 0) {
		row.filter('[id=noRecord]').remove();
	}
}

//get errorfields that contains the supplied index
function getErrorFields(index, listBean) {
	var errorFields = new Array();
	for(var i = 0; i < modalErrClass.length; i++) {
		var indexOfArrIndex = modalErrClass[i].indexOf(listBean + "[" + index + "]");
		var indexOfPeriod = modalErrClass[i].indexOf(".") + 1;
		if(indexOfArrIndex > -1) {
			errorFields.push(modalErrClass[i].substring(indexOfPeriod));
		}
	}
	return errorFields;
}

//get bean index of the selected row
function getSelectedIndex(table, tableArray) {
	var index = 0;
	for(var i = 0; i < tableArray.length; i++) {
		if(tableArray[i] == table) {
			index = index + $('#' + tableArray[i] + ' table tbody tr').index($('#' + tableArray[i] + ' .selected'));
			//if selected row is not on the bean/just been added after load
			if($('#' + tableArray[i] + ' .selected').data('row') == 'added') {
				index = -1;
			}
			break;
		} else {
			index = index + $('#' + tableArray[i] + ' table').data('rowLength');
		}
	}
	return index;
}

//add server field error on the fields
function addServerFieldError(modalBean, errorFields) {
	for(var i = 0; i < errorFields.length; i++ ) {
		var field = errorFields[i];
		if(field != ""){
			field = modalBean + "." + field;
			
			if(field.indexOf(".") != -1) {
				field = field.replace('.', '\\.')
			}
			
			//get element by name
			elem = "[name=" + field + "]";

			if($(elem).length <= 0){
				elem = "." + field;
			}
			
			$(elem).each(function(index, value){
				if($(elem).is(':visible') || $(elem).parent().hasClass('select')){
					if($(this).closest('div.form-group').find('label.control-label').parent().is( "div.form-label" )){
						$(this).closest('div.form-group').find('label.control-label').unwrap();
					}
					if($(this).closest('div[class*="col-"]').parent().is( "div.form-field" )){
						$(this).closest('div[class*="col-"]').unwrap();
					}
					$(this).closest('div.form-group').find('label.control-label').wrap(('<div class="form-label"></div>'));
					$(this).closest('div[class*="col-"]').wrap(('<div class="form-field"></div>'));
					$(this).closest('div.form-field').addClass('has-error');
					$(this).closest('div.form-group').find('div.form-label').addClass('has-error');
				}
			});	
		}
	}
	
}

/*
 * parentID-id of the parent element (example: province in [province-city])
 * childID-id of the child element (example: city in [province-city])
 * isChildMandatory-determines if child is mandatory or not
 * displayAllOnEmptyParent-determines if all items of child will be displayed if parent is null/empty
 * modalID-id of modal
 * dropdownType-keyword for ajax way of getting value of child dropdown
*/
function addDropdownEvent(parentID, childID, isChildMandatory, displayAllOnEmptyParent, modalID, depends, dropdownType) {
	var modalName = (modalID == undefined || modalID == '') ? '' : '#' + modalID;
	$(modalName + ' #' + parentID).on('ajaxDropdown', function(){
		if(dropdownType != undefined && dropdownType != '') {
			ajaxDropdownEvent($(modalName + ' #' + parentID).attr('value'), childID, dropdownType);
		}
	});
	
	$(modalName + ' #' + parentID).trigger('ajaxDropdown');
	filterDropdown($(modalName + ' #' + parentID).attr('value'), childID, isChildMandatory, displayAllOnEmptyParent, modalID, depends);
	
	$(modalName + ' #' + parentID).change(function(){
		$(modalName + ' #' + parentID).trigger('ajaxDropdown');
		filterDropdown($(this).attr('value'), childID, isChildMandatory, displayAllOnEmptyParent, modalID, depends);
		//clear child values
		$(modalName + ' #' + childID + ',' + modalName + ' #' + childID + '_view').val('');
	});
}

/*
 * parentID-id of the parent element (example: province in [province-city])
 * childID-id of the child element (example: city in [province-city])
 * isChildMandatory-determines if child is mandatory or not
 * displayAllOnEmptyParent-determines if all items of child will be displayed if parent is null/empty
 * modalID-id of modal
*/
function filterDropdown(parentValue, childID, isChildMandatory, displayAllOnEmptyParent, modalID, depends) {
	var depends = (depends == undefined || depends == '') ? false : true;
	var modalName = (modalID == undefined || modalID == '') ? '' : '#' + modalID;
	var childSelector = $(modalName + ' #' + childID);
	var childViewSelector = $(modalName + ' #' + childID + '_view');
	
	var prov = $.trim(parentValue);
	if(prov == '') {
		if(!displayAllOnEmptyParent) {
			childSelector.parent('div.select').find('ul > li:not(:first)').removeClass('filtered');
			childViewSelector.attr("disabled", true);
			doClearDropdownValue(childID);
		}
	} else {
		childSelector.parent('div.select').find('ul > li:not(:first)').addClass('filtered');
		childViewSelector.attr("disabled", false);
	}

	//remove mandatory * in label
	//added checking if mandatory checking depends on per transaction
	if(!depends){
		if(isChildMandatory && prov != '')
			childSelector.closest('div.form-group').find('label').addClass('mandatory');
		else
			childSelector.closest('div.form-group').find('label').removeClass('mandatory');	
	}
	
	//added to not include class selector(.) if prov is empty
	var provClassSelector = (prov == '') ? '' : '.' + prov;
	childSelector.parent('div.select').find('ul > li' + provClassSelector).removeClass('filtered');
	childSelector.parent('div.select').find('ul > li.liHover').removeClass('liHover');

	//hide filtered, show not(filtered)
	childSelector.siblings('.list-group').find('.filtered').hide();
	childSelector.siblings('.list-group').find(':not(.filtered)').show();
	
	//fix for items that have the same itemValue
	var selected = childSelector.siblings('.list-group').find(".list-group-item:not(.filtered)[data-value='"+childSelector.val()+"']");//changed to data-value for IE11 compatibility
	childViewSelector.val(childSelector.val() != '' ? selected.text() : '');
}

/*
 * parentValue-value of the parent element
 * childID-id of the child element (example: city in [province-city])
 * dropdownType-keyword for ajax way of getting value of child dropdown
*/
function ajaxDropdownEvent(parentValue, childID, dropdownType) {
	$('#' + childID + '_view').siblings('.list-group').find('.list-group-item:gt(0)').remove();
 	$.ajax({
    	type: "POST",
        url: "AddDropdownEvent.htm",
        data:{"code":parentValue, 
        		"dropdownType":dropdownType,
        },
        success:function (response) {
        	for (var i in response){
        		var description = dropdownType == 'agencyList' ? response[i].codeDescription : response[i].description;
        		$('#' + childID + '_view').siblings('.list-group').append('<li class="list-group-item ' + response[i].optionClass + '" data-value="' + response[i].code + '">' + description + '</li>');//changed to data-value for IE11 compatibility
        		if(response[i].code == $('#' + childID).attr('value')) {
        			$('#' + childID + '_view').val(description);
        		}
        	}
	   	},
        error:function (xhr, ajaxOptions, thrownError){
         	manageMessage("autoMessage", "Error retrieving dropdown values.");
     	}    
 	});
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function createCookie(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}


//GGP 20180627 functions for search criteria
function showAdvanceSearch() {
    $("#extcriteria").slideToggle();
}

	function  hideAdvanceSearch() {
    $("#extcriteria").slideUp();
}

function  changeChevron(button) {
	if ($(button).hasClass("show-more")) {
		$(button).find("span").text("Show Less");
        $(button).find("i").removeClass("fa fa-chevron-down").addClass("fa fa-chevron-up");
        $(button).removeClass("show-more").addClass("show-less");
	} else {
		$(button).find("span").text("Show More");
        $(button).find("i").removeClass("fa fa-chevron-up").addClass("fa fa-chevron-down");
        $(button).removeClass("show-less").addClass("show-more");
	}
}

//LEL20180924
function encryptFields(fields, bfKey){
	
	for(i = 0; i < fields.length; i++){
		var field = fields[i];
		$('#'+field.to).val(blowfish.encrypt($('#'+field.from).val(), bfKey, {cipherMode: 0, outputType: 0}));
	}
}	